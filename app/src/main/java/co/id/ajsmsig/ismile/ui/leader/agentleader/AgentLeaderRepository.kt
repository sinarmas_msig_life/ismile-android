package co.id.ajsmsig.ismile.ui.leader.agentleader

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.di.NetManager
import co.id.ajsmsig.ismile.model.api.AgentLeaderApproveRequest
import co.id.ajsmsig.ismile.model.api.AgentLeaderApproveResponse
import co.id.ajsmsig.ismile.model.api.AgentLeaderStatusApproveRequest
import co.id.ajsmsig.ismile.model.api.AgentLeaderStatusApproveResponse
import co.id.ajsmsig.ismile.util.PREF_KEY_AGENT_CODE
import co.id.ajsmsig.ismile.util.PREF_KEY_AGENT_NAME
import co.id.ajsmsig.ismile.util.PREF_KEY_AGENT_TITLE
import io.reactivex.Observable
import javax.inject.Inject

class AgentLeaderRepository @Inject constructor(private val editor: SharedPreferences.Editor, private val preferences: SharedPreferences, private val netManager: NetManager, private val apiService: ApiService) {

    fun clearSharedPreference() {
        editor.clear()
        editor.commit()
    }

    fun getSavedAgentLeaderName() = preferences.getString(PREF_KEY_AGENT_NAME, null)

    fun getSavedAgentLeaderCode() = preferences.getString( PREF_KEY_AGENT_CODE, null)

    fun getSavedAgentLeaderTitle() = preferences.getString(PREF_KEY_AGENT_TITLE, null)

    fun fetchApprovalAgentCandidate (requestCode:Int): Observable <AgentLeaderStatusApproveResponse>{
        val agentLeaderId = preferences.getString( PREF_KEY_AGENT_CODE, null)
        if (netManager.isConnectedToInternet){
            return apiService.fetchApprovalAgentCandidate(AgentLeaderStatusApproveRequest(agentLeaderId!!,requestCode))
        }
        return Observable.just(AgentLeaderStatusApproveResponse(emptyList(), true, "Mohon periksa kembali koneksi internet anda"))
    }

    fun setApporvalAggrementAgentLeader (registrationId: String, reasonReject: String, statusApprove: Boolean): Observable <AgentLeaderApproveResponse>{
        val agentLeaderId = preferences.getString( PREF_KEY_AGENT_CODE, null)
        if (netManager.isConnectedToInternet){
            return apiService.agentLeaderSubmitApproval(AgentLeaderApproveRequest(agentLeaderId!!, reasonReject, registrationId , statusApprove))
        }
        return Observable.just(AgentLeaderApproveResponse(true, "Mohon periksa kembali koneksi internet anda"))
    }

}