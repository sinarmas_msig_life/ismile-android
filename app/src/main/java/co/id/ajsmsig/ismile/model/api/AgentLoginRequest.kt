package co.id.ajsmsig.ismile.model.api

import com.google.gson.annotations.SerializedName

data class AgentLoginRequest(
    @SerializedName("username")
    val userName: String,
    @SerializedName("password")
    val password: String
)