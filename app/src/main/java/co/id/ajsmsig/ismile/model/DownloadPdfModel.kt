package co.id.ajsmsig.ismile.model

data class DownloadPdfModel(
        val error : Boolean,
        val message : String?
)