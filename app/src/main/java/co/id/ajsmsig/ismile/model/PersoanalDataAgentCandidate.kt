package co.id.ajsmsig.ismile.model

data class PersoanalDataAgentCandidate(
    val email: String?,
    val homeAddress: String?,
    val placeOfBirth: String?,
    val city: Int?,
    val cityName: String?,
    val npwpNumber: String?,
    val housePhone: String?,
    val profilePhotoImgPath: String?,
    val npwpPhotoImgPath: String?,
    val savingAccountImgPath: String?,
    val genderId : Int?,
    val religionId : Int?,
    val personalDataDate : String?
)