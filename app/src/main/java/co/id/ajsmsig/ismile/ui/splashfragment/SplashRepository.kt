package co.id.ajsmsig.ismile.ui.splashfragment

import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.model.ActiveSessionModel
import co.id.ajsmsig.ismile.model.AppVersionRequest
import co.id.ajsmsig.ismile.model.AppVersionResponse
import javax.inject.Inject

class SplashRepository @Inject constructor(private val localDataSource: SplashLocalDataSource, private val api: ApiService) {

    fun saveUserSession(agentCode : String, agentName : String, agentRoleType: Int) {
        localDataSource.saveAgentCode(agentCode)
        localDataSource.saveAgentName(agentName)
        localDataSource.setLoggedIn(true)
        localDataSource.saveLoginRoleType(agentRoleType)
    }

    fun getUserCurrentSession(): ActiveSessionModel {
        val loginRoleType = localDataSource.getLoginRoleType()
        val isLoggedIn = localDataSource.isLoggedIn()
        return ActiveSessionModel(isLoggedIn, loginRoleType)
    }

    suspend fun getInfoUpdateApps(data: AppVersionRequest) = api.getVersionAppUpdate(data)
}