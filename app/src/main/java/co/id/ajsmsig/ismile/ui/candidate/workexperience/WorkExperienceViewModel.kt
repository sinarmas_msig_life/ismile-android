package co.id.ajsmsig.ismile.ui.candidate.workexperience

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.SingleLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.db.entity.minimal.ViewWorkExperienceMinimal
import co.id.ajsmsig.ismile.model.ConvertStringToDate
import co.id.ajsmsig.ismile.util.DateUtils
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class WorkExperienceViewModel @Inject constructor(private val repository: WorkExperienceRepository) : BaseViewModel() {

    var companyName = ObservableField<String>()
    var errorCompanyName = ObservableInt()

    var position = ObservableField<String>()
    var errorPosition = ObservableInt()

    var startDateWork = ObservableField<String>("")
    var errorStartDateWork = ObservableInt()

    var endDateWork = ObservableField<String>("")
    var errorEndDateWork = ObservableInt()

    private var selectedSchoolLevel = ObservableField <String>()
    private var selectedSchoolId = ObservableField <Int> ()

    private var _isSchoolId= MutableLiveData <Int>()
    val isSchoolId : LiveData <Int>
        get() = _isSchoolId

    private var _isConvertStartDate = MutableLiveData <ConvertStringToDate>()
    val isConvertStartDate : LiveData<ConvertStringToDate>
        get()=_isConvertStartDate

    private var _isConvertEndDate = MutableLiveData <ConvertStringToDate>()
    val isConvertEndDate : LiveData<ConvertStringToDate>
        get()=_isConvertEndDate

    private val _isSucces = SingleLiveData<Boolean>()
    val isSucces: LiveData<Boolean>
        get() = _isSucces

    fun viewWorkExperienceSubmit() {
        mCompositeDisposable += repository.getWorkExperience()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { data -> getDataWorkExperienceSucces(data) },
                { error -> getDataWorkFailed(error) })
    }

    private fun submitDataWorkExperience(time: String) {
        val convertStartDate= DateUtils.getDateFormat1( startDateWork.get()!!)
        val convertEndDate= DateUtils.getDateFormat1( endDateWork.get()!!)
        mCompositeDisposable += Completable.fromCallable { repository.addWorkExeperience(companyName.get()!!, position.get()!!,convertStartDate!! , convertEndDate!!, time, selectedSchoolId.get()!!, selectedSchoolLevel.get()!!, true) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { saveWorkExperienceSucces() },
                { error -> saveInDatabaseFailed(error) })
    }

    fun validateUserInput(time: String) {
        if (!isCompanyNameValid()) {
            return
        }
        if (!isPositionValid()) {
            return
        }
        if (!isStartDateWorkValid()) {
            return
        }
        if (!isEndDateWorkValid()) {
            return
        }
        if (!isWorkDurationInputValid()){
            return
        }
        submitDataWorkExperience(time)
    }

    private fun getDataWorkExperienceSucces(data: ViewWorkExperienceMinimal) {
        if (!data.startDateWork.isNullOrEmpty()){
            val startDate = DateUtils.getDateFormat(data.startDateWork!!)
            val endDate = DateUtils.getDateFormat(data.endDateWork!!)
            companyName.set(data.companyName)
            position.set(data.lastPosition)
            startDateWork.set(startDate)
            endDateWork.set(endDate)
            _isSchoolId.value = data.schoolId
        }
    }

    private fun getDataWorkFailed(error: Throwable?) {
        Timber.e("Data work experience failed $error")
    }

    private fun saveInDatabaseFailed(error: Throwable?) {
        Timber.e("messegae Error $error")
    }

    private fun saveWorkExperienceSucces() {
        _isSucces.sendAction(true)
    }

    private fun isPositionValid(): Boolean {
        if (position.get().isNullOrEmpty()) {
            setPositionError(R.string.no_empty_field)
            return false
        } else {
            setPositionError()
        }
        return true
    }

    private fun isCompanyNameValid(): Boolean {
        if (companyName.get().isNullOrEmpty()) {
            setErroCompanyName(R.string.no_empty_field)
            return false
        } else {
            setErroCompanyName()
        }
        return true
    }

    private fun isStartDateWorkValid(): Boolean {
        if (startDateWork.get().isNullOrEmpty()) {
            setStartDateError(R.string.no_empty_field)
            return false
        } else {
            setStartDateError()
        }
        return true
    }

    private fun isEndDateWorkValid(): Boolean {
        if (endDateWork.get().isNullOrEmpty()) {
            setEndDateError(R.string.no_empty_field)
            return false
        } else {
            setEndDateError()
        }
        return true
    }

    private fun isWorkDurationInputValid() : Boolean {

        return true
    }

    fun setSelectedStartDate (){
        if (!startDateWork.get().isNullOrEmpty()){
            val date = startDateWork.get()
            _isConvertStartDate.value = DateUtils.setDateFormat(date!!)
        }
    }

    fun setSelectedEndDate (){
        if (!endDateWork.get().isNullOrEmpty()){
            val date = endDateWork.get()
            _isConvertEndDate.value = DateUtils.setDateFormat(date!!)
        }
    }

    fun setStartDate (time: String){
        startDateWork.set(time)
    }

    fun setEndDate (time: String){
        endDateWork.set(time)
    }

    fun saveSelectedSchool (schoolId: Int, schoolLevel: String) {
        selectedSchoolId.set (schoolId)
        selectedSchoolLevel.set(schoolLevel)
    }

    private fun setErroCompanyName(message: Int = 0) = errorCompanyName.set(message)

    private fun setPositionError(message: Int = 0) = errorPosition.set(message)

    private fun setStartDateError(message: Int = 0) = errorStartDateWork.set(message)

    private fun setEndDateError(message: Int = 0) = errorEndDateWork.set(message)
}