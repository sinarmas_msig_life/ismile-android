package co.id.ajsmsig.ismile.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import co.id.ajsmsig.ismile.base.BaseDao
import co.id.ajsmsig.ismile.db.entity.AgentCandidate
import co.id.ajsmsig.ismile.db.entity.embedded.CompletionStatus
import co.id.ajsmsig.ismile.db.entity.join.AgentCandidateAndAllQuestionnaire
import co.id.ajsmsig.ismile.db.entity.minimal.*
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface AgentCandidateDao : BaseDao<AgentCandidate> {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    override fun insert(entity: AgentCandidate): Single<Long>

    @Query("UPDATE agent_candidates set email=:email, home_address=:homeAddress, place_of_birth=:placeOfBirth, city_id =:cityId,city_name =:cityName, npwp_number=:npwpNumber, house_phone=:housePhone, profile_photo_img_path=:profilePhotoImgPath, npwp_photo_img_path=:npwpPhotoImgPath, saving_account_img_path=:savingAccountImgPath, religion_id=:religionId, personal_data_date =:personalDataDate where id=:registrationId")
    fun updateProfileAgentCandidate(email:String, homeAddress:String, placeOfBirth:String, cityId:Int,cityName: String, npwpNumber:String, housePhone:String,profilePhotoImgPath:String,npwpPhotoImgPath:String,savingAccountImgPath:String, religionId:Int, personalDataDate:String, registrationId:String)

    @Query("UPDATE AGENT_CANDIDATES SET ETHIC_CODE_CHECKED = :isEthicCodeChecked, ETHIC_CODE_DATE = :millis WHERE id = :registrationId")
    fun updateEthicCode(registrationId: String, isEthicCodeChecked: Int, millis: String?)

    @Query("UPDATE AGENT_CANDIDATES SET EMPLOYMENT_CONTRACT_CHECKED = :isEmploymentContractChecked, EMPLOYMENT_CONTRACT_DATE = :millis WHERE id = :registrationId")
    fun updateEmploymentContract(registrationId: String, isEmploymentContractChecked: Int, millis: String?)

    @Query("UPDATE AGENT_CANDIDATES SET AGENCY_LETTER_CHECKED = :isAgencyLetterChecked, AGENCY_LETTER_DATE = :millis, STATEMENT_COMPLETE = :isStatementCompleted WHERE id = :registrationId")
    fun updateAgencyLetter(registrationId: String, isAgencyLetterChecked: Int, isStatementCompleted : Boolean, millis: String?)

    @Query("SELECT * FROM AGENT_CANDIDATES WHERE ID = :registrationId")
    fun findAllSavedAnswer(registrationId: String): Maybe<AgentCandidateAndAllQuestionnaire>

    @Query("SELECT personal_data_date, work_experience_date, education_date, questionnaire_date, ethic_code_date, employment_contract_date, agency_letter_date, signature_date, exam_date, exam_complete, personal_data_complete, signature_complete, statement_complete, license_complete FROM AGENT_CANDIDATES WHERE ID = :registrationId")
    fun findCompletionStatus(registrationId: String): Maybe<CompletionStatus>

    @Query("UPDATE AGENT_CANDIDATES SET QUESTIONNAIRE_DATE = :millis WHERE id = :registrationId")
    fun updateQuestionnaireCompleted(registrationId:String, millis: String? )

    @Query ("UPDATE agent_candidates set company_name=:companyName, last_position=:lastPosition, start_date_work=:startDateWork,end_date_work=:endDateWork, work_experience_date=:workExperienceDate, school_id=:schoolId, school_name=:schoolName, education_date=:workExperienceDate, personal_data_complete =:personalDataComplete  where id=:registrationId")
    fun updateWorkExperience (companyName : String, lastPosition: String, startDateWork:String, endDateWork:String, workExperienceDate:String,schoolId:Int, schoolName : String, personalDataComplete: Boolean, registrationId:String)

    @Query ("UPDATE agent_candidates set signature_checked=:signatureChecked,signature_img_path=:signatureImgPath, signature_date=:signatureDate, signature_complete=:signatureComplete where id =:registrationId ")
    fun updateSignatureAgentCandidate (signatureChecked : Boolean, signatureImgPath : String, signatureDate:String,signatureComplete:Boolean, registrationId: String)

    @Query ("SELECT signature_checked, signature_img_path FROM agent_candidates WHERE id=:registrationId")
    fun getInfoSignature(registrationId: String) : Observable<SigantureAgentCandidateMinimal>

    @Query("SELECT PLACE_OF_BIRTH, HOME_ADDRESS, TITLE_NAME FROM AGENT_CANDIDATES WHERE id = :registrationId")
    fun findDataForAgencyLetter(registrationId: String) : Maybe<AgencyLetterData>

    @Query("SELECT PERSONAL_DATA_COMPLETE FROM AGENT_CANDIDATES WHERE id = :registrationId")
    fun findPersonalDataCompletionStatus(registrationId: String) : Single<Boolean>

    @Query("SELECT PERSONAL_DATA_COMPLETE FROM AGENT_CANDIDATES WHERE id = :registrationId")
    fun findStatementCompletionStatus(registrationId: String) : Single<Boolean>

    @Query("SELECT STATEMENT_COMPLETE FROM AGENT_CANDIDATES WHERE id = :registrationId")
    fun findSignatureCompletionStatus(registrationId: String) : Single<Boolean>

    @Query("SELECT SIGNATURE_COMPLETE FROM AGENT_CANDIDATES WHERE id = :registrationId")
    fun findLicenseCompletionStatus(registrationId: String) : Single<Boolean>

    @Query("SELECT email,home_address, place_of_birth,city_id,city_name, npwp_number, house_phone, profile_photo_img_path, npwp_photo_img_path, saving_account_img_path, gender_id, religion_id , personal_data_date FROM agent_candidates where id=:id")
    fun getPersonalData (id: String) : Maybe <PersonalDataMinimal>

    @Query ("SELECT email,personal_data_date FROM agent_candidates WHERE id=:registrationId")
    fun getDataSubmitPersonal (registrationId: String) : Maybe <ValidationPersonalData>

    @Query ("SELECT company_name, last_position, start_date_work, end_date_work, school_id FROM agent_candidates where id =:registrationId ")
    fun getWorkExperience (registrationId: String) : Maybe <ViewWorkExperienceMinimal>

    @Query ("SELECT name, identity_number, birth_of_date, marital_status, phone_number, title_name, bop_date, email, gender_id FROM agent_candidates WHERE id =:registrationId ")
    fun getProfileAgentCandidate(registrationId : String) : Maybe <ProfileAgentCandidateMinimal>

    @Query ("SELECT name, identity_number, birth_of_date, marital_status, phone_number, title_name, bop_date, email, gender_id FROM agent_candidates WHERE id =:registrationId ")
    suspend fun checkedForAutoMatedLogout(registrationId : String) : ProfileAgentCandidateMinimal
//    @Query ("SELECT is_submitted FROM agent_candidates WHERE id =:registrationId ")
//    fun isSubmitted(registrationId : Long) : Single<Boolean>

    @Query("UPDATE AGENT_CANDIDATES SET IS_SUBMITTED = :isSubmitted WHERE ID = :registrationId")
    fun markAsSubmitted(registrationId: String, isSubmitted : Boolean)

    @Query ("UPDATE agent_candidates set exam_city_id=:examCityId, exam_city_name=:examCityName, exam_place_id=:examPlaceId, exam_place_name=:examPlaceName, exam_selected_date=:examSelectedDate, exam_method_id=:examMethodId, exam_method_name=:examMethodName, exam_product_type_id=:examProductTypeId, exam_product_type_name=:examProductTypeName, exam_type_id=:examTypeId, exam_type_name=:examTypeName, number_rek=:numberRek, banking=:banking, nominal=:nominal where id=:registrationId")
    fun licenseSubmitted (examCityId: Int, examCityName: String, examPlaceId: Int, examPlaceName:String, examSelectedDate:String, examMethodId:Int, examMethodName:String, examProductTypeId:Int,examProductTypeName: String, examTypeId: Int, examTypeName: String, numberRek:String, banking: String, nominal:String, registrationId: String)

    @Query ("SELECT exam_city_name, exam_place_name, exam_selected_date, exam_method_id, exam_product_type_id, exam_type_id, number_rek, banking, nominal from agent_candidates where id=:registrationId")
    fun retrieveInfoSubmitted (registrationId: String) : Maybe <InfoSumbittedExamMinimal>

    @Query ("SELECT exam_city_id, exam_city_name,exam_place_id, exam_place_name, exam_selected_date, exam_method_id, exam_method_name, exam_product_type_id, exam_product_type_Name,exam_type_name,exam_type_id, exam_type_name, number_rek, banking, nominal from agent_candidates where id=:registrationId  ")
    fun examInformation (registrationId: String) : Maybe <ExamInformationMinimal>
}

