package co.id.ajsmsig.ismile.ui.candidate.workexperience

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.fragment.app.FragmentActivity
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.databinding.CustomSpinnerEducationalBackgroundBinding
import co.id.ajsmsig.ismile.model.SchoolLevel

class EducationalBackgroundSpinnerAdapter (private val context: FragmentActivity, private var school: List <SchoolLevel>) : BaseAdapter () {

    val inflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view :View
        val vh: ItemRowHolder
        if (convertView == null) {
            val binding = CustomSpinnerEducationalBackgroundBinding.inflate(inflater, parent, false)
            vh = EducationalBackgroundSpinnerAdapter.ItemRowHolder(binding)
            view = binding.root
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as EducationalBackgroundSpinnerAdapter.ItemRowHolder
        }

        vh.bind(R.drawable.ic_action_employee, school[position].level)

        return view

    }

    private class ItemRowHolder ( private val binding: CustomSpinnerEducationalBackgroundBinding){
        fun bind(imageResourceId : Int, label : String) {
            binding.ivEducational.setImageResource(imageResourceId)
            binding.tvEducational.text = label
            binding.executePendingBindings()
        }

    }

    override fun getItem(position: Int): Any?=  null

    override fun getItemId(position: Int): Long = 0

    override fun getCount(): Int = school.size

    fun refreshData(school : List<SchoolLevel>) {
        this.school = school
        notifyDataSetChanged()
    }
}