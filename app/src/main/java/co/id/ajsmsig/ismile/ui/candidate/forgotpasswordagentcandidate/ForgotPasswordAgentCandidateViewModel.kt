package co.id.ajsmsig.ismile.ui.candidate.forgotpasswordagentcandidate

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import co.id.ajsmsig.ismile.SingleLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.model.api.AgentCandidateForgotPasswordResponse
import co.id.ajsmsig.ismile.model.uimodel.ForgotPasswordUiModel
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ForgotPasswordAgentCandidateViewModel @Inject constructor(private val repository: ForgotPasswordAgentCandidateRepository) :
    BaseViewModel() {

    var userId = ObservableField<String>()
    var errorUserId = ObservableField<String>()

    var phoneNumber = ObservableField<String>()
    var errorPhoneNumber = ObservableField<String>()

    private val _isFindAccountSucces = SingleLiveData<ForgotPasswordUiModel>()
    val isFindAccountSucces: LiveData<ForgotPasswordUiModel>
        get() = _isFindAccountSucces

    private fun findAccount() {
        isLoading.set(true)
        mCompositeDisposable += repository.forgotPasswordAgentCandidate(userId.get().toString(), phoneNumber.get().toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response -> onFindSucces(response) },
                { onFindError() }
            )
    }

    fun validateUserInput() {
        if (!isUsernameValid()) {
            return
        }
        if (!isPhoneNumberValid()) {
            return
        }
        findAccount()
    }

    private fun isUsernameValid(): Boolean {
        if (userId.get().isNullOrEmpty()) {
            setErrorUsername("Field ini tidak boleh kosong")
            return false
        } else {
            setErrorUsername(null)
        }
        return true
    }

    private fun isPhoneNumberValid(): Boolean {
        if (phoneNumber.get().isNullOrEmpty()) {
            setErrorPhoneNumber("Field ini tidak boleh kosong")
            return false
        } else if (!phoneNumber.get()!!.startsWith("08")) {
            setErrorPhoneNumber("Nomor handphone harus diawali dengan nomor '08'")
            return false
        } else if (phoneNumber.get()!!.length < 10) {
            setErrorPhoneNumber("Nomor handphone minimal memilki 10 karakter")
            return false
        } else {
            setErrorPhoneNumber(null)
        }
        return true
    }

    private fun onFindSucces(data: AgentCandidateForgotPasswordResponse) {
        isLoading.set(false)
        if (!data.error) {
            _isFindAccountSucces.sendAction(ForgotPasswordUiModel(true,data.message))
        } else {
            _isFindAccountSucces.sendAction(ForgotPasswordUiModel(false,data.message))
        }
    }

    private fun onFindError() {
        _isFindAccountSucces.sendAction(ForgotPasswordUiModel(false,"Terjadi kesalahan. Mohon coba kembali"))
        isLoading.set(false)
    }


    private fun setErrorUsername(message: String?) = errorUserId.set(message)
    private fun setErrorPhoneNumber(message: String?) = errorPhoneNumber.set(message)
}