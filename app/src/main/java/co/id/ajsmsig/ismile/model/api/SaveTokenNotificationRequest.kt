package co.id.ajsmsig.ismile.model.api


import com.google.gson.annotations.SerializedName

data class SaveTokenNotificationRequest(
    @SerializedName("jenis_id")
    val jenisid: Int,
    @SerializedName("token")
    val token: String,
    @SerializedName("userid")
    val userid: String?
)