package co.id.ajsmsig.ismile.model

data class DetailAgentCandidateApproval (val fullName: String?,
                                         val recruiterName: String?,
                                         val titleName: String?,
                                         val companyName: String?,
                                         val lastPosition: String?,
                                         val timeWork: String?,
                                         val error: Boolean,
                                         val message: String)