package co.id.ajsmsig.ismile.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import co.id.ajsmsig.ismile.db.dao.AgentCandidateDao
import co.id.ajsmsig.ismile.db.dao.QuestionnaireDao
import co.id.ajsmsig.ismile.db.dao.QuestionnaireDetailDao
import co.id.ajsmsig.ismile.db.entity.AgentCandidate
import co.id.ajsmsig.ismile.db.entity.Questionnaire
import co.id.ajsmsig.ismile.db.entity.QuestionnaireDetail
import co.id.ajsmsig.ismile.util.Converters


@Database(entities = [AgentCandidate::class, Questionnaire::class, QuestionnaireDetail::class], version = 2)
@TypeConverters(Converters::class)
abstract class iSmileDatabase : RoomDatabase() {

    abstract fun agentCandidateDao(): AgentCandidateDao
    abstract fun questionnaireDao(): QuestionnaireDao
    abstract fun questionnaireDetailDao(): QuestionnaireDetailDao

}