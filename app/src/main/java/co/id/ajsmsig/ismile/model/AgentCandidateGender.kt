package co.id.ajsmsig.ismile.model

data class AgentCandidateGender (val id:Int, val gender:String)