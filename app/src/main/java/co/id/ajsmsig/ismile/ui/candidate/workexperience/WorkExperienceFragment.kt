package co.id.ajsmsig.ismile.ui.candidate.workexperience

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.DatePicker
import android.widget.Spinner
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentWorkExperienceBinding
import co.id.ajsmsig.ismile.model.SchoolLevel
import co.id.ajsmsig.ismile.ui.candidate.profileagentcandidate.ProfileAgentCandidateFragmentArgs
import co.id.ajsmsig.ismile.util.DateUtils
import co.id.ajsmsig.ismile.util.PREFF_KEY_AGENT_CANDIDATE_CODE
import com.google.android.material.snackbar.Snackbar
import java.util.*


class WorkExperienceFragment : BaseFragment<FragmentWorkExperienceBinding, WorkExperienceViewModel>() {

    override fun getLayoutResourceId() = R.layout.fragment_work_experience

    override fun getViewModelClass() = WorkExperienceViewModel::class.java

    private val calendar = Calendar.getInstance()
    private var startYear = calendar.get(Calendar.YEAR)
    private var startMonth = calendar.get(Calendar.MONTH)
    private var startDay = calendar.get(Calendar.DAY_OF_MONTH)

    private var endYear = calendar.get(Calendar.YEAR)
    private var endMonth = calendar.get(Calendar.MONTH)
    private var endDay = calendar.get(Calendar.DAY_OF_MONTH)
    var isEnabledEdit: Boolean = false

    private lateinit var  schoolLevelAdapter : EducationalBackgroundSpinnerAdapter
    private lateinit var spinnerSchoolLevel : Spinner

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        isEnabledEdit = ProfileAgentCandidateFragmentArgs.fromBundle(arguments!!).isSubmitted
        return super.onCreateView(inflater, container, savedInstanceState)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val vm = getViewModel()
        getDataBinding().vm = vm
        getDataBinding().fragment = this

        initEducationalBackground()

        getViewModel().viewWorkExperienceSubmit()

        getViewModel().isSucces.observe(this, androidx.lifecycle.Observer {
            it?.let {
                if (it) {
                    launchAgentCandidate ()
                }
            }
        })

        getViewModel().isConvertStartDate.observe(this, androidx.lifecycle.Observer {
            it?.let {
                startYear= it.year
                startMonth= it.month
                startDay= it.day
            }
        })

        getViewModel().isConvertEndDate.observe(this, androidx.lifecycle.Observer {
            it?.let {
                endYear= it.year
                endMonth= it.month
                endDay= it.day
            }
        })

        getViewModel().isSchoolId.observe( this, androidx.lifecycle.Observer {
            it?.let {
                refreshSpinnerSchool(it)
            }
        })
    }

    fun onNextButtonClicked() {
        val time = DateUtils.getCurrentTime()
        getViewModel().validateUserInput(time)
    }

    fun onStartDateWorkClick() {
        getViewModel().setSelectedStartDate()
        val dialog = DatePickerDialog(activity!!, dateStartWorkListener, startYear, startMonth, startDay)
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis())
        dialog.show()
    }

    fun onEndDateWorkClick() {
        getViewModel().setSelectedEndDate()
        val dialog = DatePickerDialog(activity!!, dateEndWorkListener, endYear, endMonth, endDay)
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis())
        dialog.show()
    }

    val dateStartWorkListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
            val day = String.format("%02d", dayOfMonth)
            val month = String.format("%02d", monthOfYear + 1) //Month is 0-based
            val selectedDate = "$day-$month-$year"
            getViewModel().setStartDate(selectedDate)
        }
    }


    val dateEndWorkListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
            val day = String.format("%02d", dayOfMonth)
            val month = String.format("%02d", monthOfYear + 1) //Month is 0-based
            val selectedDate = "$day-$month-$year"
            getViewModel().setEndDate(selectedDate)
        }
    }

    private fun initEducationalBackground (){
        val school = mutableListOf<SchoolLevel>()
        school.add(SchoolLevel(1,"SMA"))
        school.add(SchoolLevel(2,"Diploma"))
        school.add(SchoolLevel(3,"S1"))
        school.add(SchoolLevel(4,"S2"))
        school.add(SchoolLevel(5,"S3"))

        schoolLevelAdapter = EducationalBackgroundSpinnerAdapter(activity!!, school)
        spinnerSchoolLevel = getDataBinding().spinnerSchoolLevel
        spinnerSchoolLevel.adapter = schoolLevelAdapter
        spinnerSchoolLevel.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedSchool = school[position].id
                val selectedName = school[position].level
                getViewModel().saveSelectedSchool (selectedSchool, selectedName)
            }
        }
    }

    private fun refreshSpinnerSchool(index: Int) {
        spinnerSchoolLevel.setSelection(index-1, true)
    }

    private fun launchAgentCandidate (){
        val registrtationId=activity?.getSharedPreferences(PREFF_KEY_AGENT_CANDIDATE_CODE, Context.MODE_PRIVATE)
        Snackbar.make(getDataBinding().root, "Berhasil disimpan", Snackbar.LENGTH_SHORT).show()
        val action = WorkExperienceFragmentDirections.actionLaunchAgentCandidateFragment(registrtationId.toString())
        findNavController().navigate(action)
    }

}
