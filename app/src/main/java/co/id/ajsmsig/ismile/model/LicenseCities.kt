package co.id.ajsmsig.ismile.model

data class LicenseCities (val cities: List<String>)