package co.id.ajsmsig.ismile.ui.agent.login


import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentLoginBinding
import com.google.firebase.iid.FirebaseInstanceId
import timber.log.Timber


class LoginFragment : BaseFragment<FragmentLoginBinding, LoginViewModel>() {

    override fun getLayoutResourceId() = R.layout.fragment_login
    override fun getViewModelClass(): Class<LoginViewModel> = LoginViewModel::class.java

    //Defined the required values
    companion object {
        const val CHANNEL_ID = "ismileapp-sinarmasmsiglife"
        private const val CHANNEL_NAME= "i-SMiLe"
        private const val CHANNEL_DESC = "i-SMiLe Channel"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val vm = getViewModel()
        getDataBinding().vm = vm
        getDataBinding().fragment = this

        setLoginType()

        vm.isLoginSuccess.observe(this, Observer {
                it?.let {
                    if (it.error) {
                        snackbar(it.message)
                    } else {
                        launchHomeFragment(it.leader)
                    }
                }
            })
    }

    fun onLoginPressed() {
        getViewModel().validateUserInput()
    }

    fun forgotPasswordClick() {
        val loginType = LoginFragmentArgs.fromBundle(arguments!!).loginType
        val action= LoginFragmentDirections.actionForgotPaswordAgentLaunch(loginType)
        findNavController().navigate(action)
    }

    fun setLoginType() {
        val logynType = LoginFragmentArgs.fromBundle(arguments!!).loginType
        getViewModel().setLoginType (logynType)
    }
    private fun launchHomeFragment(leader:Boolean) {
        initFirebasePushNotif()
        createNotificationChannel()
        if (leader){
            val action = LoginFragmentDirections.actionLaunchAgentLeaderFragment()
            findNavController().navigate(action)
        }else{
            val action = LoginFragmentDirections.actionLaunchHomeFragment()
            findNavController().navigate(action)
        }
    }

    private fun initFirebasePushNotif() {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            if (!it.isSuccessful) {
                Timber.e("[Notification] Unable to generate token ${it.exception}")
                return@addOnCompleteListener
            }

            // Get new Instance ID token
            val token = it.result?.token
            Timber.v("[Notification] Your token is : $token")
            getViewModel().saveTokenNotification(token!!)
        }
    }

    private fun createNotificationChannel() {
        //creating notification channel if android version is greater than or equals to oreo
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(LoginFragment.CHANNEL_ID, LoginFragment.CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)
            channel.description = LoginFragment.CHANNEL_DESC
            val manager = activity?.getSystemService(NotificationManager::class.java)
            manager?.createNotificationChannel(channel)
        }
    }
}
