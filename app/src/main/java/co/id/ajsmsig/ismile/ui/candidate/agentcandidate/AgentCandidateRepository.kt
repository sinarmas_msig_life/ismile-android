package co.id.ajsmsig.ismile.ui.candidate.agentcandidate

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.db.dao.AgentCandidateDao
import co.id.ajsmsig.ismile.db.entity.minimal.ProfileAgentCandidateMinimal
import co.id.ajsmsig.ismile.model.api.CurrentStatusRequest
import co.id.ajsmsig.ismile.model.api.CurrentStatusResponse
import co.id.ajsmsig.ismile.model.api.StatusAgentCandidateApproveResponse
import co.id.ajsmsig.ismile.model.api.SubmitDetailedAgentCandidateRequest
import co.id.ajsmsig.ismile.util.PREFF_KEY_AGENT_CANDIDATE_CODE
import co.id.ajsmsig.ismile.util.PREF_KEY_AGENT_NAME
import co.id.ajsmsig.ismile.util.PREF_KEY_LOGIN
import co.id.ajsmsig.ismile.util.PREF_KEY_LOGIN_ROLE_TYPE
import io.reactivex.Observable
import okhttp3.ResponseBody
import javax.inject.Inject

class AgentCandidateRepository @Inject constructor(private val editor: SharedPreferences.Editor, private val dao: AgentCandidateDao, private var preferences: SharedPreferences, private var apiService: ApiService
) {

    fun getRegistrationId() = preferences.getString(PREFF_KEY_AGENT_CANDIDATE_CODE, null)
    fun getAgentCandidateName() = preferences.getString(PREF_KEY_AGENT_NAME, null)

    fun findCompletionStatus(registrationId: String) = dao.findCompletionStatus(registrationId)
    fun findStatementCompletionStatus(registrationId: String) = dao.findStatementCompletionStatus(registrationId)
    fun findSignatureCompletionStatus(registrationId: String) = dao.findSignatureCompletionStatus(registrationId)
    fun findLicenseCompletionStatus(registrationId: String)  = dao.findLicenseCompletionStatus(registrationId)

    fun clearSharedPreference() {
        editor.remove(PREF_KEY_LOGIN_ROLE_TYPE)
        editor.remove(PREFF_KEY_AGENT_CANDIDATE_CODE)
        editor.remove(PREF_KEY_AGENT_NAME)
        editor.remove(PREF_KEY_LOGIN)
        editor.clear()
        editor.apply()
    }

    fun findAllSavedData(registrationId: String) = dao.findAllSavedAnswer(registrationId)
    fun submitDetailedAgentCandidateData(request: SubmitDetailedAgentCandidateRequest) = apiService.submitDetailedAgentCandidateData(request)
    fun markAsSubmitted(registrationId: String, isSubmitted : Boolean) = dao.markAsSubmitted(registrationId, isSubmitted)

    suspend fun checkedStatusApprove (): StatusAgentCandidateApproveResponse{
        val registrationId = preferences.getString(PREFF_KEY_AGENT_CANDIDATE_CODE, null)
        return apiService.getStatusApprove(registrationId!!)
    }

    suspend fun getCurrentStatusSubmit () : CurrentStatusResponse {
        val registrationId = preferences.getString(PREFF_KEY_AGENT_CANDIDATE_CODE, null)
        return  apiService.currentStatusSubmitDate(CurrentStatusRequest(registrationId))
    }

    fun fetchAgencyLetter (registrationId: String): Observable <ResponseBody> {
        return apiService.downloadAgencyLetter(registrationId)
    }

    suspend fun checkedForAutoMatedLogout () : ProfileAgentCandidateMinimal {
        val userId = preferences.getString(PREFF_KEY_AGENT_CANDIDATE_CODE, null)
        return dao.checkedForAutoMatedLogout(userId!!)
    }
}