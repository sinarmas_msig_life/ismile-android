package co.id.ajsmsig.ismile.ui.candidate.licency

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.db.dao.AgentCandidateDao
import co.id.ajsmsig.ismile.db.entity.minimal.ExamInformationMinimal
import co.id.ajsmsig.ismile.db.entity.minimal.InfoSumbittedExamMinimal
import co.id.ajsmsig.ismile.di.NetManager
import co.id.ajsmsig.ismile.model.DetailLicense
import co.id.ajsmsig.ismile.model.InfoExamLicenseSubmitted
import co.id.ajsmsig.ismile.model.api.DetailLicenseResponse
import co.id.ajsmsig.ismile.model.api.LicenseCitiesResponse
import co.id.ajsmsig.ismile.model.api.SubmitExamDataRequest
import co.id.ajsmsig.ismile.model.api.SubmitExamDataResponse
import co.id.ajsmsig.ismile.util.PREFF_KEY_AGENT_CANDIDATE_CODE
import io.reactivex.Maybe
import io.reactivex.Observable
import javax.inject.Inject

class LicenseRepository @Inject constructor(private val preferences: SharedPreferences, private val agentCandidateDao: AgentCandidateDao, private val netManager: NetManager, private val apiService: ApiService) {

    val registrationId = preferences.getString(PREFF_KEY_AGENT_CANDIDATE_CODE, null)

    fun getSaveAgentCandidateCode() = preferences.getString(PREFF_KEY_AGENT_CANDIDATE_CODE, null)

    fun getAllCities (): Observable <List<LicenseCitiesResponse>>{
        return apiService.getCityLicense()
    }

    fun retrieveLicenseInfoSubmit (cities : String) : Observable <List<DetailLicenseResponse>> {
        return apiService.retrieveDetailLicense(cities)
    }

    fun saveUserSubmitted(data: DetailLicense, citiesId: Int, citiesName: String) {
        agentCandidateDao.licenseSubmitted(citiesId, citiesName, citiesId, data.placesName, data.examDate, data.examMethodId, data.examMethodName, data.examProductTypeId, data.examProductTypeName, data.examTypeId, data.examTypeName, data.noRek, data.banking, data.nominal, registrationId!!)
    }

    fun retrieveDataLicenceFromLocal() : Maybe<InfoSumbittedExamMinimal>{
        return agentCandidateDao.retrieveInfoSubmitted(registrationId!!)
    }

    fun retieveExamInformation() : Maybe <ExamInformationMinimal>{
        return agentCandidateDao.examInformation(registrationId!!)
    }

    fun submitExamLicense (bsbImage: String, dataSubmitted: InfoExamLicenseSubmitted): Observable <SubmitExamDataResponse>{
         return apiService.submittedExamData(SubmitExamDataRequest(bsbImage, dataSubmitted.examCityName!!, dataSubmitted.examSelectedDate!!, dataSubmitted.examMethodId!!, dataSubmitted.examPlaceName!!, dataSubmitted.examProductTypeId!!, dataSubmitted.examTypeId!!, registrationId!!))
    }
}