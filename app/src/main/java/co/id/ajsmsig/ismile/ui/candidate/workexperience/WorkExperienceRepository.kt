package co.id.ajsmsig.ismile.ui.candidate.workexperience

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.db.dao.AgentCandidateDao
import co.id.ajsmsig.ismile.db.entity.minimal.ViewWorkExperienceMinimal
import co.id.ajsmsig.ismile.util.PREFF_KEY_AGENT_CANDIDATE_CODE
import io.reactivex.Maybe
import javax.inject.Inject

class WorkExperienceRepository @Inject constructor(
    private var preferences: SharedPreferences,
    private val dao: AgentCandidateDao
) {
    fun addWorkExeperience(
        companyName: String,
        lastPosition: String,
        startDateWork: String,
        endDateWork: String,
        workExperienceDate: String,
        schoolId: Int,
        schoolName: String,
        personalDataComplete: Boolean
    ) {
        val userId = preferences.getString(PREFF_KEY_AGENT_CANDIDATE_CODE, null)
        dao.updateWorkExperience(companyName, lastPosition, startDateWork, endDateWork,workExperienceDate,schoolId, schoolName, personalDataComplete, userId!!)
    }

    fun getWorkExperience () : Maybe<ViewWorkExperienceMinimal> {
        val userId = preferences.getString(PREFF_KEY_AGENT_CANDIDATE_CODE, null)
        return dao.getWorkExperience(userId!!)
    }
}