package co.id.ajsmsig.ismile.model.uimodel

import co.id.ajsmsig.ismile.model.StatusApprove

data class StatusApprovalUiModel (val error:Boolean, val message: String, val status: List<StatusApprove> )