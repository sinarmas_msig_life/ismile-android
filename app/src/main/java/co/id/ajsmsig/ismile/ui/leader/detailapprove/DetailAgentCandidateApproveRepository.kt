package co.id.ajsmsig.ismile.ui.leader.detailapprove

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.di.NetManager
import co.id.ajsmsig.ismile.model.api.AgentLeaderApproveRequest
import co.id.ajsmsig.ismile.model.api.AgentLeaderApproveResponse
import co.id.ajsmsig.ismile.model.api.PreviewCandidateAgentResponse
import co.id.ajsmsig.ismile.util.PREF_KEY_AGENT_CODE
import io.reactivex.Observable
import javax.inject.Inject

class DetailAgentCandidateApproveRepository @Inject constructor(private val preferences: SharedPreferences, private val netManager: NetManager, private val apiService: ApiService) {

    fun getDetailAgentCandidate(registrationId: String): Observable<PreviewCandidateAgentResponse> {
        if (netManager.isConnectedToInternet){
            return apiService.getPreviewCandidateAgent(registrationId)
        }
        return Observable.just(PreviewCandidateAgentResponse
            (PreviewCandidateAgentResponse.Data("", "", "","","","","","","","","","","",""),true,"Mohon periksa kembali koneksi internet anda"))
    }

    private val agentLeaderId = preferences.getString( PREF_KEY_AGENT_CODE, null)

    fun submitApprovalLeader(registrationId: String, reasonRejection:String, statusSubmit:Boolean): Observable<AgentLeaderApproveResponse> {
        if (netManager.isConnectedToInternet){
            return apiService.agentLeaderSubmitApproval(AgentLeaderApproveRequest(agentLeaderId!!,  reasonRejection, registrationId, statusSubmit ))
        }
        return Observable.just(AgentLeaderApproveResponse(true, "Mohon periksa kembali koneksi internet anda"))
    }
}