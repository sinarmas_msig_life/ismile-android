package co.id.ajsmsig.ismile.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.id.ajsmsig.ismile.di.ViewModelFactory
import co.id.ajsmsig.ismile.di.ViewModelKey
import co.id.ajsmsig.ismile.ui.agent.addagentcandidate.AddAgentCandidateViewModel
import co.id.ajsmsig.ismile.ui.candidate.agentcandidate.AgentCandidateViewModel
import co.id.ajsmsig.ismile.ui.leader.agentleader.AgentLeaderViewModel
import co.id.ajsmsig.ismile.ui.leader.detailapprove.DetailAgentCandidateApproveViewModel
import co.id.ajsmsig.ismile.ui.agent.forgotpasswordagent.ForgotPasswordAgentViewModel
import co.id.ajsmsig.ismile.ui.candidate.forgotpasswordagentcandidate.ForgotPasswordAgentCandidateViewModel
import co.id.ajsmsig.ismile.ui.agent.home.HomeViewModel
import co.id.ajsmsig.ismile.ui.candidate.licency.LicenseViewModel
import co.id.ajsmsig.ismile.ui.agent.login.LoginViewModel
import co.id.ajsmsig.ismile.ui.candidate.logincandidateagent.LoginCandidateAgentViewModel
import co.id.ajsmsig.ismile.ui.agent.logintype.ChooseLoginTypeViewModel
import co.id.ajsmsig.ismile.ui.agent.notification.NotificationViewModel
import co.id.ajsmsig.ismile.ui.agent.previewcandidateagent.PreviewCandidateAgentViewModel
import co.id.ajsmsig.ismile.ui.candidate.profileagentcandidate.ProfileAgentCandidateViewModel
import co.id.ajsmsig.ismile.ui.agent.recruit.RecruitViewModel
import co.id.ajsmsig.ismile.ui.agent.recruitfailed.RecruitFailedViewModel
import co.id.ajsmsig.ismile.ui.agent.recruitsuccess.RecruitSuccessViewModel
import co.id.ajsmsig.ismile.ui.candidate.resetpasswordagentcandidate.InfoResetPasswordAgentCandidateViewModel
import co.id.ajsmsig.ismile.ui.candidate.resetpasswordsucces.ResetPasswordSuccesViewModel
import co.id.ajsmsig.ismile.ui.candidate.signature.SignatureAgentCandidateViewModel
import co.id.ajsmsig.ismile.ui.splashfragment.SplashViewModel
import co.id.ajsmsig.ismile.ui.candidate.statement.StatementViewModel
import co.id.ajsmsig.ismile.ui.candidate.submitlicense.LicenseSubmitViewModel
import co.id.ajsmsig.ismile.ui.candidate.workexperience.WorkExperienceViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChooseLoginTypeViewModel::class)
    internal abstract fun bindChooseLoginTypeViewModel(viewModel: ChooseLoginTypeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddAgentCandidateViewModel::class)
    internal abstract fun bindAddAgentCandidateViewModel(viewModel: AddAgentCandidateViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (AgentCandidateViewModel::class)
    internal abstract fun bindCandidateAgentViewModel(viewModel: AgentCandidateViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (ProfileAgentCandidateViewModel::class)
    internal abstract fun bindProfileAgentViewModel(viewModel: ProfileAgentCandidateViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (StatementViewModel::class)
    internal abstract fun bindInputStatmentViewModel(viewModel: StatementViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (LoginCandidateAgentViewModel::class)
    internal abstract fun bindLoginCandidateAgentViewModel(viewModel: LoginCandidateAgentViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (NotificationViewModel::class)
    internal abstract fun bindNotificationViewModel(viewModel: NotificationViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (ForgotPasswordAgentCandidateViewModel::class)
    internal abstract fun bindForgotPasswordAgentCandidateViewModel(viewModel: ForgotPasswordAgentCandidateViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (RecruitViewModel::class)
    internal abstract fun bindRecruitViewModel(viewModel: RecruitViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (RecruitFailedViewModel::class)
    internal abstract fun bindRecruitFailedViewModel(viewModel: RecruitFailedViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (RecruitSuccessViewModel::class)
    internal abstract fun bindRecruitSuccessViewModel(viewModel: RecruitSuccessViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (ForgotPasswordAgentViewModel::class)
    internal abstract fun bindForgotPasswordAgentViewModel(viewModel: ForgotPasswordAgentViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (ResetPasswordSuccesViewModel::class)
    internal abstract fun bindResetPasswordSuccesViewModel(viewModel: ResetPasswordSuccesViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (PreviewCandidateAgentViewModel::class)
    internal abstract fun bindPreviewCandidateAgentViewModel(viewModel: PreviewCandidateAgentViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (SplashViewModel::class)
    internal abstract fun bindSplashViewModel(viewModel: SplashViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (InfoResetPasswordAgentCandidateViewModel::class)
    internal abstract fun bindInfoResetPasswordAgentCandidateViewModel(viewModel: InfoResetPasswordAgentCandidateViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (WorkExperienceViewModel::class)
    internal abstract fun bindWorkExperienceViewModel(viewModel: WorkExperienceViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (SignatureAgentCandidateViewModel::class)
    internal abstract fun bindSignatureAgentCandidateViewModel(viewModel: SignatureAgentCandidateViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (AgentLeaderViewModel::class)
    internal abstract fun bindAgentLeaderViewModel(viewModel: AgentLeaderViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (DetailAgentCandidateApproveViewModel::class)
    internal abstract fun bindDetailAgentCandidateApproveViewModel(viewModel: DetailAgentCandidateApproveViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (LicenseViewModel::class)
    internal abstract fun bindLicenseViewModel(viewModel: LicenseViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey (LicenseSubmitViewModel::class)
    internal abstract fun bindLicenseSubmitViewModel(viewModel: LicenseSubmitViewModel):ViewModel

}