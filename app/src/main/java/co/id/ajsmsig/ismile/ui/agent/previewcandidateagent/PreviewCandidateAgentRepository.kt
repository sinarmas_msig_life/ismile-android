package co.id.ajsmsig.ismile.ui.previewagentcandidate

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.di.NetManager
import co.id.ajsmsig.ismile.model.api.PreviewCandidateAgentResponse
import co.id.ajsmsig.ismile.util.PREF_KEY_AGENT_CODE
import io.reactivex.Observable
import javax.inject.Inject

class PreviewCandidateAgentRepository @Inject constructor(private val preferences: SharedPreferences,private val netManager: NetManager,private val apiService: ApiService) {

    fun getDataCandidateAgent(registrationId:String):Observable<PreviewCandidateAgentResponse>{
        if (netManager.isConnectedToInternet){
            return apiService.getPreviewCandidateAgent(registrationId)
        }

        return Observable.just(
            PreviewCandidateAgentResponse(
            PreviewCandidateAgentResponse.Data("","","","","","","","", "","","","","",""),
            true,
            "Mohon periksa kembali koneksi internet")
        )
    }
}