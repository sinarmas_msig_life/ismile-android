package co.id.ajsmsig.ismile.model

data class AppVersionRequest(
    val flag_app: Int,
    val flag_platform: Int
)