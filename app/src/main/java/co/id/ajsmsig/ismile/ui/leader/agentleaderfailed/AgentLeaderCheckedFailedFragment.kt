package co.id.ajsmsig.ismile.ui.leader.agentleaderfailed

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentAgentLeaderCheckedFailedBinding
import co.id.ajsmsig.ismile.model.StatusApprove
import co.id.ajsmsig.ismile.ui.leader.agentleader.AgentLeaderFragmentDirections
import co.id.ajsmsig.ismile.ui.leader.agentleader.AgentLeaderViewModel
import co.id.ajsmsig.ismile.util.MENU_LEADER_REJECTED

class AgentLeaderCheckedFailedFragment : BaseFragment <FragmentAgentLeaderCheckedFailedBinding, AgentLeaderViewModel>(),
    AgentLeaderCheckedFailedAdapter.onAgentCandidateFailedListener{

    override fun getLayoutResourceId() = R.layout.fragment_agent_leader_checked_failed

    override fun getViewModelClass() = AgentLeaderViewModel::class.java

    private val adapter = AgentLeaderCheckedFailedAdapter(listOf(), this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()

        getViewModel().getAllApprovalAgentCandidate(2)
        getViewModel().isStatusApprove.observe(this, Observer {
            it?.let {
                if (!it.error){
                    getDataBinding().swpRefreshFailed.isRefreshing = false
                    refreshData(it.status)
                }else {
                    refreshData(emptyList())
                    getDataBinding().swpRefreshFailed.isRefreshing = false
                }
            }
        })

        getDataBinding().swpRefreshFailed.setOnRefreshListener {
            getViewModel().getAllApprovalAgentCandidate(2)
        }
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(activity)
        val dividerItemDecoration =
            DividerItemDecoration(getDataBinding().recyclerView.context, layoutManager.orientation)

        getDataBinding().recyclerView.layoutManager = layoutManager
        getDataBinding().recyclerView.adapter = adapter
        getDataBinding().recyclerView.addItemDecoration(dividerItemDecoration)

    }

    override fun onRecruitPressed(candidateAgent: StatusApprove, position: Int) {
        val action = AgentLeaderFragmentDirections.actionLaunchDetailAgentLeaderApproveFragment(candidateAgent.registrationId, MENU_LEADER_REJECTED)
        findNavController().navigate(action)
    }

    private fun refreshData(recruits: List<StatusApprove>) {
        getDataBinding().swpRefreshFailed.isRefreshing = false
        adapter.refreshData(recruits)
    }

}
