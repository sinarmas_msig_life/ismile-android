package co.id.ajsmsig.ismile.ui.candidate.signature

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.db.dao.AgentCandidateDao
import co.id.ajsmsig.ismile.db.entity.minimal.SigantureAgentCandidateMinimal
import co.id.ajsmsig.ismile.util.PREFF_KEY_AGENT_CANDIDATE_CODE
import io.reactivex.Observable

class SignatureAgentCandidateRepository(private val preferences: SharedPreferences, private val dao: AgentCandidateDao) {

    val signatureComplete = true

    fun updateSignatureAgentCandidate(signatureChecked: Boolean, signatureImgPath: String, signatureDate: String) {
        val userId=preferences.getString(PREFF_KEY_AGENT_CANDIDATE_CODE,null)
        dao.updateSignatureAgentCandidate(signatureChecked,signatureImgPath,signatureDate,signatureComplete,userId!!)
    }

    fun getSignAtureAgentCandidate() : Observable<SigantureAgentCandidateMinimal> {
        val userId=preferences.getString(PREFF_KEY_AGENT_CANDIDATE_CODE,null)
        return dao.getInfoSignature(userId!!)
    }
}