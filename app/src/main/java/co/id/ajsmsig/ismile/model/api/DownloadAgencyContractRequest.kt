package co.id.ajsmsig.ismile.model.api

import com.google.gson.annotations.SerializedName

data class DownloadAgencyContractRequest(
    @SerializedName("registration_id")
    val registrationId : String,
    val birthplace : String,
    val app: String = "SMiLe go!",
    val title : String,
    val address : String
)