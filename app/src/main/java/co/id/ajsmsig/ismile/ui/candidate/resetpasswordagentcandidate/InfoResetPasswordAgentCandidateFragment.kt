package co.id.ajsmsig.ismile.ui.candidate.resetpasswordagentcandidate

import android.os.Bundle
import android.view.View
import androidx.databinding.ObservableField
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentInfoResetPasswordAgentCandidateBinding

class InfoResetPasswordAgentCandidateFragment : BaseFragment<FragmentInfoResetPasswordAgentCandidateBinding, InfoResetPasswordAgentCandidateViewModel>() {

    val labelInfoSucces = ObservableField<String>()

    override fun getLayoutResourceId() = R.layout.fragment_info_reset_password_agent_candidate

    override fun getViewModelClass() = InfoResetPasswordAgentCandidateViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getDataBinding().fragment = this

        getMessage()
    }

    fun onLoginClick() {
        val action = InfoResetPasswordAgentCandidateFragmentDirections.actionLaunchLoginFragment()
        findNavController().navigate(action)
    }

    private fun getMessage() {
        val message = InfoResetPasswordAgentCandidateFragmentArgs.fromBundle(arguments!!).message
        labelInfoSucces.set(message)
    }

}
