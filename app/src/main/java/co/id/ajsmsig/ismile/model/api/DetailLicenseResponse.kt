package co.id.ajsmsig.ismile.model.api


import com.google.gson.annotations.SerializedName

data class DetailLicenseResponse (
    @SerializedName("exam_place")
    val examPlace: String,
    @SerializedName("exam_date")
    val examDate: String,
    @SerializedName("exam_method")
    val examMethod: Int,
    @SerializedName("exam_method_name")
    val examMethodName: String,
    @SerializedName("exam_product_type")
    val examProductType: Int,
    @SerializedName("exam_product_name")
    val examProductName: String,
    @SerializedName("exam_type_id")
    val examType: Int,
    @SerializedName("exam_type_name")
    val examTypeName: String,
    @SerializedName("banking")
    val banking: String,
    @SerializedName("no_rek")
    val noRek: String,
    @SerializedName("price")
    val price: String
)