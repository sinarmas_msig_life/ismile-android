package co.id.ajsmsig.ismile.model

data class ProfileCandidateView (
    val fullName: String?,
    val bod: String?,
    val idCardNo: String?,
    val phoneNo: String?,
    val email: String?,
    val titleName: String?,
    val bopDate: String?,
    val maritalStatus: String?,
    val genderName: String?
)