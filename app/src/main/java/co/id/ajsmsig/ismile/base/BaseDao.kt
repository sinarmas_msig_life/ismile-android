package co.id.ajsmsig.ismile.base

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Single

interface BaseDao<T> {

    /**
     * Insert an object in the database.
     *
     * @param entity the object to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: T) : Single<Long>


    /**
     * Insert an array of objects in the database.
     *
     * OnConflict strategy constant to abort the transaction.
     *
     * @param entity the objects to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(vararg entity: T) : Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(entities : List<T>) : Completable
    /**
     * Update an object from the database.
     *
     * @param entity the object to be updated
     */
    @Update
    fun update(entity: T)

    /**
     * Delete an object from the database
     *
     * @param entity the object to be deleted
     */

    @Delete
    fun delete(entity: T)
}