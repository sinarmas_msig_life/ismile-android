package co.id.ajsmsig.ismile.model.uimodel

data class LoginAgentCandidateUiModel (val error:Boolean, val message:String, val registrationId : String)