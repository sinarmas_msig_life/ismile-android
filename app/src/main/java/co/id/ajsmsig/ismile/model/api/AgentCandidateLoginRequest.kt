package co.id.ajsmsig.ismile.model.api

import com.google.gson.annotations.SerializedName

data class AgentCandidateLoginRequest(
    @SerializedName("userid")
    val userid: String,
    @SerializedName("password")
    val password: String
)