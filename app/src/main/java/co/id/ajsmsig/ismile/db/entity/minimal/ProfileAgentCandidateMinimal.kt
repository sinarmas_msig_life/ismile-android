package co.id.ajsmsig.ismile.db.entity.minimal

import androidx.room.ColumnInfo

data class ProfileAgentCandidateMinimal(
    @ColumnInfo (name = "name")
    var fullName : String?,

    @ColumnInfo (name = "identity_number")
    var identityNumber:String?,

    @ColumnInfo (name = "birth_of_date")
    var birthOfDate : String?,

    @ColumnInfo (name = "marital_status")
    var maritalStatus : String?,

    @ColumnInfo ( name = "phone_number")
    var phoneNumber : String?,

    @ColumnInfo(name = "title_name")
    var titleName: String?,

    @ColumnInfo (name = "bop_date")
    var bopDate : String?,

    @ColumnInfo(name = "email")
    var email: String?,

    @ColumnInfo(name = "gender_id")
    var genderId: Int?
)