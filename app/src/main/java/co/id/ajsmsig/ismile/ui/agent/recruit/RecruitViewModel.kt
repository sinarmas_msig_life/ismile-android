package co.id.ajsmsig.ismile.ui.agent.recruit

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.SingleLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.common.ResultState
import co.id.ajsmsig.ismile.model.StatusRecruit
import co.id.ajsmsig.ismile.model.api.StatusRecruitResponse
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class RecruitViewModel @Inject constructor(private val repository: RecruitRepository) : BaseViewModel() {

    private val _recruits = SingleLiveData<ResultState<List<StatusRecruit>>>()
    val recruits: LiveData<ResultState<List<StatusRecruit>>>
        get() = _recruits

    fun getAllRecruit(requestCode: Int) {
        setIsLoading(true)
        viewModelScope.launch (IO){
            try {
                val result = repository.getAllRecruit(requestCode)
                setIsLoading(false)
                if (result.error){
                    setResultRecruits(ResultState.Error(R.string.unknown_error))
                    return@launch
                }
                if (result.data.allRecruit.isEmpty()){
                    setResultRecruits(ResultState.NoData())
                    return@launch
                }
                val data  = transformData(result.data.allRecruit)
                setResultRecruits(ResultState.HasData(data))
            }catch (e: Throwable){
                setIsLoading(false)
                when (e) {
                    is IOException -> setResultRecruits(ResultState.NoInternetConnection())
                    is TimeoutException -> setResultRecruits(ResultState.TimeOut(R.string.timeout))
                    else -> setResultRecruits(ResultState.Error(R.string.unknown_error))
                }
            }
        }
    }

    private fun transformData (data: List<StatusRecruitResponse.Data.AllRecruit>) : List<StatusRecruit> {
        val recruits = mutableListOf<StatusRecruit>()
        for (i in data) {
            recruits.add(
                StatusRecruit(
                    i.agentCandidateRegistrationId,
                    i.candidateName,
                    i.latestStatus,
                    i.profpicImagePath,
                    i.statusDate,
                    i.submitDate
                )
            )
        }
        return recruits
    }

    private fun setResultRecruits(data: ResultState<List<StatusRecruit>>){
        _recruits.sendActionOnBackground(data)
    }
}