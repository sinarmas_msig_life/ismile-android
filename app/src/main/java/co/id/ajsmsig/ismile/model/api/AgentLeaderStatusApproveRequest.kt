package co.id.ajsmsig.ismile.model.api


import com.google.gson.annotations.SerializedName

data class AgentLeaderStatusApproveRequest(
    @SerializedName("msag_id")
    val msagId: String,
    @SerializedName("request_code")
    val requestCode: Int
)