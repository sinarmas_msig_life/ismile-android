package co.id.ajsmsig.ismile.ui.leader.agentleaderapprove

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentAgentLeaderCheckedApprovalBinding
import co.id.ajsmsig.ismile.model.StatusApprove
import co.id.ajsmsig.ismile.ui.leader.agentleader.AgentLeaderFragmentDirections
import co.id.ajsmsig.ismile.ui.leader.agentleader.AgentLeaderViewModel
import co.id.ajsmsig.ismile.util.MENU_LEADER_REQUSTED

class AgentLeaderCheckedApprovalFragment :
    BaseFragment<FragmentAgentLeaderCheckedApprovalBinding, AgentLeaderViewModel>(),
    AgentLeaderApproveRecyclerAdapter.onLeaderAppproveAgentCandidateListener {

    override fun getLayoutResourceId() = R.layout.fragment_agent_leader_checked_approval

    override fun getViewModelClass() = AgentLeaderViewModel::class.java

    private val adapter = AgentLeaderApproveRecyclerAdapter(listOf(), this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()

        getViewModel().getAllApprovalAgentCandidate(0)
        getViewModel().isStatusApprove.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (!it.error){
                    getDataBinding().swpRefreshApprove.isRefreshing= false
                    refreshData(it.status)
                }else{
                    refreshData(emptyList())
                    getDataBinding().swpRefreshApprove.isRefreshing = false
                }
            }
        })

        getViewModel().isApproveSucces.observe( viewLifecycleOwner, Observer {
            it?.let {
                if (it.error){
                    getDataBinding().swpRefreshApprove.isRefreshing= true
                    getViewModel().getAllApprovalAgentCandidate(0)
                    snackbar(it.message!!)
                }else {
                    getViewModel().getAllApprovalAgentCandidate(0)
                    snackbar(it.message!!)
                }
            }
        })

        getDataBinding().swpRefreshApprove.setOnRefreshListener {
            getViewModel().getAllApprovalAgentCandidate(0)
        }
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(activity)
        val dividerItemDecoration =
            DividerItemDecoration(getDataBinding().recyclerView.context, layoutManager.orientation)

        getDataBinding().recyclerView.layoutManager = layoutManager
        getDataBinding().recyclerView.adapter = adapter
        getDataBinding().recyclerView.addItemDecoration(dividerItemDecoration)

    }

    override fun onRecruitPressed(candidateAgent: StatusApprove, position: Int) {
        launchDetailLeaderApprove(candidateAgent.registrationId)
    }

    override fun onAgentCandidateApprove(registrationId: String, position: Int) {
        val reasonReject = ""
        getViewModel().agentLeaderAgrementToApprove(registrationId, reasonReject, true )
    }

    override fun onAgentCandudateReject(registrationId: String) {
        val statusSubmit = false
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(R.string.rejection)

        val view = layoutInflater.inflate(R.layout.custom_reject_reason_approve, null)
        val reasonRejection = getDataBinding().customRejectReason.etReasonReject.text

        builder.setView(view)
            .setNegativeButton(android.R.string.no) {
                    dialogInterface, i -> dialogInterface.dismiss() }
            .setPositiveButton(android.R.string.ok) { _, _ ->
                getViewModel().agentLeaderAgrementToApprove(registrationId, reasonRejection.toString() , statusSubmit)
            }
            .show()


    }

    private fun refreshData(recruits: List<StatusApprove>) {
        adapter.refreshData(recruits)
        getDataBinding().swpRefreshApprove.isRefreshing = false
    }

    fun launchDetailLeaderApprove (registrationId : String){
        val action = AgentLeaderFragmentDirections.actionLaunchDetailAgentLeaderApproveFragment(registrationId, MENU_LEADER_REQUSTED)
        findNavController().navigate(action)
    }
}
