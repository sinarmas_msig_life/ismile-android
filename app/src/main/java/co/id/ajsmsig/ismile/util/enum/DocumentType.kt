package co.id.ajsmsig.ismile.util.enum

enum class DocumentType {
    FOTO_KTP,
    FOTO_PROFIL,
    FOTO_NPWP,
    FOTO_HALAMAN_DEPAN_BUKU_TABUNGAN
}