package co.id.ajsmsig.ismile.model.uimodel

data class CandidateAgentSubmitUiModel(
    val isError : Boolean,
    val message : String
)