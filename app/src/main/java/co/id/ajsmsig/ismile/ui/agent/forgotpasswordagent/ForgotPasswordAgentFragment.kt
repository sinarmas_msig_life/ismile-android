package co.id.ajsmsig.ismile.ui.agent.forgotpasswordagent

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentForgotPasswordAgentBinding

class ForgotPasswordAgentFragment : BaseFragment<FragmentForgotPasswordAgentBinding, ForgotPasswordAgentViewModel>() {

    override fun getLayoutResourceId() = R.layout.fragment_forgot_password_agent
    override fun getViewModelClass() = ForgotPasswordAgentViewModel::class.java

    var loginType: Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loginType = ForgotPasswordAgentFragmentArgs.fromBundle(arguments!!).isLeader
        vm.setLoginType(loginType)

        vm.isFindAccountSucces.observe(this, Observer {
            it?.let {
                if (!it.error) {
                    snackbar(it.message!!)
                    launchNotificationFragment(it.message)
                } else {
                    snackbar(it.message!!)
                }
            }
        })
    }

    private fun launchNotificationFragment(message: String) {
        val action = ForgotPasswordAgentFragmentDirections.actionNotificationForgotLaunch()
        action.setMessage(message)
        findNavController().navigate(action)
    }

    fun onSendPressed() {
        vm.validateUserInput(loginType)
    }

}
