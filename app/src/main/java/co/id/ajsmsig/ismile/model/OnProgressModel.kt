package co.id.ajsmsig.ismile.model

data class OnProgressModel(val isFinished : Boolean, val message : String?)