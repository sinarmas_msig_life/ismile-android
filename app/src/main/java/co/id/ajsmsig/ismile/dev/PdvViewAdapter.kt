package co.id.ajsmsig.ismile.dev

import android.graphics.Bitmap
import android.graphics.pdf.PdfRenderer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import co.id.ajsmsig.ismile.R

class PdvViewAdapter (private var renderer: PdfRenderer, private val pageWidth: Int) : RecyclerView.Adapter<PdvViewAdapter.ViewHolder>() {

    class ViewHolder(private var view: View) : RecyclerView.ViewHolder(view) {

        fun bind(bitmap: Bitmap) {
            val imageView =  view.findViewById<ImageView>(R.id.ivPdfViewer)
            imageView.setImageBitmap(bitmap)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_pdf_page, parent, false))

    override fun getItemCount() = renderer.pageCount

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(renderer.openPage(position).renderAndClose(pageWidth))

    fun refreshAdapter( renderer: PdfRenderer){
        this.renderer = renderer
        notifyDataSetChanged()
    }
}