package co.id.ajsmsig.ismile.di.module

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.db.dao.AgentCandidateDao
import co.id.ajsmsig.ismile.db.iSmileDatabase
import co.id.ajsmsig.ismile.di.NetManager
import co.id.ajsmsig.ismile.ui.agent.addagentcandidate.AddAgentCandidateRemoteDataSource
import co.id.ajsmsig.ismile.ui.agent.addagentcandidate.AddAgentCandidateRepository
import co.id.ajsmsig.ismile.ui.candidate.agentcandidate.AgentCandidateRepository
import co.id.ajsmsig.ismile.ui.leader.agentleader.AgentLeaderRepository
import co.id.ajsmsig.ismile.ui.leader.detailapprove.DetailAgentCandidateApproveRepository
import co.id.ajsmsig.ismile.ui.agent.forgotpasswordagent.ForgotPasswordAgentRepository
import co.id.ajsmsig.ismile.ui.candidate.licency.LicenseRepository
import co.id.ajsmsig.ismile.ui.agent.login.LoginLocalDataSource
import co.id.ajsmsig.ismile.ui.agent.login.LoginRemoteDataSource
import co.id.ajsmsig.ismile.ui.agent.login.LoginRepository
import co.id.ajsmsig.ismile.ui.agent.notification.NotificationRepository
import co.id.ajsmsig.ismile.ui.previewagentcandidate.PreviewCandidateAgentRepository
import co.id.ajsmsig.ismile.ui.candidate.profileagentcandidate.ProfileAgentCandidateRepositroy
import co.id.ajsmsig.ismile.ui.agent.recruit.RecruitRepository
import co.id.ajsmsig.ismile.ui.candidate.signature.SignatureAgentCandidateRepository
import co.id.ajsmsig.ismile.ui.splashfragment.SplashLocalDataSource
import co.id.ajsmsig.ismile.ui.splashfragment.SplashRepository
import co.id.ajsmsig.ismile.ui.candidate.workexperience.WorkExperienceRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun providesAgentCandidateDao(db: iSmileDatabase) = db.agentCandidateDao()

    @Provides
    @Singleton
    fun providesQuestionnaireDao(db: iSmileDatabase) = db.questionnaireDao()

    @Provides
    @Singleton
    fun providesQuestionnaireDetailDao(db: iSmileDatabase) = db.questionnaireDetailDao()


    //Local data source
    @Provides
    @Singleton
    fun providesLoginLocalDataSource(editor: SharedPreferences.Editor, preferences: SharedPreferences) =
        LoginLocalDataSource(editor, preferences)

    //Local data source
    @Provides
    @Singleton
    fun providesSplashLocalDataSource(editor: SharedPreferences.Editor, preferences: SharedPreferences) =
        SplashLocalDataSource(editor, preferences)

    @Provides
    @Singleton
    fun providesLoginRemoteDataSource(apiService: ApiService) = LoginRemoteDataSource(apiService)

    //Repository
    @Provides
    @Singleton
    fun provideLoginRepository(
        localDataSource: LoginLocalDataSource,
        remoteDataSource: LoginRemoteDataSource,
        netManager: NetManager,
        preferences: SharedPreferences,
        apiService: ApiService
    ) = LoginRepository(localDataSource, remoteDataSource, netManager, preferences, apiService)


    @Provides
    @Singleton
    fun providesAddAgentCandidateRemoteDataSource(apiService: ApiService) =
        AddAgentCandidateRemoteDataSource(apiService)

    //Repository
    @Provides
    @Singleton
    fun provideAddAgentCandidateRepository(
        remoteDataSource: AddAgentCandidateRemoteDataSource,
        netManager: NetManager,
        preferences: SharedPreferences
    ) = AddAgentCandidateRepository(remoteDataSource, netManager, preferences)

    @Provides
    @Singleton
    fun providesForgotPasswordAgentRepository(
        editor: SharedPreferences.Editor,
        preferences: SharedPreferences,
        netManager: NetManager,
        apiService: ApiService
    ) = ForgotPasswordAgentRepository(editor, preferences, netManager, apiService)

    @Provides
    @Singleton
    fun providesRecruitRepository(apiService: ApiService, netManager: NetManager, preferences: SharedPreferences) =
        RecruitRepository(apiService, netManager, preferences)

    @Provides
    @Singleton
    fun providesPreviewCandidateAgentRepository(
        preferences: SharedPreferences,
        netManager: NetManager,
        apiService: ApiService
    ) = PreviewCandidateAgentRepository(preferences, netManager, apiService)

    @Provides
    @Singleton
    fun providesNotificationRepository(preferences: SharedPreferences, netManager: NetManager, apiService: ApiService) =
        NotificationRepository(preferences, netManager, apiService)

    @Provides
    @Singleton
    fun providesSplashRepository(localDataSource: SplashLocalDataSource, apiService: ApiService) = SplashRepository(localDataSource, apiService)


    @Provides
    @Singleton
    fun providesCandidateAgentRepository(
        agentCandidateDao: AgentCandidateDao,
        editor: SharedPreferences.Editor,
        preferences: SharedPreferences,
        apiService: ApiService
    ) = AgentCandidateRepository(editor,agentCandidateDao, preferences, apiService)

    @Provides
    @Singleton
    fun providesProfileAgentCandidateRepositroy(
        preferences: SharedPreferences,
        agentCandidateDao:AgentCandidateDao,
        netManager: NetManager,
        apiService: ApiService
    ) =ProfileAgentCandidateRepositroy(preferences,agentCandidateDao,netManager,apiService)

    @Provides
    @Singleton
    fun providesWorkExperienceRepository(
        preferences: SharedPreferences,
        agentCandidateDao:AgentCandidateDao
    ) =WorkExperienceRepository(preferences,agentCandidateDao)

    @Provides
    @Singleton
    fun providesSignatureAgentCandidateViewModel(
        preferences: SharedPreferences,
        agentCandidateDao:AgentCandidateDao
    ) =SignatureAgentCandidateRepository(preferences,agentCandidateDao)


    @Provides
    @Singleton
    fun providesAgentLeaderRepository(
        editor: SharedPreferences.Editor,
        preferences: SharedPreferences,
        netManager: NetManager,
        apiService: ApiService
    ) = AgentLeaderRepository(editor, preferences, netManager, apiService)

    @Provides
    @Singleton
    fun providesDetailAgentCandidateApproveRepository(preferences: SharedPreferences, netManager: NetManager, apiService: ApiService) = DetailAgentCandidateApproveRepository( preferences, netManager, apiService)

    @Provides
    @Singleton
    fun providesLicenseRepository(preferences: SharedPreferences, agentCandidateDao: AgentCandidateDao,netManager: NetManager, apiService: ApiService) = LicenseRepository( preferences,agentCandidateDao, netManager, apiService)

}