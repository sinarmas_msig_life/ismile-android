package co.id.ajsmsig.ismile.ui.agent.login

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.di.NetManager
import co.id.ajsmsig.ismile.model.api.AgentLoginResponse
import co.id.ajsmsig.ismile.model.api.SaveTokenNotificationRequest
import co.id.ajsmsig.ismile.model.api.SaveTokenNotificationResponse
import co.id.ajsmsig.ismile.util.*
import io.reactivex.Observable
import javax.inject.Inject

class LoginRepository @Inject constructor(
        private val localDataSource: LoginLocalDataSource,
        private val remoteDataSource: LoginRemoteDataSource,
        private val netManager: NetManager,
        private val preferences: SharedPreferences,
        private val apiService: ApiService
) {

    fun loginAgent(username: String, password: String): Observable<AgentLoginResponse> {
        if (netManager.isConnectedToInternet) {
            return remoteDataSource.login(username, password)
        }

        return Observable.just(
                AgentLoginResponse(
                        AgentLoginResponse.Data("", "", "", -1, false),
                        true,
                        "Login Gagal. Harap periksa kembali koneksi internet")
        )
    }

    fun clearSharedPreference() = localDataSource.clearSharedPreference()

    fun saveUserSession(agentCode: String, agentName: String, agentRoleType: Int, agentTitle: String, agentUserId : String) {
        localDataSource.saveAgentCode(agentCode)
        localDataSource.saveAgentName(agentName)
        localDataSource.setLoggedIn(true)
        localDataSource.saveLoginRoleType(agentRoleType)
        localDataSource.saveAgentTitle(agentTitle)
        localDataSource.saveAgentUserId(agentUserId)
    }


    fun getSavedAgentCode() = localDataSource.getSavedAgentCode()

    fun getSavedAgentName() = localDataSource.getSavedAgentName()

    fun getSavedAgentTitle() = localDataSource.getSavedAgentTitle()

    fun saveTokenNotification(token:String) : Observable<SaveTokenNotificationResponse>{
        val jenis_id= LAUNCH_APP_ID
        val userId=preferences.getString(PREF_KEY_AGENT_USER_ID,null)

        if (netManager.isConnectedToInternet){
            return apiService.saveTokenNotificication(SaveTokenNotificationRequest(jenis_id,token,userId))
        }
        return Observable.just(SaveTokenNotificationResponse(true,"Mohon periksa kembali koneksi internet"))
    }

}