package co.id.ajsmsig.ismile.ui.agent.notification

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.model.NotificationModel
import co.id.ajsmsig.ismile.model.uimodel.NotificationUiModel
import co.id.ajsmsig.ismile.util.DateUtils
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class NotificationViewModel @Inject constructor(private val repository: NotificationRepository) : BaseViewModel() {

    val isInboxEmpty = ObservableBoolean(true)

    private val _inbox = MutableLiveData<NotificationUiModel>()
    val inbox: LiveData<NotificationUiModel>
        get() = _inbox

    private val _shouldLogout = MutableLiveData<Boolean>()
    val shouldLogout: LiveData<Boolean>
        get() = _shouldLogout

    fun getInbox() {
        if (isValidUserId()) {
            setShouldLogout(false)
            mCompositeDisposable += repository.getNotification(repository.getLoggedInUserId()!!)
                .map {
                    val messages = ArrayList<NotificationModel>()
                    for (i in it.data) {
                        messages.add(NotificationModel(i.title, i.message, DateUtils.getReadableDateFrom(i.createdDate)!!, i.parameter?.nextActionMenuId))
                    }
                    NotificationUiModel(it.error, it.message, messages)
                }
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { setIsLoading(true) }
                .doOnTerminate { setIsLoading(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { data -> onRetrieveInboxSuccess(data) },
                    { throwable -> onRetrieveInboxFailed(throwable) }
                )
        } else {
            setShouldLogout(true)
        }
    }

    private fun onRetrieveInboxSuccess(data: NotificationUiModel) {

        if (data.data.isEmpty()) {
            isInboxEmpty.set(true)
        } else {
            isInboxEmpty.set(false)
        }
        _inbox.value = data

    }

    private fun onRetrieveInboxFailed(throwable: Throwable?) {
        _inbox.value = NotificationUiModel(true, "Terjadi kesalahan. Mohon coba kembali", emptyList())
    }

    private fun isValidUserId(): Boolean {
        val savedUserId = repository.getLoggedInUserId()

        if (!savedUserId.isNullOrEmpty()) {
            return true
        }

        return false
    }

    private fun setShouldLogout(shouldLogout : Boolean) {
        _shouldLogout.value = shouldLogout
    }


}