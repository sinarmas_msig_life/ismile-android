package co.id.ajsmsig.ismile.model

data class SuccesMessage (val isSucces:Boolean, val message: String)