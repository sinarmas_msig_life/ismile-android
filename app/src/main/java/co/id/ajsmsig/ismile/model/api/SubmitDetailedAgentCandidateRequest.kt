package co.id.ajsmsig.ismile.model.api


import com.google.gson.annotations.SerializedName

data class SubmitDetailedAgentCandidateRequest(
    @SerializedName("personal_data")
    val personalData: PersonalData,
    @SerializedName("registration_id")
    val registrationId: String,
    @SerializedName("signature")
    val signature: Signature,
    @SerializedName("statement")
    val statement: Statement
) {
    data class Statement(
        @SerializedName("questionnaire")
        val questionnaire: List<Questionnaire>
    ) {
        data class Questionnaire(
            @SerializedName("id")
            val id: Long?,
            @SerializedName("optional_answer")
            val optionalAnswer: List<OptionalAnswer>,
            @SerializedName("question_type")
            val questionType: Int?
        )
        data class OptionalAnswer(
            @SerializedName("id")
            val id : Long,
            @SerializedName("label")
            val label : String?,
            @SerializedName("answer")
            val answer : String?
        )
    }

    data class PersonalData(
        @SerializedName("address")
        val address: String?,
        @SerializedName("birthplace")
        val birthplace: String?,
        @SerializedName("buku_tabungan_image")
        val bukuTabunganImage: String?,
        @SerializedName("city_id")
        val cityId: String?,
        @SerializedName("education")
        val education: Education?,
        @SerializedName("email")
        val email: String?,
        @SerializedName("home_phone_no")
        val homePhoneNo: String?,
        @SerializedName("npwp")
        val npwp: String?,
        @SerializedName("npwp_image")
        val npwpImage: String?,
        @SerializedName("profpic_image")
        val profpicImage: String?,
        @SerializedName("religion_id")
        val religionId: Int?,
        @SerializedName("work")
        val work: Work?
    ) {
        data class Work(
            @SerializedName("company_name")
            val companyName: String?,
            @SerializedName("end_date")
            val endDate: String?,
            @SerializedName("last_title_name")
            val lastTitleName: String?,
            @SerializedName("start_date")
            val startDate: String?
        )

        data class Education(
            @SerializedName("college_name")
            val collegeName: String?
        )
    }

    data class Signature(
        @SerializedName("signature_image")
        val signatureImage: String
    )
}