package co.id.ajsmsig.ismile.model.api


import com.google.gson.annotations.SerializedName

data class AgentLeaderStatusApproveResponse(
    @SerializedName("data")
    val data: List<Data>,
    @SerializedName("error")
    val error: Boolean,
    @SerializedName("message")
    val message: String
) {
    data class Data(
        @SerializedName("agent_candidate_registration_id")
        val registrationId: String,
        @SerializedName("candidate_name")
        val agentCandidateName: String,
        @SerializedName("title_name")
        val titleName: String,
        @SerializedName("submit_date")
        val submitDate: String,
        @SerializedName("profpic_image_path")
        val profpicImagePath: String
    )
}