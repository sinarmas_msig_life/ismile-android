package co.id.ajsmsig.ismile.model

data class InputStatmentModel (val quesioner:String, var selectedAnswer:Answer, val answer:Answer , val devaultValue: Answer = Answer.not_selected)

enum class Answer {
    not_selected,
    no,
    yes
}