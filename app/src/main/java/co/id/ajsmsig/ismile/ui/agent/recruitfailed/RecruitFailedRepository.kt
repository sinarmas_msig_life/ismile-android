package co.id.ajsmsig.ismile.ui.agent.recruitfailed


import android.content.SharedPreferences
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.di.NetManager
import co.id.ajsmsig.ismile.model.api.StatusRecruitRequest
import co.id.ajsmsig.ismile.model.api.StatusRecruitResponse
import co.id.ajsmsig.ismile.util.PREF_KEY_AGENT_CODE
import io.reactivex.Observable
import javax.inject.Inject

class RecruitFailedRepository @Inject constructor(
    private val apiService: ApiService,
    private val netManager: NetManager,
    private val preferences: SharedPreferences
) {

    suspend fun getRecruitFailed(requestCode: Int): StatusRecruitResponse {
        val agentCode = preferences.getString(PREF_KEY_AGENT_CODE, null)
        return apiService.fetchStatusRecruit(StatusRecruitRequest(agentCode!!, requestCode))
    }
}