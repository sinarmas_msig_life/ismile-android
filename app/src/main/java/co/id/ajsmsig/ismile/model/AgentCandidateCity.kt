package co.id.ajsmsig.ismile.model

data class AgentCandidateCity (var citiesId : List<Int>, var citiesName : List<String>)