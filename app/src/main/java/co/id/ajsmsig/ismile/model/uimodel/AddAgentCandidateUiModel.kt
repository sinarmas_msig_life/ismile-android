package co.id.ajsmsig.ismile.model.uimodel

data class AddAgentCandidateUiModel(val error: Boolean, val message : String)