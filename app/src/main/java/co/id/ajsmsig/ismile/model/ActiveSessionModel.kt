package co.id.ajsmsig.ismile.model

data class ActiveSessionModel(val isLoggedIn : Boolean, val loginRoleType : Int)