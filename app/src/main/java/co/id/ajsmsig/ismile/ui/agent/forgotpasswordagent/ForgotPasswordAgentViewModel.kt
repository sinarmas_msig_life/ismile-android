package co.id.ajsmsig.ismile.ui.agent.forgotpasswordagent

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import co.id.ajsmsig.ismile.SingleLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.model.MessageInfo
import co.id.ajsmsig.ismile.model.api.AgentForgotPasswordResponse
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ForgotPasswordAgentViewModel @Inject constructor(private val repository: ForgotPasswordAgentRepository) :
    BaseViewModel() {

    var username = ObservableField<String>()
    var errorUsername = ObservableField<String>()

    var phoneNumber = ObservableField<String>()
    var errorPhoneNumber = ObservableField<String>()

    val isLeader = ObservableBoolean()

    private val _isFindAccountSucces = SingleLiveData<MessageInfo>()
    val isFindAccountSucces: LiveData<MessageInfo>
        get() = _isFindAccountSucces

    private fun findAccountToResetPassword(isLeader: Boolean) {
        val username = username.get().toString().toUpperCase()
        val phoneNumber = phoneNumber.get()
        isLoading.set(true)
        mCompositeDisposable += repository.forgotPasswordAgent(username, phoneNumber!!, isLeader)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { data -> findAccountSucces(data) },
                { error -> findAccountFailed(error) })
    }

    fun validateUserInput(loginType: Boolean) {
        if (!isUsernameValid(loginType)) {
            return
        }
        if (!isPhoneNumberValid()) {
            return
        }

        findAccountToResetPassword(loginType)
    }

    private fun isUsernameValid(loginType: Boolean): Boolean {
        val isValidEmail = isValidEmail(username.get().toString())

        if (username.get().isNullOrEmpty()) {
            setErrorUsername("Field ini tidak boleh kosong")
            return false
        }
        if (!loginType) {
            if (!isValidEmail) {
                setErrorUsername("Format email tidak sesuai  dengan ketentuan")
                return false
            }
        } else {
            setErrorUsername(null)
        }
        return true
    }

    private fun isPhoneNumberValid(): Boolean {
        if (phoneNumber.get().isNullOrEmpty()) {
            setErrorPhoneNumber("Field ini tidak boleh kosong")
            return false
        } else if (!phoneNumber.get()!!.startsWith("08")) {
            setErrorPhoneNumber("Nomor handphone harus diawali dengan nomor '08'")
            return false
        } else if (phoneNumber.get()!!.length < 10) {
            setErrorPhoneNumber("Nomor handphone minimal memiliki 10 karakter")
            return false
        } else {
            setErrorPhoneNumber(null)
        }
        return true
    }

    fun setLoginType(loginType: Boolean) {
        isLeader.set(loginType)
    }

    private fun findAccountSucces(data: AgentForgotPasswordResponse) {
        isLoading.set(false)
        _isFindAccountSucces.sendAction(MessageInfo(data.error, data.message))
    }

    private fun findAccountFailed(error: Throwable) {
        isLoading.set(false)
        _isFindAccountSucces.sendAction(MessageInfo(true, "Terjadi kesalahan. Mohon untuk mencoba kembali"))
    }

    private fun setErrorUsername(message: String?) = errorUsername.set(message)
    private fun setErrorPhoneNumber(message: String?) = errorPhoneNumber.set(message)
    private fun isValidEmail(email: String) = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
}