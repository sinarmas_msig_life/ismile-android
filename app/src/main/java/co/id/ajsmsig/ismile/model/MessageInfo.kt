package co.id.ajsmsig.ismile.model

data class MessageInfo (val error: Boolean, val message: String?)