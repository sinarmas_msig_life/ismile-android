package co.id.ajsmsig.ismile.db.dao

import androidx.room.Dao
import androidx.room.Query
import co.id.ajsmsig.ismile.base.BaseDao
import co.id.ajsmsig.ismile.db.entity.Questionnaire
import co.id.ajsmsig.ismile.db.entity.join.QuestionnaireAndAllQuestionnaireDetail
import io.reactivex.Observable

@Dao
interface QuestionnaireDao : BaseDao<Questionnaire> {

    @Query("UPDATE QUESTIONNAIRES SET ACTUAL_ANSWER = :answer WHERE counter = :id AND REGISTRATION_ID = :registrationId")
    fun updateAnswer(id: Long, registrationId: String, answer: Int)

    @Query("SELECT * FROM QUESTIONNAIRES WHERE REGISTRATION_ID = :registrationId")
    fun findAllSavedAnswer(registrationId: String): Observable<List<QuestionnaireAndAllQuestionnaireDetail>>

    @Query("SELECT id FROM QUESTIONNAIRES WHERE REGISTRATION_ID = :registrationId and COUNTER =:counterId ")
    suspend fun getQuestionereId(registrationId: String, counterId: Long) : Long
}