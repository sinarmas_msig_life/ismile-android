package co.id.ajsmsig.ismile.util

import android.graphics.BitmapFactory
import android.net.Uri
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.view.View
import android.widget.ImageView
import android.widget.Switch
import androidx.databinding.BindingAdapter
import co.id.ajsmsig.ismile.R
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import timber.log.Timber
import java.io.File

@BindingAdapter("loadImage")
fun loadImage(view: ImageView, imagePath: String) {
    if (!imagePath.isEmpty()) {
        val file = File(imagePath)
        val uri = Uri.fromFile(file)
        Glide.with(view.context).load(uri).into(view)
    }
}

@BindingAdapter("bind:imageUrl")
fun setImageUrl(view: ImageView, url: String) {
    Glide.with(view.context).load(url).into(view)
}

@BindingAdapter("loadImageBitmap")
fun loadImageBitmapCheck(view: ImageView, imagePath: String) {
    val context = view.getContext()
    Timber.e("CROT")
    if (!imagePath.isEmpty()) {
        val decodedString = Base64.decode(imagePath, Base64.DEFAULT)
        val decodedImage = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        Glide.with(context).load(decodedImage).into(view)
    }
}

@BindingAdapter("imageSrc")
fun loadImageFromDrawable(view: ImageView, imageId: Int) {
    view.setImageResource(imageId)
}

@BindingAdapter("minLength")
fun minLength(view: TextInputEditText, minLength: Int) {
    if (minLength > 0) {
        view.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(sequence: CharSequence, start: Int, before: Int, count: Int) {
                if (sequence.length < minLength) {
                    view.error = "${view.hint} tidak boleh kurang dari $minLength karakter"
                } else {
                    view.error = null
                }
            }
        })
    }
}

@BindingAdapter("required")
fun shouldShowErrorMessages(view: TextInputLayout, required: Boolean) {
    if (required) {
        view.isErrorEnabled = true
        view.error = view.context.getString(R.string.err_msg_field_empty_)
    } else {
        view.isErrorEnabled = false
        view.error = null
    }
}

@BindingAdapter("errorMessage")
fun shouldShowError(view: TextInputLayout, errorMessage: Int?) {
    if (errorMessage != null && errorMessage != 0) {
        view.error = view.context.getString(errorMessage)
    } else {
        view.error = null
    }
}


@BindingAdapter("errorMessageIfNotAgreed")
fun shouldShowErrorSwitch(view: Switch, errorMessage: Int?) {
    if (errorMessage != null && errorMessage !=0){
        view.error = view.context.getString(errorMessage)
    }else {
        view.error = null
    }
}

/**
 * When error message is exist, it means error is occurred.
 */
@BindingAdapter("requestFocusWhenEmpty")
fun requestFocusWhenEmpty(view: View, errorMessage: Int?) {
    if (errorMessage != 0) {
        view.requestFocus()
    }
}

@BindingAdapter("requestFocus")
fun requestFocus(view: TextInputLayout, requestFocus: Boolean) {
    if (requestFocus) view.requestFocus()
}
