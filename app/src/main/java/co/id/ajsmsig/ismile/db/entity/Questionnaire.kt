package co.id.ajsmsig.ismile.db.entity

import androidx.room.*

@Entity(
        tableName = "questionnaires",
        foreignKeys = [
            ForeignKey(
                    entity = AgentCandidate::class,
                    parentColumns = arrayOf("id"), //column name in parent table
                    childColumns = arrayOf("registration_id"), //column name in child table
                    onDelete = ForeignKey.CASCADE, //when agent candidate is removed, this questionnare also removed
                    onUpdate = ForeignKey.CASCADE
            )
        ],
        indices = arrayOf(Index(value = ["counter"], unique = false))
)

data class Questionnaire(

        @PrimaryKey (autoGenerate = true)
        @ColumnInfo(name = "id")
        var id: Long? = 0,

        @ColumnInfo(name = "registration_id")
        var registrationId: String?,

        @ColumnInfo(name = "question")
        var question: String?,

        // -1 = NOT SELECTED, 0 = NO, 1 = YES
        @ColumnInfo(name = "actual_answer")
        var answer: Int?,

        // -1 = NOT SELECTED, 0 = NO, 1 = YES
        @ColumnInfo(name = "default_answer")
        var defaultAnswer: Int?,

        //0 = NO, 1 = YES
        @ColumnInfo(name = "correct_answer")
        var correctAnswer: Int?,

        @ColumnInfo(name = "question_type")
        var questionType: Int?,

        @ColumnInfo(name = "counter")
        var counter: Long?
)