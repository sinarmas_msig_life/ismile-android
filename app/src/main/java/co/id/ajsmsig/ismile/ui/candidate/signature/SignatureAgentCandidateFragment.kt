package co.id.ajsmsig.ismile.ui.candidate.signature

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentSignatureAgentCandidateBinding
import co.id.ajsmsig.ismile.util.PREFF_KEY_AGENT_CANDIDATE_CODE
import co.id.ajsmsig.ismile.util.REQUEST_CODE_DRAW
import com.bumptech.glide.Glide
import com.divyanshu.draw.activity.DrawingActivity
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.fragment_signature_agent_candidate.*
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class SignatureAgentCandidateFragment : BaseFragment<FragmentSignatureAgentCandidateBinding, SignatureAgentCandidateViewModel>() {

    private var mCurrentPhotoPath = ""

    override fun getLayoutResourceId() = R.layout.fragment_signature_agent_candidate

    override fun getViewModelClass() = SignatureAgentCandidateViewModel::class.java
    var isSubmitted : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isSubmitted = SignatureAgentCandidateFragmentArgs.fromBundle(arguments!!).isSubmitted

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val vm = getViewModel()
        getDataBinding().vm = vm
        getDataBinding().fragment = this

        getViewModel().getInfoSignatureAgentCandidate ()

        getViewModel().isUserInputValid.observe(this, androidx.lifecycle.Observer {
            it?.let {
                if (it.error){
                    Snackbar.make(getDataBinding().root, "${it.message}", Snackbar.LENGTH_SHORT).show()
                }else{
                    submit()
                }
            }
        })

        getViewModel().isSignaturePath.observe(this, androidx.lifecycle.Observer {
            it?.let {
                setSignature(it)
            }
        })

        getDataBinding().cbAgrrement.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                getViewModel().isSignatureChecked.set(true)
            } else {
                getViewModel().isSignatureChecked.set(false)
            }
        }

        getViewModel().isSuccesSubmit.observe(this, androidx.lifecycle.Observer {
            it?.let {
                if(it){
                    Snackbar.make(getDataBinding().root, getString(R.string.save_success), Snackbar.LENGTH_SHORT).show()
                    navigateToMenuHome()
                }
            }
        })
    }

    fun onSubmitClicked() {
        getViewModel().validateUserInput()
    }

    fun submit(){
        val timeStamp = SimpleDateFormat("yyyyMMddHHmmss").format(Date())
        getViewModel().submitSignature(timeStamp)
    }
    fun onImageSignatureClicked() {
        val permissionAllowed = checkPermission()
        if (permissionAllowed) takeSignature() else requestPermission()
    }

    private fun checkPermission(): Boolean {
        return (ContextCompat.checkSelfPermission(
            activity!!,
            android.Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
                &&
                ContextCompat.checkSelfPermission(
                    activity!!,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermission() {
        Dexter.withActivity(activity)
            .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // Check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        takeSignature()
                    }

                    // Check for permanent denial of any permission
                    if (report.isAnyPermissionPermanentlyDenied) {
                        // permission is denied permenantly, navigate user to app settings
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }

            })
            .withErrorListener {
                Toast.makeText(activity, "Error occurred! $it", Toast.LENGTH_SHORT).show()
            }
            .onSameThread()
            .check()
    }

    fun takeSignature() {
        val intent = Intent(activity, DrawingActivity::class.java)
        startActivityForResult(intent, REQUEST_CODE_DRAW)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data != null && resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_CODE_DRAW -> {
                    val result = data.getByteArrayExtra("bitmap")
                    val bitmap = BitmapFactory.decodeByteArray(result, 0, result.size)

                    saveImage(bitmap)

                    //Save In View Model
                    getViewModel().saveSigantureImagePath(mCurrentPhotoPath)

                }
            }
        }
    }

    private fun saveImage(bitmap: Bitmap) {
        val timeStamp = SimpleDateFormat("yyyyMMddHHmmss").format(Date())
        val imageFile = "JPEG_" + timeStamp + "_"
        val storageDir = activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(imageFile, ".jpg", storageDir)
        mCurrentPhotoPath = image.absolutePath

        val outputStream = FileOutputStream(mCurrentPhotoPath)
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
        outputStream.flush()
        outputStream.close()
    }

    fun setSignature(imagePath: String) {
        val bitmap = BitmapFactory.decodeFile(imagePath)
        Glide.with(activity!!).load(bitmap).into(ivSignature)
        getDataBinding().cbAgrrement.isChecked=true
    }

    private fun navigateToMenuHome(){
        val registrtationId=activity?.getSharedPreferences(PREFF_KEY_AGENT_CANDIDATE_CODE, Context.MODE_PRIVATE)
        val action = SignatureAgentCandidateFragmentDirections.actionLaunchAgentCandidateFragment(registrtationId.toString())
        findNavController().navigate(action)
    }
}
