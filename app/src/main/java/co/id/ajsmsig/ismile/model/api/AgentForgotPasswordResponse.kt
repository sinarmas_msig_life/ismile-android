package co.id.ajsmsig.ismile.model.api

import com.google.gson.annotations.SerializedName

data class AgentForgotPasswordResponse(
    @SerializedName("message")
    val message: String,
    @SerializedName("error")
    val error: Boolean,
    @SerializedName("data")
    val data: Any
)