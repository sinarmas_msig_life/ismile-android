package co.id.ajsmsig.ismile.ui.agent.logintype


import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentChooseLoginTypeBinding


class ChooseLoginTypeFragment : BaseFragment<FragmentChooseLoginTypeBinding,ChooseLoginTypeViewModel>() {

    override fun getLayoutResourceId() = R.layout.fragment_choose_login_type
    override fun getViewModelClass(): Class<ChooseLoginTypeViewModel> = ChooseLoginTypeViewModel::class.java


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val vm = getViewModel()
        getDataBinding().vm = vm
        getDataBinding().fragment = this
    }

    fun onAgentCandidateLoginPressed() {
        val action=ChooseLoginTypeFragmentDirections.actionLaunchAgentCandidateLoginFragment()
        findNavController().navigate(action)

    }

    fun onAgentLoginTypePressed() {
        val action = ChooseLoginTypeFragmentDirections.actionLaunchAgentLoginFragment(false)
        findNavController().navigate(action)
    }

    fun onAgentLeaderLoginTypePressed() {
        val action=ChooseLoginTypeFragmentDirections.actionLaunchAgentLoginFragment(true)
        findNavController().navigate(action)
    }


}
