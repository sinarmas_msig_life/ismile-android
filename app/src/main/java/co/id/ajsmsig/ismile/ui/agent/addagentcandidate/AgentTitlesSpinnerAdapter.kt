package co.id.ajsmsig.ismile.ui.agent.addagentcandidate

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.fragment.app.FragmentActivity
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.databinding.CustomSpinnerAgentPositionBinding
import co.id.ajsmsig.ismile.model.AgentTitle

class AgentTitlesSpinnerAdapter(val context: FragmentActivity, private var titles: List<AgentTitle>) : BaseAdapter() {

    val inflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemRowHolder

        if (convertView == null) {
            val binding = CustomSpinnerAgentPositionBinding.inflate(inflater, parent, false)
            vh = ItemRowHolder(binding)
            view = binding.root
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemRowHolder
        }


        vh.bind(R.drawable.ic_action_employee, titles[position].titleName)

        return view

    }

    private class ItemRowHolder(private val binding : CustomSpinnerAgentPositionBinding) {

        fun bind(imageResourceId : Int, label : String) {
            binding.ivAgentPosition.setImageResource(imageResourceId)
            binding.tvAgentPosition.text = label
            binding.executePendingBindings()
        }
    }


    override fun getItem(p0: Int): Any? = null
    override fun getItemId(p0: Int) : Long = 0
    override fun getCount(): Int  = titles.size

    fun refreshData(titles : List<AgentTitle>) {
        this.titles = titles
        notifyDataSetChanged()
    }

}