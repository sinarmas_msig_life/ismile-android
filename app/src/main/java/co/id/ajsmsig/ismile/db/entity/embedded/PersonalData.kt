package co.id.ajsmsig.ismile.db.entity.embedded

import androidx.room.ColumnInfo

data class PersonalData(
    //halaman 1 dari 3 @ColumnInfo(name = "name")
    var name: String?,

    @ColumnInfo (name = "identity_number")
    var identityNumber:String?,

    @ColumnInfo (name = "birth_of_date")
    var birthOfDate : String?,

    @ColumnInfo (name = "marital_id")
    var maritalId : Int?,

    @ColumnInfo (name = "marital_status")
    var maritalStatus : String?,

    @ColumnInfo ( name = "phone_number")
    var phoneNumber : String?,

    @ColumnInfo(name = "title_id")
    var titleId: Int?,

    @ColumnInfo(name = "title_name")
    var titleName: String?,

    @ColumnInfo (name = "bop_date")
    var bopDate : String?,

    @ColumnInfo(name = "email")
    var email: String?,

    @ColumnInfo(name = "place_of_birth")
    var placeOfBirth: String?,

    @ColumnInfo(name = "gender_id")
    var genderId: Int,

    @ColumnInfo(name = "home_address")
    var homeAddress: String?,

    @ColumnInfo(name = "city_id")
    var cityId: Int?,

    @ColumnInfo(name = "city_name")
    var cityName: String?,

    @ColumnInfo(name = "npwp_number")
    var npwpNumber: String?,

    @ColumnInfo(name = "religion_id")
    var religionId: Int,

    @ColumnInfo(name = "house_phone")
    var housePhone: String?,

    @ColumnInfo (name ="profile_photo_img_path")
    var profilePhotoImgPath : String?,

    @ColumnInfo (name ="identity_photo_img_path")
    var identityPhotoImgPath : String?,

    @ColumnInfo(name = "saving_account_img_path")
    var savingAccountImgPath: String?,

    @ColumnInfo(name = "npwp_photo_img_path")
    var npwpPhotoImgPath: String?,

    //halaman 2 dari 3
    @ColumnInfo(name = "company_name")
    var companyName: String?,

    @ColumnInfo(name = "last_position")
    var lastPosition: String?,

    @ColumnInfo(name = "start_date_work")
    var startDateWork: String?,

    @ColumnInfo(name = "end_date_work")
    var endDateWork: String?,

    //halaman 3 dari 3
    @ColumnInfo (name = "school_id")
    var schoolId: Int?,

    @ColumnInfo(name = "school_name")
    var schoolName: String?
)