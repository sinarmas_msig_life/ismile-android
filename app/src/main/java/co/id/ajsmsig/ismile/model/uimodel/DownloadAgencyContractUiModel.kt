package co.id.ajsmsig.ismile.model.uimodel

import okhttp3.ResponseBody

data class DownloadAgencyContractUiModel (
    val error : Boolean,
    val message : String?,
    val pdfFilePath : String?
)