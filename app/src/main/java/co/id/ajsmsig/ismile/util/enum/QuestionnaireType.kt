package co.id.ajsmsig.ismile.util.enum

enum class QuestionnaireType(val value : Int) {
    SINGLE_CHOICE(0),
    SINGLE_CHOICE_WITH_SIMPLE_ESSAY(1)
}