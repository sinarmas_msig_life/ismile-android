package co.id.ajsmsig.ismile.di.module

import android.app.Application
import android.text.TextUtils
import android.util.Log
import co.id.ajsmsig.ismile.BuildConfig
import co.id.ajsmsig.ismile.util.*
import dagger.Module
import dagger.Provides
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class                                                                                                                                                                                                                   NetworkModule {
    @Provides
    @Singleton
    fun provideHttpCache(application: Application) : Cache {
        val cacheSize : Long = 10 * 10 * 1024
        return Cache(application.cacheDir,cacheSize)
    }

    @Provides
    @Singleton
    fun provideLoggingInterceptor() : HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return loggingInterceptor
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor,cache: Cache, timeoutInterceptor : Interceptor) : OkHttpClient {
        val client = OkHttpClient.Builder()

        client.connectTimeout(DEFAULT_CONNECT_TIMEOUT , TimeUnit.SECONDS)
        client.writeTimeout(DEFAULT_WRITE_TIMEOUT , TimeUnit.SECONDS)
        client.readTimeout(DEFAULT_READ_TIMEOUT , TimeUnit.SECONDS)
        client.cache(cache)
        if (BuildConfig.DEBUG) {
            client.addInterceptor(loggingInterceptor)
        }
        client.addInterceptor(timeoutInterceptor)
        return client.build()
    }

    @Provides
    @Singleton
    fun provideCustomTimeoutInterceptor(gson : Gson) : Interceptor {

        return Interceptor { chain ->
            val request = chain.request()

            var connectTimeout = chain.connectTimeoutMillis()
            var readTimeout = chain.readTimeoutMillis()
            var writeTimeout = chain.writeTimeoutMillis()

            val connectNew = request.header(CONNECT_TIMEOUT)
            val readNew = request.header(READ_TIMEOUT)
            val writeNew = request.header(WRITE_TIMEOUT)

            if (! TextUtils.isEmpty(connectNew)) {
                connectNew?.let {
                    connectTimeout = Integer.valueOf(it)
                }
            }
            if (! TextUtils.isEmpty(readNew)) {
                readNew?.let {
                    readTimeout = Integer.valueOf(it)
                }
            }
            if (! TextUtils.isEmpty(writeNew)) {
                writeNew?.let {
                    writeTimeout = Integer.valueOf(it)
                }
            }

            val response = chain
                .withConnectTimeout(connectTimeout , TimeUnit.MILLISECONDS)
                .withReadTimeout(readTimeout , TimeUnit.MILLISECONDS)
                .withWriteTimeout(writeTimeout , TimeUnit.MILLISECONDS)
                .proceed(request)

            if (response.code() != 200) {
                sendErrorLogs(request, response, gson, chain)
            }

            response
        }
    }

    @Provides
    @Singleton
    fun provideBaseUrl() = BASE_URL

    @Provides
    @Singleton
    fun provideGsonConverterFactory() : Gson {
        return GsonBuilder()
            .serializeNulls()
            .setPrettyPrinting()
            .create()
    }

    @Provides
    @Singleton
    fun provideRetrofit(baseUrl : String, okHttpClient: OkHttpClient, gson: Gson) : Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
    }

    private fun sendErrorLogs(request: Request, response: Response, gson: Gson, chain: Interceptor.Chain) {
        val url = request.url().url().toString()
        val method = request.method()
        val oldRequest = Utils.bodyToString(request)
        val responseBody = response.peekBody(1024*1024)
        val oldResponse = responseBody.string() ?: "Response is null"

        val newRequestBuilder = request.newBuilder()

        val text = """
            *Oops. An issue detected*
            %0A
            %0A
            Application Id : ${BuildConfig.APPLICATION_ID}
            %0A
            Version name : ${BuildConfig.VERSION_NAME}
            %0A
            %0A
            
            URL : `$url`%0A
            
            HTTP code : ${response.code()}
            %0A
            HTTP method : $method
            %0A
            Response time (millis) : ${response.receivedResponseAtMillis() - response.sentRequestAtMillis()}
            %0A
            %0A
            
            Request :
            ```
            $oldRequest
            ```
             Response :
            ```
            $oldResponse
            ```

        """.trimIndent()

        val message = "https://api.telegram.org/bot$TELEGRAM_CHANNEL_ID/sendMessage?chat_id=${TELEGRAM_CHATT_ID}_ID&text=$text&parse_mode=Markdown&disable_notification=true"

        val newRequest = newRequestBuilder
            .url(message)
            .get()
            .build()

        chain.proceed(newRequest)

        Timber.e("Sending error log data : $text")
    }
}