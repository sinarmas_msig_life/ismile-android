package co.id.ajsmsig.ismile.model

data class GetSpinnerCities (val error : Boolean, val citiesId: Int)