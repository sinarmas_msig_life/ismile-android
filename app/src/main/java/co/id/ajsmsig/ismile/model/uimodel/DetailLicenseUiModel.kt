package co.id.ajsmsig.ismile.model.uimodel

import co.id.ajsmsig.ismile.model.DetailLicense

data class DetailLicenseUiModel (val error: Boolean, val detail: List<DetailLicense>? )