package co.id.ajsmsig.ismile.model

data class SchoolLevel (val id: Int, val level: String)