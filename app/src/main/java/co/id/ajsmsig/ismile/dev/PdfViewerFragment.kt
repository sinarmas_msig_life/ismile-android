package co.id.ajsmsig.ismile.dev


import android.content.ContentValues.TAG
import android.content.Context
import android.graphics.pdf.PdfRenderer
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.databinding.FragmentPdfViewerBinding
import co.id.ajsmsig.ismile.util.INITIAL_PAGE_INDEX
import java.io.File
import java.io.IOException

class PdfViewerFragment : Fragment() {

    private var fileName = ""
    private lateinit var pdfRenderer: PdfRenderer
    private lateinit var currentPage: PdfRenderer.Page
    private lateinit var pdfPageView: ImageView
    private var currentPageNumber: Int = INITIAL_PAGE_INDEX

    private lateinit var binding : FragmentPdfViewerBinding
    private lateinit var adapter: PdvViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        fileName = PdfViewerFragmentArgs.fromBundle(arguments!!).fileName

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pdf_viewer, container, false)
        binding.apply {
            executePendingBindings()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        val pdfFile = Uri.fromFile(File("${Environment.getExternalStorageDirectory().absolutePath}/SMiLeGo/PDF/$fileName"))

        try {
            openRenderer(activity, pdfFile)
            showPage(currentPageNumber)
        } catch (ioException: IOException) {
            Log.d(TAG, "Exception opening document", ioException)
        }
    }

    /**
     * Sets up a [PdfRenderer] and related resources.
     */
    @Throws(IOException::class)
    private fun openRenderer(context: Context?, documentUri: Uri) {
        if (context == null) return

        val fileDescriptor = context.contentResolver.openFileDescriptor(documentUri, "r") ?: return

        // This is the PdfRenderer we use to render the PDF.
        pdfRenderer = PdfRenderer(fileDescriptor)
        currentPage = pdfRenderer.openPage(currentPageNumber)

    }

    override fun onStop() {
        super.onStop()
        try {
            closeRenderer()
        } catch (ioException: IOException) {
            Log.d(TAG, "Exception closing document", ioException)
        }
    }

    /**
     * Closes the [PdfRenderer] and related resources.
     *
     * @throws IOException When the PDF file cannot be closed.
     */
    @Throws(IOException::class)
    private fun closeRenderer() {
        pdfRenderer.close()
    }

    private fun showPage(index: Int) {
        if (index < 0 || index >= pdfRenderer.pageCount) return

        currentPage.close()

        adapter = PdvViewAdapter(pdfRenderer, currentPage.width)
        val layoutManager = LinearLayoutManager(activity)
        binding.rvPdfViewer.layoutManager = layoutManager
        binding.rvPdfViewer.adapter = adapter

        adapter.refreshAdapter(pdfRenderer)
        // Important: the destination bitmap must be ARGB (not RGB).
    }
}