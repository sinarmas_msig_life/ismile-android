package co.id.ajsmsig.ismile.db.entity.join

import androidx.room.Embedded
import androidx.room.Relation
import co.id.ajsmsig.ismile.db.entity.AgentCandidate
import co.id.ajsmsig.ismile.db.entity.Questionnaire

data class AgentCandidateAndAllQuestionnaire (
    @Embedded
    var agentCandidate: AgentCandidate? = null,

    @Relation(
        parentColumn = "id",
        entityColumn = "registration_id",
        entity = Questionnaire::class
    )
    var questionnaire: List<QuestionnaireAndAllQuestionnaireDetail> = emptyList()

)