package co.id.ajsmsig.ismile.model.uimodel

import co.id.ajsmsig.ismile.model.AgentTitle

data class AgentTitleUiModel(val error: Boolean, val message : String, val titles : List<AgentTitle>)