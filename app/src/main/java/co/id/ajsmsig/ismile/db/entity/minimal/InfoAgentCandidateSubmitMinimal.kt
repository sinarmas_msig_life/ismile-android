package co.id.ajsmsig.ismile.db.entity.minimal

import androidx.room.ColumnInfo

data class InfoAgentCandidateSubmitMinimal (

    @ColumnInfo(name = "id")
    var registrationId: String,

    @ColumnInfo(name = "personal_data_date")
    var personalDataDate: String?,

    @ColumnInfo(name = "work_experience_date")
    var workExperienceDate: String?,

    @ColumnInfo(name = "education_date")
    var educationDate: String?,

    //Surat pernyataan
    @ColumnInfo(name = "questionnaire_date")
    var questionnaireDate: String?,

    @ColumnInfo(name = "ethic_code_date")
    var ethicCodeDate: String?,

    @ColumnInfo(name = "employment_contract")
    var employmentContractDate: String?,

    @ColumnInfo(name = "agency_letter_date")
    var agencyLetterDate: String?,

    //Tanda tangan
    @ColumnInfo(name = "signature_date")
    var signatureDate: String?,

    //Lisensi
    @ColumnInfo(name = "exam_input_date")
    var examInputDate: String?,

    @ColumnInfo(name = "bsb_submit_date")
    var bsbSubmitDate: String?,

    @ColumnInfo(name = "exam_date")
    var examDate: String?
)