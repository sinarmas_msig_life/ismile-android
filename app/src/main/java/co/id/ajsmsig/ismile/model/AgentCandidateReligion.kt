package co.id.ajsmsig.ismile.model

data class AgentCandidateReligion (val id :Int, val religion:String)