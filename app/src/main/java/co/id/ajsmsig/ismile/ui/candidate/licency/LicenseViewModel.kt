package co.id.ajsmsig.ismile.ui.candidate.licency

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.id.ajsmsig.ismile.SingleLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.db.entity.minimal.ExamInformationMinimal
import co.id.ajsmsig.ismile.model.DetailLicense
import co.id.ajsmsig.ismile.model.GetSpinnerCities
import co.id.ajsmsig.ismile.model.LicenseCities
import co.id.ajsmsig.ismile.model.MessageInfo
import co.id.ajsmsig.ismile.model.api.LicenseCitiesResponse
import co.id.ajsmsig.ismile.model.uimodel.DetailLicenseUiModel
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class LicenseViewModel @Inject constructor(private val repository: LicenseRepository) : BaseViewModel() {

    val isConnected = ObservableBoolean ()

    val inProgress = ObservableBoolean ()

    val isEmpetyCities = ObservableBoolean (false)

    private val cityId = ObservableField (0)

    private val _isSubmitSucces= SingleLiveData <MessageInfo>()
    val isSubmitSucces : LiveData <MessageInfo>
        get() = _isSubmitSucces

    private val _isExam = MutableLiveData <DetailLicenseUiModel>()
    val isExam: LiveData<DetailLicenseUiModel>
        get() = _isExam

    private val _isCities = MutableLiveData<LicenseCities>()
    val isCities: LiveData<LicenseCities>
        get() = _isCities

    private val _isExamSelected = MutableLiveData<GetSpinnerCities>()
    val isExamSelected: LiveData<GetSpinnerCities>
        get() = _isExamSelected

    fun getALlCities() {
        inProgress.set(true)
        mCompositeDisposable += repository.getAllCities()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { data -> getAllCitiesSuccess(data) },
                { getAllCitiesFailed() }
            )
    }

    fun retrieveInfoLicenseExam (cities: String) {
        inProgress.set(true)
        mCompositeDisposable += repository.retrieveLicenseInfoSubmit(cities)
            .map {
                val exam = ArrayList<DetailLicense>()
                for (i in it){ exam.add(DetailLicense(i.examPlace, i.examDate, i.examMethod, i.examMethodName, i.examProductType, i.examProductName, i.examType, i.examTypeName, i.noRek, i.banking, i.price)) }
                DetailLicenseUiModel (false, exam)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { data -> retriveAllInfoSubmitSuccess(data) },
                { retriveAllInfoSubmitFailed() })

    }

    fun saveDataSubmitted(data : DetailLicense, citiesId: Int, citiesName: String) {
        mCompositeDisposable += Single.fromCallable {repository.saveUserSubmitted(data, citiesId, citiesName)}
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {saveDataLicenseSucces()},
                {error-> saveDataFailed (error)}
            )
    }

    fun checkedDataDatabase () {
        mCompositeDisposable += repository.retieveExamInformation()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {data-> getInfoCitiesSuccess (data)},
                {error-> getInfoCitiesFailed(error)})
    }

    private fun getInfoCitiesSuccess(data: ExamInformationMinimal) {
        if (!data.examCityName.isNullOrEmpty()){
            DetailLicense(data.examPlaceName!!, data.examSelectedDate!!, data.examMethodId!!, data.examMethodName!!, data.examProductTypeId!!, data.examProductTypeName!!, data.examTypeId!!, data.examTypeName!!, data.numberRek!!, data.banking!!, data.nominal!!)
            _isExamSelected.value = GetSpinnerCities(true, data.examCityId!!)
        }else{
            _isExamSelected.value = GetSpinnerCities(false, cityId.get()!!) // error
        }
    }

    private fun getInfoCitiesFailed(error: Throwable) {
        Timber.e ("$error")
    }

    private fun saveDataLicenseSucces() {
        _isSubmitSucces.sendAction(MessageInfo(false, "Data berhasil di simpan"))
    }

    private fun saveDataFailed(error: Throwable?) {
        _isSubmitSucces.sendAction(MessageInfo(true, error.toString()))
    }

    private fun retriveAllInfoSubmitSuccess(data: DetailLicenseUiModel) {
        _isExam.value=data
        if (!data.detail.isNullOrEmpty()){
            isEmpetyCities.set(false)
        }else{
            isEmpetyCities.set(true)
        }
        inProgress.set(false)
        isConnected.set(true)
    }

    private fun retriveAllInfoSubmitFailed() {
        inProgress.set(false)
        isConnected.set(false)
    }

    private fun getAllCitiesSuccess(data: List<LicenseCitiesResponse>) {
        val cities = mutableListOf<String>()
        for (i in data) {
            cities.add(i.CITY)
        }
        _isCities.value = LicenseCities(cities)
        inProgress.set(false)
        isConnected.set(true)
    }

    private fun getAllCitiesFailed() {
        inProgress.set(false)
        isConnected.set(false)
    }

    fun saveCityId (cityId: Int){
        this.cityId.set(cityId)
    }

}