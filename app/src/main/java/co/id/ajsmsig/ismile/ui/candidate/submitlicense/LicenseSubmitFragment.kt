package co.id.ajsmsig.ismile.ui.candidate.submitlicense

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.BuildConfig
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentLicenseSubmitBinding
import co.id.ajsmsig.ismile.util.IMAGE_COMPRESSION_PERCENTAGE
import co.id.ajsmsig.ismile.util.REQUEST_IMAGE_CAPTURE
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class LicenseSubmitFragment : BaseFragment<FragmentLicenseSubmitBinding, LicenseSubmitViewModel>() {

    override fun getLayoutResourceId() = R.layout.fragment_license_submit

    override fun getViewModelClass() = LicenseSubmitViewModel::class.java

    private var mCurrentPhotoPath = ""

    private var registrationId = 0L

    private var progressDialog: ProgressDialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getActivity()!!.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)

        val vm = getViewModel()
        getDataBinding().vm = vm
        getDataBinding().fragment = this

        registrationId = getViewModel().getAgentCandidateCode()

        getViewModel().retrieveInfoExamSubmitted()

        getViewModel().imagePath.observe(this, androidx.lifecycle.Observer {
            it?.let {
                setImagePathBitmap(it)
            }
        })

        getViewModel().mCurrentPath.observe(this, androidx.lifecycle.Observer {
            it?.let {
                mCurrentPhotoPath = it
            }
        })

        getViewModel().isMessage.observe(this, androidx.lifecycle.Observer {
            it?.let {
                if (!it.error) {
                    launchAgentCandidate()
                } else {
                    Snackbar.make(getDataBinding().root, it.message!!, Snackbar.LENGTH_SHORT).show()
                }
            }
        })

        getViewModel().isUserInputValid.observe(this, androidx.lifecycle.Observer {
            it?.let {
                submit()
            }
        })

        getViewModel().displaySubmitDataOnProgress.observe(this, androidx.lifecycle.Observer {
            it?.let {
                if (it.isFinished) {
                    hideProgressDialog()
                } else {
                    displayProgressDialog(it.message!!)
                }
            }
        })

    }

    fun launchAgentCandidate() {
        val action = LicenseSubmitFragmentDirections.actionLaunchCandateAgent(registrationId.toString())
        findNavController().navigate(action)
    }

    fun onBsbImageClicked() {
        val permissionAllowed = checkPermission()
        if (permissionAllowed) takePicture() else requestPermission()
    }

    fun onSubmitLicenseClicked() {
        getViewModel().validateUserInput()
    }

    fun submit() {
        var convertImage = convertToBase64(mCurrentPhotoPath)
        getViewModel().submitLicense(convertImage)
    }

    private fun checkPermission(): Boolean {
        return (ContextCompat.checkSelfPermission(
            activity!!,
            android.Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            activity!!,
            android.Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED)
    }

    private fun takePicture() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file = createFile()
        val uri = FileProvider.getUriForFile(activity!!, BuildConfig.APPLICATION_ID, file)

        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
    }

    private fun requestPermission() {
        Dexter.withActivity(activity)
            .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // Check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        takePicture()
                    }

                    // Check for permanent denial of any permission
                    if (report.isAnyPermissionPermanentlyDenied) {
                        // permission is denied permenantly, navigate user to app settings
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }

            })
            .withErrorListener {
                Toast.makeText(activity, "Error occurred! $it", Toast.LENGTH_SHORT).show()
            }
            .onSameThread()
            .check()
    }

    @Throws(IOException::class)
    private fun createFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMddHHmmss").format(Date())
        val imageFile = "JPEG_" + timeStamp + "_"
        val storageDir = activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(imageFile, ".jpg", storageDir)

        getViewModel().saveMcurrentPath(image.absolutePath) //contains image uri

        return image
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {

            compressImage(mCurrentPhotoPath)

            //Save the bitmap to viewmodel, so it will be restored when orientation changed.
            getViewModel().saveImagePath(mCurrentPhotoPath)

            setImagePathBitmap(mCurrentPhotoPath)

        }
    }

    private fun setImagePathBitmap(imagePath: String) {
        Glide.with(activity!!)
            .load(imagePath)
            .thumbnail(0.1f)
            .into(getDataBinding().ivDocumentImage)
    }

    private fun compressImage(imagePath: String) {
        val bitmap = BitmapFactory.decodeFile(imagePath)
        val file = File(imagePath)
        val out = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.JPEG, IMAGE_COMPRESSION_PERCENTAGE, out)
        out.flush()
        out.close()
    }

    private fun convertToBase64(imagePath: String): String {
        val bitmap = BitmapFactory.decodeFile(imagePath)
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, IMAGE_COMPRESSION_PERCENTAGE, outputStream)
        val byteArray = outputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }

    private fun displayProgressDialog(title: String?) {
        progressDialog = ProgressDialog(activity)
        progressDialog?.isIndeterminate = true
        progressDialog?.setTitle(title)
        progressDialog?.setMessage("Mohon tunggu")
        progressDialog?.setCancelable(false)
        progressDialog?.show()
    }

    private fun hideProgressDialog() {
        progressDialog?.dismiss()
    }
}
