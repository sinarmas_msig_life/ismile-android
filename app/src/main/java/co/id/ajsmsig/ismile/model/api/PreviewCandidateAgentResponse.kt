package co.id.ajsmsig.ismile.model.api

import com.google.gson.annotations.SerializedName

data class PreviewCandidateAgentResponse(
    @SerializedName("data")
    val data: Data,
    @SerializedName("error")
    val error: Boolean,
    @SerializedName("message")
    val message: String
) {
    data class Data(
        @SerializedName("bod")
        val bod: String,
        @SerializedName("bop_date")
        val bopDate: String,
        @SerializedName("email")
        val email: String,
        @SerializedName("full_name")
        val fullName: String,
        @SerializedName("id_card_no")
        val idCardNo: String,
        @SerializedName("marital_status")
        val maritalStatus: String,
        @SerializedName("gender_name")
        val gender: String,
        @SerializedName("phone_no")
        val phoneNo: String,
        @SerializedName("profpic_image_path")
        val profpicImagePath: String?,
        @SerializedName("title_name")
        val titleName: String,
        @SerializedName("recruiter_name")
        val recruiterName: String,
        @SerializedName("time_work")
        val timeWork: String,
        @SerializedName("company_name")
        val companyName: String,
        @SerializedName("last_position")
        val lastPosition: String
    )
}