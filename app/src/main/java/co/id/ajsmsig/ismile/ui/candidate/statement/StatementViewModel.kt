package co.id.ajsmsig.ismile.ui.candidate.statement

import android.os.Environment
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.SingleLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.db.entity.embedded.Statement
import co.id.ajsmsig.ismile.db.entity.join.AgentCandidateAndAllQuestionnaire
import co.id.ajsmsig.ismile.db.entity.join.QuestionnaireAndAllQuestionnaireDetail
import co.id.ajsmsig.ismile.db.entity.minimal.AgencyLetterData
import co.id.ajsmsig.ismile.model.DownloadPdfModel
import co.id.ajsmsig.ismile.model.uimodel.DownloadAgencyContractUiModel
import co.id.ajsmsig.ismile.model.uimodel.StatementUiModel
import co.id.ajsmsig.ismile.util.DateUtils
import co.id.ajsmsig.ismile.util.enum.QuestionnaireAnswer
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import timber.log.Timber
import java.io.*
import javax.inject.Inject

class StatementViewModel @Inject constructor(private val repository: StatementRepository) : BaseViewModel() {
    var companyName = ObservableField<String>()
    var errorCompanyName = ObservableInt()

    var workYear = ObservableField<String>()
    var errorWorkYear = ObservableInt()

    var workMonth = ObservableField<String>()
    var errorWorkMonth = ObservableInt()

    private val _inputAreValid = SingleLiveData<StatementUiModel>()
    val inputAreValid: LiveData<StatementUiModel>
        get() = _inputAreValid

    val numberOneYesAnswer = ObservableField<Boolean>()
    val numberOneNoAnswer = ObservableBoolean()
    val numberTwoYesAnswer = ObservableBoolean()
    val numberTwoNoAnswer = ObservableBoolean()
    val numberThreeYesAnswer = ObservableBoolean()
    val numberThreeNoAnswer = ObservableBoolean()

    val ethicCodeChecked = ObservableBoolean()
    val employmentContractChecked = ObservableBoolean()
    val agencyLetterChecked = ObservableBoolean()

    val numberOneQuestion = ObservableField<String>()
    val numberTwoQuestion = ObservableField<String>()
    val numberThreeQuestion = ObservableField<String>()

    val agentCandidateName = ObservableField<String>()
    val agentCandidateKtpNumber = ObservableField<String>()

    val displaySecondQuestionMoreAnswer = ObservableBoolean()

    private val _downloadAgencyLetterPdfSuccess = SingleLiveData<DownloadAgencyContractUiModel>()
    val downloadAgencyLetterPdfSuccess: LiveData<DownloadAgencyContractUiModel>
        get() = _downloadAgencyLetterPdfSuccess

    fun findAllSavedData(registrationId: String) {
        mCompositeDisposable += repository.findAllSavedAnswer(registrationId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { data -> onRetrieveSavedDataSuccessful(data) },
                { throwable -> onRetrieveSavedDataFailed(throwable) }
            )
    }

    private fun updateQuestionnaireDetailAnswer(id: Long, counterId: Long, answer: String?, isActive: Boolean) {
        mCompositeDisposable += Maybe.fromCallable { repository.updateDetailAnswer(id, counterId, answer, isActive) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onUpdateQuestionnaireAnswerDetailSuccessful() },
                { throwable -> onUpdateQuestionnaireAnswerDetailFailed(throwable) }
            )
    }

    private fun updateEthicCode(registrationId: String, isEthicCodeChecked: Boolean, timestamp: String) {
        mCompositeDisposable += Maybe.fromCallable { repository.updateEthicCodeChecked(registrationId, isChecked(isEthicCodeChecked), getMillis(isEthicCodeChecked, timestamp)) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onUpdateEthicCodeSuccessful() },
                { throwable -> onUpdateEthicCodeFailed(throwable) }
            )
    }

    private fun updateEmploymentContract(registrationId: String, isEmploymentContractChecked: Boolean, timestamp: String) {
        mCompositeDisposable += Maybe.fromCallable { repository.updateEmploymentContract(registrationId, isChecked(isEmploymentContractChecked), getMillis(isEmploymentContractChecked, timestamp)) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onUpdateEmploymentContractSuccessful() },
                { throwable -> onUpdateEmploymentContractFailed(throwable) }
            )
    }

    private fun updateAgencyLetter(registrationId: String, isAgencyLetterChecked: Boolean, isStatementCompleted: Boolean, timestamp: String) {
        mCompositeDisposable += Completable.fromCallable { repository.updateAgencyLetter(registrationId, isChecked(isAgencyLetterChecked), isStatementCompleted, getMillis(isAgencyLetterChecked, timestamp)) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onUpdateAgencyLetterSuccessful() },
                { throwable -> onUpdateAgencyLetterFailed(throwable) }
            )
    }

    private fun updateQuestionnaire(counterId: Long, registrationId: String, answer: Int) {
        mCompositeDisposable += Completable.fromCallable { repository.updateAnswer(counterId, registrationId, answer) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onUpdateQuestionnaireSuccessful(counterId, answer) },
                { throwable -> onUpdateQuestionnaireFailed(throwable) }
            )
    }

    private fun updateQuestionnaireCompleted(registrationId: String, millis: String) {
        mCompositeDisposable += Completable.fromCallable { repository.updateQuestionnaireCompleted(registrationId, millis) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onupdateQuestionnaireCompletedSuccessful() },
                { throwable -> onupdateQuestionnaireCompletedFailed(throwable) }
            )
    }

    private fun onupdateQuestionnaireCompletedFailed(throwable: Throwable?) {
        Timber.v("Failed to update questionnaire to completed. ${throwable?.message}")
    }

    private fun onupdateQuestionnaireCompletedSuccessful() {
        Timber.v("Successfully update questionnaire to completed")
    }

    fun validateAllAnswer(
        registrationId: String,
        selectedFirstGroupRadioButtonIndex: Int,
        selectedSecondGroupRadioButtonIndex: Int,
        selectedThirdGroupRadioButtonIndex: Int,
        ethicCodeChecked: Boolean,
        employmentContractChecked: Boolean,
        currentTime: String
    ) {

        if (!isFirstQuestionAnswered(registrationId, selectedFirstGroupRadioButtonIndex)) {
            setError(true, "Pertanyaan pertama harus diisi")
            return
        }

        if (!isSecondQuestionAnswered(registrationId, selectedSecondGroupRadioButtonIndex)) {
            setError(true, "Pertanyaan kedua harus diisi")
            return
        }

        if (!isMoreAnswerAnswered(selectedSecondGroupRadioButtonIndex)) {
            return
        }

        if (!isThirdQuestionAnswered(registrationId, selectedThirdGroupRadioButtonIndex)) {
            setError(true, "Pertanyaan ketiga harus diisi")
            return
        }

        if (!ethicCodeChecked) {
            setError(true, "Checkbox kode etik harus dicentang")
            return
        }

        if (!employmentContractChecked) {
            setError(true, "Checkbox kontrak keagenan harus dicentang")
            return
        }

        //All is valid
        updateEmploymentContract(registrationId, employmentContractChecked, currentTime)
        updateEthicCode(registrationId, ethicCodeChecked, currentTime)
        updateQuestionnaireCompleted(registrationId, currentTime)

        setError(false, "Data berhasil disimpan")
    }

    fun fetchDataForAgencyLetter(registrationId: String) {
        mCompositeDisposable += repository.findDataForAgencyLetter(registrationId)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { setIsLoading(true) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { data -> onRetrieveDataForAgencyLetterSuccessful(registrationId, data) },
                { throwable -> onRetrieveDataForAgencyLetterFailed(throwable) }
            )
    }

    private fun downloadAgencyLetter(registrationId: String, birthPlace: String, title: String, address: String) {
        mCompositeDisposable += repository.downloadAgencyLetter(registrationId, birthPlace, "SMiLe go!", title, address)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { setIsLoading(true) }
            .doOnTerminate { setIsLoading(false) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { data -> onDownloadAgencyLetterSuccessful(data, registrationId) },
                { throwable -> onDownloadAgencyLetterFailed(throwable) }
            )

    }

    fun validateAgencyLetterAnswer(registrationId: String, isAgencyLetterChecked: Boolean) {
        if (!isAgencyLetterChecked) {
            setError(true, "Checkbox surat keagenan harus dicentang")
        }else {
            updateAgencyLetter(registrationId, isAgencyLetterChecked, true, DateUtils.getCurrentTime())
            setError(false, null)
        }
    }

    private fun isFirstQuestionAnswered(registrationId: String, selectedFirstGroupRadioButtonIndex: Int): Boolean {
        when (selectedFirstGroupRadioButtonIndex) {
            -1 -> {
                //Not selected
                return false
            }
            0 -> {
                //Yes
                updateQuestionnaire(1, registrationId, QuestionnaireAnswer.YES.value)
                return true
            }
            1 -> {
                //No
                updateQuestionnaire(1, registrationId, QuestionnaireAnswer.NO.value)
                return true
            }
            else -> return false
        }
    }

    private fun isSecondQuestionAnswered(registrationId: String, selectedSecondGroupRadioButtonIndex: Int): Boolean {
        when (selectedSecondGroupRadioButtonIndex) {
            -1 -> {
                //Not selected
                return false
            }
            0 -> {
                //Yes
                updateQuestionnaire(2, registrationId, QuestionnaireAnswer.YES.value)

                return true

            }
            1 -> {
                //No
                updateQuestionnaire(2, registrationId, QuestionnaireAnswer.NO.value)
                return true
            }
            else -> return false
        }

    }

    private fun isThirdQuestionAnswered(registrationId: String, selectedThirdGroupRadioButtonIndex: Int): Boolean {
        when (selectedThirdGroupRadioButtonIndex) {
            -1 -> {
                //Not selected
                return false
            }
            0 -> {
                //Yes
                updateQuestionnaire(3, registrationId, QuestionnaireAnswer.YES.value)
                return true
            }
            1 -> {
                //No
                updateQuestionnaire(3, registrationId, QuestionnaireAnswer.NO.value)
                return true
            }
            else -> return false
        }

    }

    private fun isMoreAnswerAnswered(selectedSecondGroupRadioButtonIndex: Int): Boolean {
        //Yes answer
        if (selectedSecondGroupRadioButtonIndex == 0) {

            if (!isValidCompanyName()) {
                return false
            }
            if (!isValidWorkYear()) {
                return false
            }
            if (!isValidWorkMonth()) {
                return false
            }

            return true

        }

        return true
    }

    private fun writePdfToDisk(body: ResponseBody?, outputFileName: String): DownloadPdfModel {
        try {

            val directory =
                File(Environment.getExternalStorageDirectory().toString() + File.separator + "/SMiLeGo" + "/PDF/")
            if (!directory.exists()) {
                directory.mkdirs()
            }

            val fileName = "$directory/$outputFileName"

            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null

            try {
                val fileReader = ByteArray(4096)

                var fileSizeDownloaded: Long = 0

                inputStream = body?.byteStream()
                outputStream = FileOutputStream(fileName)

                while (true) {
                    val read = inputStream!!.read(fileReader)

                    if (read == -1) {
                        break
                    }

                    outputStream.write(fileReader, 0, read)

                    fileSizeDownloaded += read.toLong()

                }

                outputStream.flush()

                return DownloadPdfModel(false, "")
            } catch (e: IOException) {
                return DownloadPdfModel(true, "Terjadi kesalahan dalam membuat file PDF ${e.message}")
            } finally {
                inputStream?.close()
                outputStream?.close()
            }
        } catch (e: IOException) {
            return DownloadPdfModel(true, "Terjadi kesalahan dalam membuat file PDF ${e.message}")
        }
    }

    //Ask user to fill company name, work duration (month and year) for question number 2 if user selected answer is 'YES'
    private fun onUpdateQuestionnaireSuccessful(counterId: Long, answer: Int) {

        if (isSecondQuestionAnswerUpdated(counterId) && isYesAnswer(answer)) {
            displaySecondQuestionMoreAnswer(true)
        } else if (isSecondQuestionAnswerUpdated(counterId) && !isYesAnswer(answer)) {
            displaySecondQuestionMoreAnswer(false)
        }
    }

    private fun onUpdateQuestionnaireFailed(throwable: Throwable?) {
        Timber.v("Failed to update questionnaire ${throwable?.message}")
    }

    private fun onUpdateQuestionnaireAnswerDetailSuccessful() {
        Timber.v("Successfully update question answer detail")

    }

    private fun onUpdateQuestionnaireAnswerDetailFailed(throwable: Throwable?) {
        Timber.v("Failed to update questionnaire answer detail ${throwable?.message}")
    }

    private fun onUpdateEthicCodeSuccessful() {
        Timber.v("Successfully update ethic code")

    }

    private fun onUpdateEthicCodeFailed(throwable: Throwable?) {
        Timber.v("Failed to update ethic code ${throwable?.message}")
    }

    private fun onUpdateEmploymentContractSuccessful() {
        Timber.v("Successfully update employment contract")

    }

    private fun onUpdateEmploymentContractFailed(throwable: Throwable?) {
        Timber.v("Failed to update employment contract ${throwable?.message}")
    }

    private fun onUpdateAgencyLetterSuccessful() {
        Timber.v("Successfully update agency letter successful")

    }

    private fun onUpdateAgencyLetterFailed(throwable: Throwable?) {
        Timber.v("Failed to update agency letter ${throwable?.message}")
    }

    private fun onRetrieveSavedDataSuccessful(data: AgentCandidateAndAllQuestionnaire) {
        reloadSavedQuestionnaire(data.questionnaire)
        reloadSavedStatement(data.agentCandidate?.statement)
        reloadAgencyLetter(data)
    }

    private fun onRetrieveSavedDataFailed(throwable: Throwable?) {
        Timber.v("Failed to get questionnaires ${throwable?.message}")
    }

    private fun onDownloadAgencyLetterSuccessful(data: ResponseBody, registrationId: String) {
        val fileName = "SURAT_KEAGENAN_${registrationId}_${DateUtils.getCurrentTimeForFile()}.pdf"

        val response = writePdfToDisk(data, fileName)
        setWritePdfSuccessful(response.error, response.message, fileName)

    }

    private fun onRetrieveDataForAgencyLetterSuccessful(registrationId: String, data: AgencyLetterData) {
        downloadAgencyLetter(registrationId, data.placeOfBirth!!, data.titleName!!, data.homeAddress!!)
    }

    private fun onRetrieveDataForAgencyLetterFailed(throwable: Throwable?) {
        Timber.v("Failed to retrieve data for agency letter ${throwable?.message}")
    }

    private fun onDownloadAgencyLetterFailed(throwable: Throwable?) {
        setWritePdfSuccessful(true, "Download pdf failed ${throwable?.message}", null)
    }

    private fun reloadSavedQuestionnaire(questionnaires: List<QuestionnaireAndAllQuestionnaireDetail>) {

        //Load question
        val firstQuestion = questionnaires[0].questionnaire
        val secondQuestion = questionnaires[1].questionnaire
        val secondQuestionMoreAnswer = questionnaires[1].questionnairesDetail
        val thirdQuestion = questionnaires[2].questionnaire

        numberOneQuestion.set(firstQuestion?.question)
        numberTwoQuestion.set(secondQuestion?.question)
        numberThreeQuestion.set(thirdQuestion?.question)

        //First question
        if (firstQuestion?.answer == QuestionnaireAnswer.YES.value) {
            numberOneYesAnswer.set(true)
            numberOneNoAnswer.set(false)
        } else if (firstQuestion?.answer == QuestionnaireAnswer.NO.value) {
            numberOneYesAnswer.set(false)
            numberOneNoAnswer.set(true)
        }

        //Second question
        if (secondQuestion?.answer == QuestionnaireAnswer.YES.value) {

            numberTwoYesAnswer.set(true)
            numberTwoNoAnswer.set(false)

            //Show more detailed answer

            val companyName = secondQuestionMoreAnswer[0].answer
            val workDurationYear = secondQuestionMoreAnswer[1].answer
            val workDurationMonth = secondQuestionMoreAnswer[2].answer

            displaySecondQuestionMoreAnswer(true)

            this.companyName.set(companyName)
            workYear.set(workDurationYear)
            workMonth.set(workDurationMonth)

        } else if (secondQuestion?.answer == QuestionnaireAnswer.NO.value) {
            numberTwoYesAnswer.set(false)
            numberTwoNoAnswer.set(true)

            displaySecondQuestionMoreAnswer(false)
        }

        //Third question
        if (thirdQuestion?.answer == QuestionnaireAnswer.YES.value) {
            numberThreeYesAnswer.set(true)
            numberThreeNoAnswer.set(false)
        } else if (thirdQuestion?.answer == QuestionnaireAnswer.NO.value) {
            numberThreeYesAnswer.set(false)
            numberThreeNoAnswer.set(true)
        }

    }

    private fun reloadSavedStatement(statement: Statement?) {
        if (statement?.ethicCodeChecked!!){
            ethicCodeChecked.set(statement.ethicCodeChecked!!)
            employmentContractChecked.set(statement.employmentContractChecked!!)
        }
    }

    private fun reloadAgencyLetter(data: AgentCandidateAndAllQuestionnaire) {
        agencyLetterChecked.set(data.agentCandidate?.statement?.agencyLetterChecked!!)
        agentCandidateName.set(data.agentCandidate?.personalData?.name)
        agentCandidateKtpNumber.set(data.agentCandidate?.personalData?.identityNumber)
    }

    fun displayMoreAnswer(shouldShow: Int) {
        if (shouldShow == 1) {
            displaySecondQuestionMoreAnswer(true)
            return
        }

        displaySecondQuestionMoreAnswer(false)
    }

    private fun isSecondQuestionAnswerUpdated(questionId: Long): Boolean {
        if (questionId == 2L) {
            return true
        }
        return false
    }

    private fun isYesAnswer(answer: Int): Boolean {
        if (answer == QuestionnaireAnswer.YES.value) {
            return true
        }
        return false
    }

    private fun isChecked(isChecked: Boolean): Int {
        if (isChecked) {
            return 1
        }
        return 0
    }

    private fun getMillis(isChecked: Boolean, millis: String): String? {
        if (isChecked) {
            return millis
        }
        return null
    }

    private fun displaySecondQuestionMoreAnswer(shouldDisplay: Boolean) {
        displaySecondQuestionMoreAnswer.set(shouldDisplay)
    }


    private fun isValidCompanyName(): Boolean {
        if (!companyName.get().isNullOrEmpty()) {
            updateQuestionnaireDetailAnswer(1, 2, companyName.get()!!, true)
            setErrorCompanyName()
            return true
        }

        updateQuestionnaireDetailAnswer(1, 2, null, false)
        setErrorCompanyName(R.string.no_empty_field)
        return false
    }

    private fun isValidWorkYear(): Boolean {
        if (workYear.get().isNullOrEmpty()) {
            setErrorWorkYear(R.string.no_empty_field)
            return false
        } else if (workYear.get()!!.startsWith("0")) {
            setErrorWorkYear(R.string.start_from_zero)
            return false
        }
        setErrorWorkYear()
        updateQuestionnaireDetailAnswer(2, 2, workYear.get()!!, true)
        return true
    }

    private fun isValidWorkMonth(): Boolean {
        if (workMonth.get().isNullOrEmpty()) {
            setErrorWorkMonth(R.string.no_empty_field)
            return false
        } else if (workMonth.get()!!.startsWith("0")) {
            setErrorWorkMonth(R.string.start_from_zero)
            return false
        } else if (!workMonth.get().isNullOrEmpty() && workMonth.get()!!.toInt() > 11) {
            setErrorWorkMonth(R.string.duration_work)
            return false
        }

        updateQuestionnaireDetailAnswer(3, 2, workMonth.get()!!, true)

        setErrorWorkMonth()
        return true
    }

    private fun setErrorCompanyName(message: Int = 0) = errorCompanyName.set(message)
    private fun setErrorWorkYear(message: Int = 0) = errorWorkYear.set(message)
    private fun setErrorWorkMonth(message: Int = 0) = errorWorkMonth.set(message)

    private fun setError(isError: Boolean, message: String?) {
        _inputAreValid.sendAction(StatementUiModel(isError, message))
    }

    private fun setWritePdfSuccessful(error: Boolean, message: String?, fileName: String?) {
        _downloadAgencyLetterPdfSuccess.sendAction(DownloadAgencyContractUiModel(error, message, fileName))
    }

}