package co.id.ajsmsig.ismile.ui.agent.previewcandidateagent

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.model.PreviewCandidateAgentModel
import co.id.ajsmsig.ismile.util.DateUtils
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import co.id.ajsmsig.ismile.ui.previewagentcandidate.PreviewCandidateAgentRepository


class PreviewCandidateAgentViewModel @Inject constructor(private val repository: PreviewCandidateAgentRepository) : BaseViewModel() {

    val candidateAgentModel = ObservableField<PreviewCandidateAgentModel>()

    private val _isConnected = MutableLiveData<PreviewCandidateAgentModel>()
    val isConnected: LiveData<PreviewCandidateAgentModel>
        get() = _isConnected

    fun getCandidateAgentData(registrationId: String) {
        isLoading.set(true)
        mCompositeDisposable += repository.getDataCandidateAgent(registrationId)
            .map {
                val bop = DateUtils.getDateFormat(it.data.bopDate)
                val bod = DateUtils.getDateFormat(it.data.bod)
                PreviewCandidateAgentModel(it.data.fullName, bod, it.data.idCardNo, it.data.phoneNo, it.data.email, it.data.titleName, bop,it.data.gender, it.data.maritalStatus, it.data.profpicImagePath, it.error, it.message) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {data-> getAgentCandidateDataSuccess(data)},
                {error-> getAgentCandidateDataFailed (error)}
            )
    }

    private fun getAgentCandidateDataSuccess(data: PreviewCandidateAgentModel) {
        isLoading.set(false)
        candidateAgentModel.set(data)
        _isConnected.value = data
    }

    private fun getAgentCandidateDataFailed(error: Throwable) {
        isLoading.set(false)
    }

}