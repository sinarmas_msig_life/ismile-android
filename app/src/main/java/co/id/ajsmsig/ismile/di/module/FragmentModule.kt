package co.id.ajsmsig.ismile.di.module

import co.id.ajsmsig.ismile.ui.agent.addagentcandidate.AddAgentCandidateFragment
import co.id.ajsmsig.ismile.ui.candidate.agentcandidate.AgentCandidateFragment
import co.id.ajsmsig.ismile.ui.leader.agentleaderapprove.AgentLeaderCheckedApprovalFragment
import co.id.ajsmsig.ismile.ui.leader.agentleaderfailed.AgentLeaderCheckedFailedFragment
import co.id.ajsmsig.ismile.ui.leader.agentleadersucces.AgentLeaderCheckedSuccesFragment
import co.id.ajsmsig.ismile.ui.leader.agentleader.AgentLeaderFragment
import co.id.ajsmsig.ismile.ui.leader.detailapprove.DetailAgentCandidateApproveFragment
import co.id.ajsmsig.ismile.ui.agent.forgotpasswordagent.ForgotPasswordAgentFragment
import co.id.ajsmsig.ismile.ui.candidate.forgotpasswordagentcandidate.ForgotPasswordAgentCandidateFragment
import co.id.ajsmsig.ismile.ui.agent.home.HomeFragment
import co.id.ajsmsig.ismile.ui.candidate.licency.LicenseFragment
import co.id.ajsmsig.ismile.ui.agent.login.LoginFragment
import co.id.ajsmsig.ismile.ui.candidate.logincandidateagent.LoginCandidateAgentFragment
import co.id.ajsmsig.ismile.ui.agent.logintype.ChooseLoginTypeFragment
import co.id.ajsmsig.ismile.ui.agent.notification.NotificationFragment
import co.id.ajsmsig.ismile.ui.agent.previewcandidateagent.PreviewCandidateAgentFragment
import co.id.ajsmsig.ismile.ui.candidate.profileagentcandidate.ProfileAgentCandidateFragment
import co.id.ajsmsig.ismile.ui.agent.recruit.RecruitFragment
import co.id.ajsmsig.ismile.ui.agent.recruitfailed.RecruitFailedFragment
import co.id.ajsmsig.ismile.ui.agent.recruitsuccess.RecruitSuccessFragment
import co.id.ajsmsig.ismile.ui.candidate.resetpasswordagentcandidate.InfoResetPasswordAgentCandidateFragment
import co.id.ajsmsig.ismile.ui.candidate.resetpasswordsucces.ResetPasswordSuccesFragment
import co.id.ajsmsig.ismile.ui.candidate.signature.SignatureAgentCandidateFragment
import co.id.ajsmsig.ismile.ui.splashfragment.SplashFragment
import co.id.ajsmsig.ismile.ui.candidate.statement.AgencyLetterFragment
import co.id.ajsmsig.ismile.ui.candidate.statement.StatementFragment
import co.id.ajsmsig.ismile.ui.candidate.submitlicense.LicenseSubmitFragment
import co.id.ajsmsig.ismile.ui.candidate.workexperience.WorkExperienceFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract fun contributesLoginFragment() : LoginFragment

    @ContributesAndroidInjector
    abstract fun contributesChooseLoginTypeFragment() : ChooseLoginTypeFragment

    @ContributesAndroidInjector
    abstract fun contributesHomeFragment() : HomeFragment

    @ContributesAndroidInjector
    abstract fun contributesAddAgentCandidateFragment() : AddAgentCandidateFragment

    @ContributesAndroidInjector
    abstract fun contributesCandidateAgentFragment(): AgentCandidateFragment

    @ContributesAndroidInjector
    abstract fun contributesProfileAgentFragment(): ProfileAgentCandidateFragment

    @ContributesAndroidInjector
    abstract fun contributesNotificationFragment() : NotificationFragment

    @ContributesAndroidInjector
    abstract fun contributesInputStatmentFragment() : StatementFragment

    @ContributesAndroidInjector
    abstract fun contributesAgencyLetterFragment() : AgencyLetterFragment

    @ContributesAndroidInjector
    abstract fun contributesRecruitFragment() : RecruitFragment

    @ContributesAndroidInjector
    abstract fun contributesRecruitFailedFragment() : RecruitFailedFragment


    @ContributesAndroidInjector
    abstract fun contributesRecruitSuccessFragment() : RecruitSuccessFragment

    @ContributesAndroidInjector
    abstract fun contributesLoginCandidateAgentFragment() : LoginCandidateAgentFragment

    @ContributesAndroidInjector
    abstract fun contributesForgotPasswordCancidateAgentFragment() : ForgotPasswordAgentCandidateFragment

    @ContributesAndroidInjector
    abstract fun contributesForgotPasswordAgentFragment() : ForgotPasswordAgentFragment

    @ContributesAndroidInjector
    abstract fun contributesResetPasswordSuccesFragment() : ResetPasswordSuccesFragment

    @ContributesAndroidInjector
    abstract fun contributesSplashFragment() : SplashFragment

    @ContributesAndroidInjector
    abstract fun contributesPreviewCandidateAgentFragment() : PreviewCandidateAgentFragment

    @ContributesAndroidInjector
    abstract fun contributesInfoResetPasswordAgentCandidateFragment() : InfoResetPasswordAgentCandidateFragment

    @ContributesAndroidInjector
    abstract fun contributesWorkExperienceFragment() : WorkExperienceFragment

    @ContributesAndroidInjector
    abstract fun contributesSignatureFragment() : SignatureAgentCandidateFragment

    @ContributesAndroidInjector
    abstract fun contributesAgentLeaderFragment() : AgentLeaderFragment

    @ContributesAndroidInjector
    abstract fun contributesAgentLeaderCheckedApprovalFragment() : AgentLeaderCheckedApprovalFragment

    @ContributesAndroidInjector
    abstract fun contributesAgentLeaderCheckedSuccesFragment() : AgentLeaderCheckedSuccesFragment

    @ContributesAndroidInjector
    abstract fun contributesAgentLeaderCheckedFailedFragment() : AgentLeaderCheckedFailedFragment

    @ContributesAndroidInjector
    abstract fun contributesDetailAgentCandidateApproveFragment() : DetailAgentCandidateApproveFragment

    @ContributesAndroidInjector
    abstract fun contributesLicenseFragment() : LicenseFragment

    @ContributesAndroidInjector
    abstract fun contributesLicenseSubmitFragment() : LicenseSubmitFragment
}