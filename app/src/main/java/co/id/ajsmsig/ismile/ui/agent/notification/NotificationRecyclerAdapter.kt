package co.id.ajsmsig.ismile.ui.agent.notification

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.id.ajsmsig.ismile.databinding.RvItemNotificationBinding
import co.id.ajsmsig.ismile.model.NotificationModel

class NotificationRecyclerAdapter(
    private var notificationList: List<NotificationModel>,
    private val listener: onNotificationPressedListener
) : RecyclerView.Adapter<NotificationRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RvItemNotificationBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = notificationList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(notificationList[position], listener, holder.adapterPosition)

    class ViewHolder(private val binding:RvItemNotificationBinding ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(model: NotificationModel, listener: onNotificationPressedListener?, position: Int) {
            binding.model = model

            binding.root.setOnClickListener {
                listener?.onNotificationSelected(model, position)
            }

            binding.executePendingBindings()
        }


    }

    interface onNotificationPressedListener {
        fun onNotificationSelected(notification: NotificationModel, position: Int)
    }

    fun refreshData(notificationList: List<NotificationModel>) {
        this.notificationList = notificationList
        notifyDataSetChanged()
    }

}