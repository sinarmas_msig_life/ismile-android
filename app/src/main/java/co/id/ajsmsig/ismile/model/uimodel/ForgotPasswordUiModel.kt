package co.id.ajsmsig.ismile.model.uimodel

data class ForgotPasswordUiModel (val findAccount:Boolean, val message:String)