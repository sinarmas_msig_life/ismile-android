package co.id.ajsmsig.ismile.db.entity.embedded

import androidx.room.ColumnInfo
import androidx.room.TypeConverters
import co.id.ajsmsig.ismile.util.Converters

data class Statement(
    //halaman 1 dari 4

    //masuk ke table questionnaires

    //halaman 2 dari 4
    @TypeConverters(Converters::class)
    @ColumnInfo(name = "ethic_code_checked")
    var ethicCodeChecked: Boolean?,

    //halaman 3 dari 4
    @TypeConverters(Converters::class)
    @ColumnInfo(name = "employment_contract_checked")
    var employmentContractChecked: Boolean?,

    //halaman 4 dari 4
    @TypeConverters(Converters::class)
    @ColumnInfo(name = "agency_letter_checked")
    var agencyLetterChecked: Boolean?
)