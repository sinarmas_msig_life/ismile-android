package co.id.ajsmsig.ismile.model.api

import com.google.gson.annotations.SerializedName

data class GetAgentTitleResponse(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("error")
    val error: Boolean,
    @SerializedName("message")
    val message: String
) {
    data class Data(
        @SerializedName("title_id")
        val titleId: Int,
        @SerializedName("title_name")
        val titleName: String
    )
}