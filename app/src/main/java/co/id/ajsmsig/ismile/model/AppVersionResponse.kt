package co.id.ajsmsig.ismile.model

data class AppVersionResponse(
    val data: AppVersionData,
    val error: Boolean,
    val message: String
)

data class AppVersionData(
    val app_name: String,
    val desc_app: String,
    val version_code: Int,
    val version_name: String
)