package co.id.ajsmsig.ismile.model

import com.google.gson.annotations.SerializedName

data class AgentTitle(val titleId: Int, val titleName: String)