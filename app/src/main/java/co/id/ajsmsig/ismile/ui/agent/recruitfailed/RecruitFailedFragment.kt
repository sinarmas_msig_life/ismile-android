package co.id.ajsmsig.ismile.ui.agent.recruitfailed

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.common.ResultState
import co.id.ajsmsig.ismile.databinding.FragmentRecruitFailedBinding
import co.id.ajsmsig.ismile.model.StatusRecruit
import co.id.ajsmsig.ismile.ui.agent.home.HomeFragmentDirections
import co.id.ajsmsig.ismile.util.RECRUIT_FAILED_REQUEST_CODE

class RecruitFailedFragment : BaseFragment<FragmentRecruitFailedBinding, RecruitFailedViewModel>(), RecruitFailedRecyclerAdapter.OnRecruitmentStatusPressedListener {

    override fun getLayoutResourceId() = R.layout.fragment_recruit_failed

    override fun getViewModelClass() = RecruitFailedViewModel::class.java
    private val adapter = RecruitFailedRecyclerAdapter(listOf(), this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()

        vm.getAllRecruitFiled(RECRUIT_FAILED_REQUEST_CODE)
        vm.recruits.observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it){
                    is ResultState.Error -> setResultRecruits(emptyList(), false, getString(R.string.unknown_error), View.VISIBLE)
                    is ResultState.TimeOut -> setResultRecruits(emptyList(), false, getString(R.string.timeout), View.VISIBLE)
                    is ResultState.NoData -> setResultRecruits(emptyList(), true, getString(R.string.no_data_agent), View.VISIBLE)
                    is ResultState.NoInternetConnection -> setResultRecruits(emptyList(), false, getString(R.string.no_internet_connection), View.VISIBLE)
                    is ResultState.HasData ->setResultRecruits(it.data, true, "", View.GONE)
                    else -> setResultRecruits(emptyList(), false, getString(R.string.unknown_error), View.VISIBLE)
                }
            }
        })

        binding.swipeRecruitFailed.setOnRefreshListener {
            vm.getAllRecruitFiled(RECRUIT_FAILED_REQUEST_CODE)
        }
    }

    private fun setResultRecruits(data: List<StatusRecruit>, isRefresh: Boolean, message: String, visibility: Int) {
        binding.swipeRecruitFailed.isRefreshing = isRefresh
        binding.tvMessageRecruitFailed.text = message
        binding.tvMessageRecruitFailed.visibility = visibility
        refreshData(data)
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(activity)
        val dividerItemDecoration = DividerItemDecoration(binding.recyclerView.context, layoutManager.orientation)

        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.addItemDecoration(dividerItemDecoration)
        binding.recyclerView.adapter = adapter
    }

    override fun onRecruitFailedPressed(candidateAgent: StatusRecruit, position: Int) {
        launchPreviewAgentCandidate(candidateAgent.agentCandidateRegistrationId)
    }

    private fun refreshData(recruits : List<StatusRecruit>) {
        binding.swipeRecruitFailed.isRefreshing = false
        adapter.refreshData(recruits)
    }

    private fun launchPreviewAgentCandidate(registrationId : String) {
        val action = HomeFragmentDirections.actionHomeFragmentToPreviewCandidateAgentFragment(registrationId)
        findNavController().navigate(action)
    }
}
