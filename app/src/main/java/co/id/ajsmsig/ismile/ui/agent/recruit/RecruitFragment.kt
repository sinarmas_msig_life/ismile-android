package co.id.ajsmsig.ismile.ui.agent.recruit


import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.common.ResultState
import co.id.ajsmsig.ismile.databinding.FragmentRecruitBinding
import co.id.ajsmsig.ismile.model.StatusRecruit
import co.id.ajsmsig.ismile.ui.agent.home.HomeFragmentDirections
import co.id.ajsmsig.ismile.util.ALL_RECRUIT_REQUEST_CODE

class RecruitFragment : BaseFragment<FragmentRecruitBinding, RecruitViewModel>(), RecruitRecyclerAdapter.OnRecruitmentStatusPressedListener {

    override fun getLayoutResourceId() = R.layout.fragment_recruit
    override fun getViewModelClass() = RecruitViewModel::class.java
    private  val adapter = RecruitRecyclerAdapter(listOf(), this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()

        vm.getAllRecruit(ALL_RECRUIT_REQUEST_CODE)
        vm.recruits.observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it){
                    is ResultState.Error -> setResultRecruits(emptyList(), false, getString(R.string.unknown_error), View.VISIBLE)
                    is ResultState.TimeOut -> setResultRecruits(emptyList(), false, getString(R.string.timeout), View.VISIBLE)
                    is ResultState.NoData -> setResultRecruits(emptyList(), true, getString(R.string.no_data_agent), View.VISIBLE)
                    is ResultState.NoInternetConnection -> setResultRecruits(emptyList(), false, getString(R.string.no_internet_connection), View.VISIBLE)
                    is ResultState.HasData ->setResultRecruits(it.data, true, "", View.GONE)
                    else -> setResultRecruits(emptyList(), false, getString(R.string.unknown_error), View.VISIBLE)
                }
            }
        })

        binding.swipeRectuit.setOnRefreshListener {
            vm.getAllRecruit(ALL_RECRUIT_REQUEST_CODE)
        }
    }

    private fun setResultRecruits(data: List<StatusRecruit>, isRefresh: Boolean, message: String, visibility: Int) {
        binding.swipeRectuit.isRefreshing = isRefresh
        binding.tvMessage.text = message
        binding.tvMessage.visibility = visibility
        refreshData(data)
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(activity)
        val dividerItemDecoration = DividerItemDecoration(getDataBinding().recyclerView.context, layoutManager.orientation)
        getDataBinding().recyclerView.layoutManager = layoutManager
        getDataBinding().recyclerView.adapter = adapter
        getDataBinding().recyclerView.addItemDecoration(dividerItemDecoration)
    }

    override fun onRecruitPressed(candidateAgent: StatusRecruit, position: Int) {
        launchPreviewAgentCandidate(candidateAgent.agentCandidateRegistrationId)
    }

    private fun refreshData(recruits : List<StatusRecruit>) {
        getDataBinding().swipeRectuit.isRefreshing = false
        adapter.refreshData(recruits)
    }

    private fun launchPreviewAgentCandidate(registrationId : String) {
        val action = HomeFragmentDirections.actionHomeFragmentToPreviewCandidateAgentFragment(registrationId)
        findNavController().navigate(action)
    }

}
