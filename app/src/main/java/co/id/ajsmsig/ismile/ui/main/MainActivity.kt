package co.id.ajsmsig.ismile.ui.main

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.databinding.ActivityMainBinding
import co.id.ajsmsig.ismile.ui.agent.login.LoginViewModel
import co.id.ajsmsig.ismile.util.PREF_KEY_AGENT_CODE
import co.id.ajsmsig.ismile.util.PREF_NAME
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity: AppCompatActivity(){

    private lateinit var toolbar : Toolbar
    private lateinit var binding : ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var viewModel: LoginViewModel
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)

        val navHostFragment= supportFragmentManager.findFragmentById(R.id.nav_host) as NavHostFragment
        val inflater = navHostFragment.navController.navInflater
        val graph = inflater.inflate(R.navigation.navigation)
        binding.bottomNavigationView.inflateMenu(R.menu.menu_main)
        navHostFragment.navController.graph = graph
        navController = navHostFragment.navController

        setupToolbar()
        setupBottomNavigation()
        initFirebaseCrashlytics()
    }

    private fun setupToolbar() {
        toolbar = binding.contentToolbar.toolbar
        setSupportActionBar(toolbar)
        setupActionBarWithNavController(navController)
    }

    private fun setupBottomNavigation() {
        navController.addOnDestinationChangedListener(navigationListener)
        navController.addOnDestinationChangedListener(navigationListener)
    }

    private val navigationListener = NavController.OnDestinationChangedListener  {  _, destination, _ ->
        when(destination.id) {
            R.id.fragmentChooseLoginRole -> {
                showBottomNavigation(false)
                showToolbarBackArrow(false)
                showToolbar(false)
            }
            R.id.loginFragment -> {
                showBottomNavigation(false)
                showToolbar(false)
            }
            R.id.homeFragment -> {
                showBottomNavigation(false)
                showToolbar(true)
                showToolbarBackArrow(false)
            }
            R.id.fragmentAddAgentCandidate -> {
                showBottomNavigation(false)
            }
            R.id.fragmentInbox -> {
                showBottomNavigation(false)
            }
            R.id.forgotPasswordAgentFragment-> {
                showBottomNavigation(false)
                showToolbar(true)
            }
            R.id.resetPasswordResult-> {
                showBottomNavigation(false)
                showToolbar(true)
            }
            R.id.splashFragment-> {
                showBottomNavigation(false)
                showToolbar(false)
            }
            R.id.previewCandidateAgentFragment-> {
                showBottomNavigation(false)
                showToolbar(true)
            }
            R.id.loginCandidateAgentFragment -> {
                showBottomNavigation(false)
                showToolbar(false)
            }
            R.id.candidateAgentFragment -> {
                showBottomNavigation(false)
                showToolbar(true)
                showToolbarBackArrow(false)
                hideToolbarSubtitle()
            }
            R.id.statementFragment -> {
                showBottomNavigation(false)
                showToolbar(true)
                showToolbarBackArrow(true)
                changeToolbarSubtitle("Halaman 1 dari 2")
            }
            R.id.agencyLetterFragment -> {
                showBottomNavigation(false)
                showToolbar(true)
                showToolbarBackArrow(true)
                changeToolbarSubtitle("Halaman 2 dari 2")
            }
            R.id.profileAgentFragment ->{
                changeToolbarSubtitle("Halaman 1 dari 2")
                showBottomNavigation(false)
                showToolbar(true)
                showToolbarBackArrow(true)
            }
            R.id.workExperienceFragment ->{
                changeToolbarSubtitle("Halaman 2 dari 2")
                showBottomNavigation(false)
                showToolbar(true)
                showToolbarBackArrow(true)
            }
            R.id.pdfViewerFragment -> {
                showBottomNavigation(false)
                showToolbar(true)
                showToolbarBackArrow(true)
                changeToolbarSubtitle(null)
            }
            R.id.signatureFragment ->{
                showBottomNavigation(false)
                showToolbar(true)
                showToolbarBackArrow(true)
            }
            R.id.forgotPasswordCandidateAgentFragment ->{
                showBottomNavigation(false)
                showToolbar(true)
            }
            R.id.agentLeaderFragment -> {
                showBottomNavigation(false)
                showToolbar(true)
                showToolbarBackArrow(false)
            }
            R.id.detailAgentCandidateApprove -> {
                showBottomNavigation(false)
                showToolbar(true)
                showToolbarBackArrow(true)
            }
            R.id.licenseSubmitFragment -> {
                showBottomNavigation(false)
                showToolbar(true)
                showToolbarBackArrow(true)
                changeToolbarSubtitle("Halaman 2 dari 2")
            }
            R.id.licenseFragment -> {
                showBottomNavigation(false)
                showToolbar(true)
                showToolbarBackArrow(true)
                changeToolbarSubtitle("Halaman 1 dari 2")
            }
            else -> {
                showBottomNavigation(true)
                showToolbar(true)
                hideToolbarSubtitle()
                changeToolbarSubtitle(null)
            }
        }
    }

    override fun onSupportNavigateUp() = navController.navigateUp()


    private fun showToolbar(shouldShow: Boolean) = if (shouldShow) toolbar.visibility = View.VISIBLE else toolbar.visibility = View.GONE
    private fun showBottomNavigation(shouldShow : Boolean) = if (shouldShow) binding.bottomNavigationView.visibility = View.VISIBLE else binding.bottomNavigationView.visibility = View.GONE
    private fun showToolbarBackArrow(shouldShow: Boolean) = if (shouldShow) supportActionBar?.setDisplayHomeAsUpEnabled(true) else supportActionBar?.setDisplayHomeAsUpEnabled(false)

    override fun onBackPressed() {

        when (navController.currentDestination?.id) {
            R.id.agentLeaderFragment -> showExitAppConfirmation()
            R.id.fragmentChooseLoginRole -> showExitAppConfirmation()
            R.id.splashFragment -> showExitAppConfirmation()
            R.id.candidateAgentFragment -> showExitAppConfirmation()
            R.id.homeFragment -> showExitAppConfirmation()
            else -> {
                navController.navigateUp()
            }
        }
    }


    private fun showExitAppConfirmation() {
        AlertDialog.Builder(this, R.style.AlertDialogCustom)
            .setTitle(getString(R.string.confirmation))
            .setCancelable(false)
            .setMessage(getString(R.string.exit_confirmation))
            .setPositiveButton(android.R.string.ok) { dialogInterface, _ ->
                finish()
                dialogInterface.dismiss()
            }
            .setNegativeButton(android.R.string.no) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
            .show()
    }

    private fun initFirebaseCrashlytics() {
        val crashlytics = FirebaseCrashlytics.getInstance()
        val pref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        var userId = pref.getString(PREF_KEY_AGENT_CODE, null)

        crashlytics.run {
            if (userId == null) {
                userId = "0"
            }
            setUserId(userId!!)
            setCustomKey("agent-code", userId!!)
        }
    }

    private fun changeToolbarSubtitle(subtitle : String?) {
        supportActionBar?.subtitle = subtitle
    }
    private fun hideToolbarSubtitle() {
        supportActionBar?.subtitle = null
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
