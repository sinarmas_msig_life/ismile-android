package co.id.ajsmsig.ismile.ui.agent.addagentcandidate

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.di.NetManager
import co.id.ajsmsig.ismile.model.api.GetAgentTitleResponse
import co.id.ajsmsig.ismile.model.api.SubmitAgentCandidateRequest
import co.id.ajsmsig.ismile.model.api.SubmitAgentCandidateResponse
import co.id.ajsmsig.ismile.model.api.UploadFileResponse
import co.id.ajsmsig.ismile.util.PREF_KEY_AGENT_CODE
import io.reactivex.Observable
import okhttp3.MultipartBody
import javax.inject.Inject

class AddAgentCandidateRepository @Inject constructor(private val remoteDataSource: AddAgentCandidateRemoteDataSource, private val netManager: NetManager, private val preference: SharedPreferences) {

    fun uploadFile(body: MultipartBody): Observable<UploadFileResponse> {
        if (netManager.isConnectedToInternet) {
            return remoteDataSource.uploadFile(body)
        }

        return Observable.just(UploadFileResponse(true, "Gagal upload file. Mohon periksa kembali koneksi internet Anda"))
    }
    fun getAgentTitle(): Observable<GetAgentTitleResponse> {
        if (netManager.isConnectedToInternet) {
            val agentCode = preference.getString(PREF_KEY_AGENT_CODE, null)
            return remoteDataSource.getAgentTitle(agentCode!!)
        }
        return Observable.just(GetAgentTitleResponse(emptyList(), true, "Gagal mengunduh jabatan calon agent. Mohon periksa kembali koneksi internet Anda"))
    }

    fun submitAgentCandidate(firstName: String, bod: String, idCardNo: String, phoneNo: String, email: String, titleId: Int, maritalStatus : Int,genderId: Int, bopDate: String, agentCode: String, fileInBase64Format : String): Observable<SubmitAgentCandidateResponse> {

        if (netManager.isConnectedToInternet) {
            val request = SubmitAgentCandidateRequest(firstName, bod, idCardNo, phoneNo, email, titleId, maritalStatus,genderId, bopDate, agentCode, fileInBase64Format)
            return remoteDataSource.submitAgentCandidate(request)
        }

        return noConnectionObservable()
    }

    private fun noConnectionObservable(): Observable<SubmitAgentCandidateResponse> {
        val data = SubmitAgentCandidateResponse.Data("")
        val response = SubmitAgentCandidateResponse(data,true, "Tidak ada koneksi internet. Mohon periksa kembali koneksi internet Anda")
        return Observable.just(response)
    }

    fun getAgentCode() = preference.getString(PREF_KEY_AGENT_CODE, null)

}