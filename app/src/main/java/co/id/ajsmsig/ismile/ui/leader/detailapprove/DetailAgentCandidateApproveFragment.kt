package co.id.ajsmsig.ismile.ui.leader.detailapprove

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentDetailAgentCandidateApproveBinding
import co.id.ajsmsig.ismile.util.MENU_LEADER_APPROVE
import co.id.ajsmsig.ismile.util.MENU_LEADER_REQUSTED
import com.google.android.material.snackbar.Snackbar

class DetailAgentCandidateApproveFragment : BaseFragment<FragmentDetailAgentCandidateApproveBinding, DetailAgentCandidateApproveViewModel>() {

    override fun getLayoutResourceId() = R.layout.fragment_detail_agent_candidate_approve

    override fun getViewModelClass() = DetailAgentCandidateApproveViewModel::class.java

    private var registrationId: String = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registrationId = DetailAgentCandidateApproveFragmentArgs.fromBundle(arguments!!).registrationId
        val menuLeaderId = DetailAgentCandidateApproveFragmentArgs.fromBundle(arguments!!).roleMenuLeader

        checkedMenuLeader(menuLeaderId)

        vm.agentCandidateAccept(registrationId)

        vm.isSubmitSucces.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (!it.error) {
                    launchFragmentLeader()
                    Snackbar.make(getDataBinding().root, it.message!!, Snackbar.LENGTH_SHORT).show()
                } else {
                    Snackbar.make(getDataBinding().root, it.message!!, Snackbar.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun checkedMenuLeader(menuLeaderId: Int) {
        when(menuLeaderId){
            MENU_LEADER_REQUSTED -> setVisibleButton(View.VISIBLE)
            MENU_LEADER_APPROVE -> setVisibleButton(View.GONE)
            else -> setVisibleButton(View.GONE)
        }
    }

    private fun setVisibleButton(value: Int){
        binding.btnApprove.visibility = value
        binding.btnCancel.visibility = value
    }

    private fun launchFragmentLeader() {
        val action = DetailAgentCandidateApproveFragmentDirections.actionLaunchAgentLeaderFragment()
        findNavController().navigate(action)
    }

    fun onAccpetAgentCandidateClicked() {
        val reasonRejection= ""
        val statusSubmit = true
        vm.submitLeaderAccpetAgentCandidate(registrationId,  reasonRejection, statusSubmit)
    }

    fun onRejectAgentCandidateClicked() {

        val statusSubmit = false
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(R.string.rejection)

        val view = layoutInflater.inflate(R.layout.custom_reject_reason_approve, null)
        val reasonRejection = getDataBinding().customRejectReason.etReasonReject.text
        builder.setView(view)
            .setNegativeButton(android.R.string.no) {
                    dialogInterface, _ -> dialogInterface.dismiss() }
            .setPositiveButton(android.R.string.ok) { dialogInterface, _ ->
                getViewModel().submitLeaderAccpetAgentCandidate(registrationId, reasonRejection.toString(), statusSubmit)
            }
            .show()
    }
}
