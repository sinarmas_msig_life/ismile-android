package co.id.ajsmsig.ismile.model.api

import co.id.ajsmsig.ismile.util.LAUNCH_APP_ID

data class InboxRequest(val userid : String, val jenis_id : String = LAUNCH_APP_ID.toString())