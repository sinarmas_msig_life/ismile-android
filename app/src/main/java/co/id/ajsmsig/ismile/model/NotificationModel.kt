package co.id.ajsmsig.ismile.model

data class NotificationModel(val title : String, val message : String, val timestamp : String, val nextActionMenuId : Int?)