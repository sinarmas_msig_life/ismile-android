package co.id.ajsmsig.ismile.db.entity.minimal

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class AgencyLetterData (

        @ColumnInfo(name = "home_address")
        var homeAddress: String?,

        @ColumnInfo(name = "place_of_birth")
        var placeOfBirth: String?,

        @ColumnInfo(name = "title_name")
        var titleName: String?
)