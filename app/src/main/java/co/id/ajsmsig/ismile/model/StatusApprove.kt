package co.id.ajsmsig.ismile.model

data class StatusApprove (
    val registrationId: String,
    val agentCandidateName: String,
    val submitDate: String,
    val titleName: String,
    val imageProfile: String?
)