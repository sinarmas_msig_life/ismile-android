package co.id.ajsmsig.ismile.model.api


import com.google.gson.annotations.SerializedName

data class AgentCandidateLoginResponse(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("error")
    val error: Boolean,
    @SerializedName("message")
    val message: String
) {
    data class Data(

        @SerializedName("agent_code")
        val agentCode: String,
        @SerializedName("data_submit_date")
        val dataSubmitDate: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("registration_id")
        val registrationId: String,
        @SerializedName("result")
        val result: String?,
        @SerializedName("verification_date")
        val verificationDate: String?,
        @SerializedName("ktp_number")
        val ktpNumber: String,
        @SerializedName( "bod")
        var birthOfDate : String?,
        @SerializedName("marital_status")
        var maritalStatus : String?,
        @SerializedName( "phone_no")
        var phoneNumber : String?,
        @SerializedName("title_id")
        var titleId: Int?,
        @SerializedName("bop_date")
        var bopDate : String?,
        @SerializedName("email")
        var email: String?,
        @SerializedName("marital_id")
        var maritalId : Int?,
        @SerializedName("title_name")
        var tittleName: String?,
        @SerializedName("gender_id")
        var genderId : Int?,
        @SerializedName("gender_name")
        var genderName: String?
    )
}