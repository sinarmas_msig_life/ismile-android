package co.id.ajsmsig.ismile.model

data class ConvertStringToDate (val year: Int, val month: Int, val day: Int)