package co.id.ajsmsig.ismile.model.api

import com.google.gson.annotations.SerializedName

data class AgentLeaderApproveRequest(
    @SerializedName("nik_leader")
    val nikLeader: String,
    @SerializedName("reasaon_rejection")
    val reasaonRejection: String?,
    @SerializedName("registration_id")
    val registrationId: String,
    @SerializedName("status_approve")
    val statusApprove: Boolean
)