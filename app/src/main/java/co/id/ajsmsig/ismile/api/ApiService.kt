package co.id.ajsmsig.ismile.api

import co.id.ajsmsig.ismile.model.AppVersionRequest
import co.id.ajsmsig.ismile.model.AppVersionResponse
import co.id.ajsmsig.ismile.model.api.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.http.*

interface ApiService {

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @POST("agentlogin")
    fun login(@Body request : AgentLoginRequest) : Observable<AgentLoginResponse>

    @Headers("CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000")
    @POST("uploadfilejpg")
    fun uploadFile(@Body requestBody: MultipartBody): Observable<UploadFileResponse>

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @GET("agenttitle")
    fun getAgentTitle(@Query("msag_id") agentCode : String) : Observable<GetAgentTitleResponse>

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @POST("agentforgotpassword")
    fun agentForgotPassword(@Body request:AgentForgotPasswordRequest):Observable<AgentForgotPasswordResponse>

    @Headers("CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000")
    @POST("submitcandidateagent")
    fun submitAgentCandidate(@Body body: SubmitAgentCandidateRequest): Observable<SubmitAgentCandidateResponse>

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @POST("statusrecruit")
    suspend fun fetchStatusRecruit(@Body request: StatusRecruitRequest): StatusRecruitResponse

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @POST("inbox")
    fun getInbox(@Body request: InboxRequest):Observable<InboxResponse>

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @GET("candidateagentdata")
    fun getPreviewCandidateAgent(@Query("registration_id")registrationId:String):Observable<PreviewCandidateAgentResponse>

    //Agent Candidate
    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @POST("agentcandidatelogin")
    fun agentCandidatelogin(@Body request : AgentCandidateLoginRequest) : Observable<AgentCandidateLoginResponse>

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @POST("agentcandidateforgotpassword")
    fun agentCandidateForgotPassword(@Body request:AgentCandidateForgotPasswordRequest):Observable<AgentCandidateForgotPasswordResponse>

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @POST("savetoken")
    fun saveTokenNotificication(@Body request: SaveTokenNotificationRequest):Observable<SaveTokenNotificationResponse>

    @Headers("CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000")
    @POST("getagencycontract")
    fun downloadAgencyContract(@Body request: DownloadAgencyContractRequest) : Observable<ResponseBody>

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @GET ("getallkota")
    suspend fun getAllCity() : List<AgentCandidateCityResponse>

    @Headers("CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000")
    @POST("submitdata")
    fun submitDetailedAgentCandidateData(@Body request: SubmitDetailedAgentCandidateRequest) : Observable<SubmitDetailedAgentCandidateResponse>

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @POST("leaderapproval")
    fun fetchApprovalAgentCandidate (@Body request: AgentLeaderStatusApproveRequest) : Observable<AgentLeaderStatusApproveResponse>

    @Headers("CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000")
    @POST("submitapproval")
    fun agentLeaderSubmitApproval (@Body request: AgentLeaderApproveRequest) : Observable <AgentLeaderApproveResponse>

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @GET("getkotalicense")
    fun getCityLicense (): Observable <List<LicenseCitiesResponse>>

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @GET("getstatusapprove")
    suspend fun getStatusApprove (@Query ("registration_id")registrationId:String): StatusAgentCandidateApproveResponse

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @GET("getdatalicensebycity")
    fun retrieveDetailLicense (@Query ("city")city: String) : Observable <List<DetailLicenseResponse>>

    @Headers("CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000")
    @POST("submitexamdata")
    fun submittedExamData (@Body request: SubmitExamDataRequest) : Observable <SubmitExamDataResponse>

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @POST("currentstatus")
    suspend fun currentStatusSubmitDate (@Body request: CurrentStatusRequest) : CurrentStatusResponse

    @Headers("CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000")
    @GET("downloadcontract")
    fun downloadAgencyLetter(@Query("registration_id")registrationId: String): Observable <ResponseBody>

    @Headers("CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000")
    @POST("versioncodeapp")
    suspend fun getVersionAppUpdate (@Body request: AppVersionRequest) : AppVersionResponse

}