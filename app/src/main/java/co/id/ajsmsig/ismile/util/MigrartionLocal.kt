package co.id.ajsmsig.ismile.util

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

val MIGRATION_1_2: Migration = object : Migration (1,2){
    override fun migrate(database: SupportSQLiteDatabase) {

        database.execSQL("ALTER TABLE questionnaires ADD COLUMN counter LONG PRIMARY KEY")
        database.execSQL("ALTER TABLE questionnaires_detail ADD COLUMN counter_id LONG")
        database.execSQL("ALTER TABLE questionnaires_detail ADD COLUMN answer_id LONG")
    }

}