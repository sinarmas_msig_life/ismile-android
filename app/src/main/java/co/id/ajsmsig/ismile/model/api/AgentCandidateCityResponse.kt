package co.id.ajsmsig.ismile.model.api


data class AgentCandidateCityResponse(val KOTA_ID : Int, val KOTA_KABUPATEN : String)