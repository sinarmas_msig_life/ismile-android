package co.id.ajsmsig.ismile.ui.candidate.logincandidateagent

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.db.dao.AgentCandidateDao
import co.id.ajsmsig.ismile.db.dao.QuestionnaireDao
import co.id.ajsmsig.ismile.db.dao.QuestionnaireDetailDao
import co.id.ajsmsig.ismile.db.entity.AgentCandidate
import co.id.ajsmsig.ismile.db.entity.Questionnaire
import co.id.ajsmsig.ismile.db.entity.QuestionnaireDetail
import co.id.ajsmsig.ismile.di.NetManager
import co.id.ajsmsig.ismile.model.api.AgentCandidateLoginRequest
import co.id.ajsmsig.ismile.model.api.AgentCandidateLoginResponse
import co.id.ajsmsig.ismile.model.api.SaveTokenNotificationRequest
import co.id.ajsmsig.ismile.model.api.SaveTokenNotificationResponse
import co.id.ajsmsig.ismile.util.*
import io.reactivex.Observable
import javax.inject.Inject

class LoginCandidateAgentRepository @Inject constructor(
    private val editor: SharedPreferences.Editor,
    private val preferences: SharedPreferences,
    private val dao: AgentCandidateDao,
    private val questionnaireDao: QuestionnaireDao,
    private val questionnaireDetailDao: QuestionnaireDetailDao,
    private val apiService: ApiService,
    private val netManager: NetManager
) {
    fun login(username: String, password: String): Observable<AgentCandidateLoginResponse> {
        if (netManager.isConnectedToInternet) {
            return apiService.agentCandidatelogin(AgentCandidateLoginRequest(username, password))
        }

        return Observable.just(
            AgentCandidateLoginResponse(
                AgentCandidateLoginResponse.Data(
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    -1,
                    "",
                    "",
                    -1,
                    "",
                    -1,
                    ""
                ), true, "Tidak ada koneksi internet. Harap periksa kembali koneksi internet"
            )
        )
    }

    fun saveUserSession(agentCode: String, agentCandidateCode: String, agentCandidateName: String, agentRoleType: Int) {
        editor.putBoolean(PREF_KEY_LOGIN, true)
        editor.putInt(PREF_KEY_LOGIN_ROLE_TYPE, agentRoleType)
        editor.putString(PREF_KEY_AGENT_NAME, agentCandidateName)
        editor.putString(PREF_KEY_AGENT_CODE, agentCode)
        editor.putString(PREFF_KEY_AGENT_CANDIDATE_CODE, agentCandidateCode)
        editor.putString(PREF_KEY_AGENT_USER_ID, agentCandidateCode) //Halaman inbox mengakses PREF_KEY_AGENT_USER_ID ini sebagai parameter POST ke api. Jika calon agen, maka PREF_KEY_AGENT_USER_ID bernilai registrasi id
        editor.apply()
    }

    fun insertAgentCandidateInitialData(data: AgentCandidate) = dao.insert(data)
    fun insertQuestionnaires(questionnaires: List<Questionnaire>) =
        questionnaireDao.insert(*questionnaires.toTypedArray())

    fun insertQuestionnaireDetail(questionnairesDetail: List<QuestionnaireDetail>) =
        questionnaireDetailDao.insert(*questionnairesDetail.toTypedArray())

    fun saveTokenNotification(token:String, registrationId: String) : Observable<SaveTokenNotificationResponse>{
        val jenis_id= LAUNCH_APP_ID

        if (netManager.isConnectedToInternet){
            return apiService.saveTokenNotificication(SaveTokenNotificationRequest(jenis_id,token, registrationId))
        }
        return Observable.just(SaveTokenNotificationResponse(true,"Mohon periksa kembali koneksi internet"))
    }

    suspend fun getQuestionereId( agentCandidateCode: String?, counterId: Long) = questionnaireDao.getQuestionereId(agentCandidateCode!!, counterId)
}