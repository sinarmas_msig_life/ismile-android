package co.id.ajsmsig.ismile.ui.candidate.logincandidateagent

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.db.entity.AgentCandidate
import co.id.ajsmsig.ismile.db.entity.Questionnaire
import co.id.ajsmsig.ismile.db.entity.QuestionnaireDetail
import co.id.ajsmsig.ismile.db.entity.embedded.*
import co.id.ajsmsig.ismile.model.api.AgentCandidateLoginResponse
import co.id.ajsmsig.ismile.model.api.SaveTokenNotificationResponse
import co.id.ajsmsig.ismile.model.uimodel.LoginAgentCandidateUiModel
import co.id.ajsmsig.ismile.util.LOGIN_TYPE_AGENT_CANDIDATE
import co.id.ajsmsig.ismile.util.enum.QuestionnaireAnswer
import co.id.ajsmsig.ismile.util.enum.QuestionnaireType
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject
import kotlinx.coroutines.runBlocking

class LoginCandidateAgentViewModel @Inject constructor(private val repository: LoginCandidateAgentRepository) : BaseViewModel() {

    var registerId = ObservableField<String>()
    var errorRegisterId = ObservableField<String>()
    var password = ObservableField<String>()
    var errorPassword = ObservableField<String>()

    private val _loginSuccessful = MutableLiveData<LoginAgentCandidateUiModel>()
    val loginSuccessful: LiveData<LoginAgentCandidateUiModel>
        get() = _loginSuccessful

    private fun login() {
        val registerId = registerId.get().toString().toUpperCase()
        val password = password.get()
        mCompositeDisposable += repository.login(registerId, password!!)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { setIsLoading(true) }
                .doOnTerminate { setIsLoading(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response -> onLoginSuccessful(response) },
                        { throwable -> onLoginFailed(throwable) }
                )
    }

    private fun insertAgentCandidateInitialDataToDb(name: String, identityNumber: String?, birthOfDate : String?,maritalId:Int?, maritialStatus : String?, phoneNumber:String?, bopDate : String?, email :String?,registrationId: String,titleId : Int?, titleName: String?, genderId: Int) {
        val personalData = PersonalData(name ,identityNumber , birthOfDate, maritalId, maritialStatus, phoneNumber,titleId,titleName, bopDate, email, null, genderId, null, null, null,null, -1, null, null, null, null, null, null, null, null, null,0,null)
        val statement = Statement(
            ethicCodeChecked = false,
            employmentContractChecked = false,
            agencyLetterChecked = false
        )
        val signature = Signature(false, null)
        val completionStatus = CompletionStatus(null, null, null, null, null, null, null, null, null,null,false,false, false, false,false)
        val exam = Exam(-1,"", -1,"", "", -1, "", -1, "",-1,"",null,"","","")

        val data = AgentCandidate(
                registrationId,
                personalData,
                statement,
                signature,
                exam,
                completionStatus,
                false
        )

        mCompositeDisposable += repository.insertAgentCandidateInitialData(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { savedRegistrationId -> onInsertInitialAgentDataSuccessful(savedRegistrationId.toString()) },
                        { throwable -> onInsertInitialAgentDataFailed(throwable) }
                )
    }

    /**
     * Create list of question for questionnaire and insert it to db
     */
    private fun insertQuestionnaires(registrationId: String) {
        val  register= registerId.get()
        val questionnaires = generateQuestionnaires(register!!)
        mCompositeDisposable += repository.insertQuestionnaires(questionnaires)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { onInsertQuestionnaireComplete() },
                        { throwable -> onInsertQuestionnaireFailed(throwable) }
                )
    }

    private fun insertQuestionnairesDetailForSecondQuestion() {
        val questionnairesDetail = generateQuestionnaireDetailForSecondQuestion()
        mCompositeDisposable += repository.insertQuestionnaireDetail(questionnairesDetail)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { onInsertQuestionnaireDetailComplete() },
                        { throwable -> onInsertQuestionnaireDetailFailed(throwable) }
                )
    }

    fun saveTokenNotification (token:String){
        val registrationId = registerId.get()!!
        mCompositeDisposable+= repository.saveTokenNotification(token, registrationId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {succes->saveTokenSucces(succes)},
                {error->saveTokenFailed()}
            )
    }

    fun validateUserLogin() {
        if (!isValidRegisterId()) {
            return
        }
        if (!isValidPassword()) {
            return
        }

        login()
    }

    private fun saveTokenSucces(cek: SaveTokenNotificationResponse) {
        Timber.e(cek.message)
    }
    private fun saveTokenFailed() {

    }

    private fun onInsertInitialAgentDataSuccessful(registrationId: String) {
        Timber.v("Successfully insert agent data")
        insertQuestionnaires(registrationId)
    }

    private fun onInsertInitialAgentDataFailed(throwable: Throwable) {
        Timber.v("Unable to insert agent data ${throwable.message}")
    }


    private fun onInsertQuestionnaireComplete() {
        Timber.v("Successfully insert questionnaire")
        insertQuestionnairesDetailForSecondQuestion()
    }

    private fun onInsertQuestionnaireFailed(throwable: Throwable) {
        Timber.v("Unable to insert questionnaire ${throwable.message}")
    }

    private fun onInsertQuestionnaireDetailComplete() {
        Timber.v("Successfully insert questionnaire detail")
    }

    private fun onInsertQuestionnaireDetailFailed(throwable: Throwable) {
        Timber.v("Unable to insert questionnaire detail ${throwable.message}")
    }

    private fun onLoginSuccessful(response: AgentCandidateLoginResponse) {
        if (response.error) {
            _loginSuccessful.value = LoginAgentCandidateUiModel(true, response.message, "")

        } else {
            repository.saveUserSession(response.data.agentCode, response.data.registrationId, response.data.name, LOGIN_TYPE_AGENT_CANDIDATE)
            _loginSuccessful.value = LoginAgentCandidateUiModel(false, response.message, response.data.registrationId)
            insertAgentCandidateInitialDataToDb(response.data.name, response.data.ktpNumber, response.data.birthOfDate, response.data.maritalId,response.data.maritalStatus,response.data.phoneNumber, response.data.bopDate, response.data.email,response.data.registrationId, response.data.titleId, response.data.tittleName, response.data.genderId!!)
        }
    }

    private fun onLoginFailed(throwable: Throwable) {
        _loginSuccessful.value = LoginAgentCandidateUiModel(true, "Mohon maaf terjadi kesalahan. Harap mencoba kembali", "")
    }

    private fun setErrorUsername(message: String?) = errorRegisterId.set(message)
    private fun setErrorPassword(message: String?) = errorPassword.set(message)
    private fun isValidRegisterId(): Boolean {
        if (registerId.get().isNullOrEmpty()) {
            setErrorUsername("Field ini tidak boleh kosong")
            return false
        } else {
            setErrorUsername(null)
        }

        return true
    }

    private fun isValidPassword(): Boolean {
        if (password.get().isNullOrEmpty()) {
            setErrorPassword("Field ini tidak boleh kosong")
            return false
        }
        if (password.get()!!.length<8){
            setErrorPassword("Password minimal memiliki 8 karakter")
            return false
        } else{
            setErrorPassword(null)
        }
        return true
    }

    private fun generateQuestionnaires(registrationId: String): ArrayList<Questionnaire> {
        val questionnaires = ArrayList<Questionnaire>()
        questionnaires.add(
                Questionnaire(
                        null,
                        registrationId,
                        "Apakah anda pernah bergabung menjadi tenaga pemasaran PT. Asuransi Jiwa Sinarmas MSIG?",
                        QuestionnaireAnswer.NOT_SELECTED.value, //answer
                        QuestionnaireAnswer.NOT_SELECTED.value, //default answer
                        QuestionnaireAnswer.NO.value, //correct answer
                        QuestionnaireType.SINGLE_CHOICE.value,
                    1
                )
        )

        questionnaires.add(
                Questionnaire(
                        null,
                        registrationId,
                        "Apakah dalam 6 bulan terakhir anda pernah bergabung dengan perusahaan asuransi jiwa lain?",
                        QuestionnaireAnswer.NOT_SELECTED.value, //answer
                        QuestionnaireAnswer.NOT_SELECTED.value, //default answer
                        QuestionnaireAnswer.NO.value, //correct answer
                        QuestionnaireType.SINGLE_CHOICE_WITH_SIMPLE_ESSAY.value,2
                )
        )

        questionnaires.add(
                Questionnaire(
                        null,
                        registrationId,
                        "Apakah anda pernah terlibat perkara kriminal?",
                        QuestionnaireAnswer.NOT_SELECTED.value, //answer
                        QuestionnaireAnswer.NOT_SELECTED.value, //default answer
                        QuestionnaireAnswer.NO.value, //correct answer
                        QuestionnaireType.SINGLE_CHOICE.value,
                    3
                )
        )

        return questionnaires
    }


    private fun generateQuestionnaireDetailForSecondQuestion(): List<QuestionnaireDetail> {
        val questionnaireDetails = ArrayList<QuestionnaireDetail>()
        val questionerId = getQuetionareId(2)

        questionnaireDetails.add(QuestionnaireDetail(null, questionerId, "Nama perusahaan", null, false, 2,1))
        questionnaireDetails.add(QuestionnaireDetail(null, questionerId, "Lama bekerja (tahun)", null, false, 2,2))
        questionnaireDetails.add(QuestionnaireDetail(null, questionerId, "Lama bekerja (bulan)", null, false, 2,3))

        return questionnaireDetails
    }

    //Digunakan menambahkan questioner id di questioner detail. Hal ini dikarenkan questioner id itu auto increment.
    private fun getQuetionareId(counter: Long) : Long {
        val registerId = registerId.get()
        var questionerId :Long
        runBlocking { questionerId = repository.getQuestionereId(registerId, counter) }
        return questionerId
    }
}