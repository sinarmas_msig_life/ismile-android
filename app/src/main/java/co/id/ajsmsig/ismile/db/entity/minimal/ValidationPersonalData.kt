package co.id.ajsmsig.ismile.db.entity.minimal

import androidx.room.ColumnInfo

data class ValidationPersonalData(
    @ColumnInfo(name = "email")
    var email: String?,
    @ColumnInfo(name = "personal_data_date")
    var personalDataDate: String?
    )