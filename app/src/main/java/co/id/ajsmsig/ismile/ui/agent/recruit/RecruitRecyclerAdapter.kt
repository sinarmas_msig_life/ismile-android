package co.id.ajsmsig.ismile.ui.agent.recruit

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.id.ajsmsig.ismile.databinding.RvItemRecruitmentStatusBinding
import co.id.ajsmsig.ismile.model.StatusRecruit

class RecruitRecyclerAdapter(private var recruitmentStatusList: List<StatusRecruit>, private val listener: OnRecruitmentStatusPressedListener) : RecyclerView.Adapter<RecruitRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RvItemRecruitmentStatusBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = recruitmentStatusList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(recruitmentStatusList[position], listener, holder.adapterPosition)

    class ViewHolder(private val binding: RvItemRecruitmentStatusBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(model: StatusRecruit, listener: OnRecruitmentStatusPressedListener?, position: Int) {
            binding.model = model
            binding.root.setOnClickListener {
                listener?.onRecruitPressed(model, position)
            }

            binding.executePendingBindings()
        }


    }

    interface OnRecruitmentStatusPressedListener {
        fun onRecruitPressed(candidateAgent: StatusRecruit, position : Int)
    }

    fun refreshData(recruitmentStatusList: List<StatusRecruit>) {
        this.recruitmentStatusList = recruitmentStatusList
        notifyDataSetChanged()
    }

}