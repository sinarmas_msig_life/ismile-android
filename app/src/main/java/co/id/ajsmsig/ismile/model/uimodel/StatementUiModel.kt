package co.id.ajsmsig.ismile.model.uimodel

data class StatementUiModel(val error : Boolean, val message : String?)