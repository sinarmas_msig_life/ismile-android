package co.id.ajsmsig.ismile.ui.agent.addagentcandidate

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.fragment.app.FragmentActivity
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.databinding.CustomSpinnerAgentMaritalStatusBinding
import co.id.ajsmsig.ismile.model.AgentMaritalStatus

class AgentMaritalStatusSpinnerAdapter(val context: FragmentActivity, private var maritalStatuses: List<AgentMaritalStatus>) : BaseAdapter() {

    val inflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemRowHolder

        if (convertView == null) {
            val binding = CustomSpinnerAgentMaritalStatusBinding.inflate(inflater, parent, false)
            vh = ItemRowHolder(binding)
            view = binding.root
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemRowHolder
        }


        vh.bind(R.drawable.ic_action_employee, maritalStatuses[position].maritalStatus)

        return view

    }

    private class ItemRowHolder(private val binding : CustomSpinnerAgentMaritalStatusBinding) {

        fun bind(imageResourceId : Int, label : String) {
            binding.ivAgentMaritalStatus.setImageResource(imageResourceId)
            binding.tvAgentMaritalStatus.text = label
            binding.executePendingBindings()
        }
    }


    override fun getItem(p0: Int): Any? = null
    override fun getItemId(p0: Int) : Long = 0
    override fun getCount(): Int  = maritalStatuses.size

    fun refreshData(maritalStatuses : List<AgentMaritalStatus>) {
        this.maritalStatuses = maritalStatuses
        notifyDataSetChanged()
    }

}