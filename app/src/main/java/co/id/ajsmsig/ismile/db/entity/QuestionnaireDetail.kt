package co.id.ajsmsig.ismile.db.entity

import androidx.room.*
import co.id.ajsmsig.ismile.util.Converters

@Entity(
        tableName = "questionnaires_detail",
        foreignKeys = [
            ForeignKey(
                    entity = Questionnaire::class,
                    parentColumns = arrayOf("id"), //column name in parent table
                    childColumns = arrayOf("questionnaire_id"), //column name in child table
                    onDelete = ForeignKey.CASCADE,
                    onUpdate = ForeignKey.CASCADE
            )
        ]

)
data class QuestionnaireDetail(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        var id: Long? = 0,

        @ColumnInfo(name = "questionnaire_id")
        var questionnaireId: Long?,

        @ColumnInfo(name = "label")
        var label: String?,

        @ColumnInfo(name = "answer")
        var answer: String?,

        @TypeConverters(Converters::class)
        @ColumnInfo(name = "is_active")
        var isActive: Boolean?,

        @ColumnInfo(name = "counter_id")
        var counterId: Long?,

        @ColumnInfo(name = "answer_id")
        var answerId: Long
)