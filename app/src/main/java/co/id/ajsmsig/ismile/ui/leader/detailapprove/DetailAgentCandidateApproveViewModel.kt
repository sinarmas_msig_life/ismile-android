package co.id.ajsmsig.ismile.ui.leader.detailapprove

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.model.DetailAgentCandidateApproval
import co.id.ajsmsig.ismile.model.MessageInfo
import co.id.ajsmsig.ismile.model.api.AgentLeaderApproveResponse
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DetailAgentCandidateApproveViewModel @Inject constructor(private val repository: DetailAgentCandidateApproveRepository): BaseViewModel() {

    val detailApprove = ObservableField <DetailAgentCandidateApproval>()

    val isSuccess = ObservableBoolean ()

    private val _isSubmitSucces = MutableLiveData <MessageInfo>()
    val isSubmitSucces : LiveData <MessageInfo>
        get() = _isSubmitSucces

    fun agentCandidateAccept (registratioId: String){
        isLoading.set(true)
        mCompositeDisposable += repository.getDetailAgentCandidate(registratioId)
            .map { DetailAgentCandidateApproval (it.data.fullName, it.data.recruiterName, it.data.titleName, it.data.companyName, it.data.lastPosition, it.data.timeWork, it.error, it.message) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {data->getApprovalSucces(data)},
                {error->getApprovalFailed(error)})
    }

    fun submitLeaderAccpetAgentCandidate (registratioId: String, reasonRejection: String, statusSubmit:Boolean){
        isLoading.set(true)
        mCompositeDisposable += repository.submitApprovalLeader(registratioId, reasonRejection, statusSubmit)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {data-> submitApprovalSucces(data)},
                {error-> submitApprovalFailed(error)})
    }

    private fun submitApprovalSucces(data: AgentLeaderApproveResponse) {
        isLoading.set(false)
        _isSubmitSucces.value = MessageInfo (data.error, data.message)
    }

    private fun submitApprovalFailed(error: Throwable) {
        isLoading.set(false)
        _isSubmitSucces.value = MessageInfo (true, "Terjadi kesalahan. Mohon coba kembali")
    }

    private fun getApprovalSucces(data: DetailAgentCandidateApproval) {
        isLoading.set(false)
        detailApprove.set(data)
    }

    private fun getApprovalFailed(error: Throwable) {
        isLoading.set(false)
    }
}