package co.id.ajsmsig.ismile.ui.leader.agentleadersucces


import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentAgentLeaderCheckedSuccesBinding
import co.id.ajsmsig.ismile.model.StatusApprove
import co.id.ajsmsig.ismile.ui.leader.agentleader.AgentLeaderFragmentDirections
import co.id.ajsmsig.ismile.ui.leader.agentleader.AgentLeaderViewModel
import co.id.ajsmsig.ismile.util.MENU_LEADER_APPROVE

class AgentLeaderCheckedSuccesFragment : BaseFragment <FragmentAgentLeaderCheckedSuccesBinding, AgentLeaderViewModel>(),
    AgentLeaderCheckedSuccesAdapter.onAgentCandidateSuccesListener{

    override fun getLayoutResourceId() = R.layout.fragment_agent_leader_checked_succes

    override fun getViewModelClass() = AgentLeaderViewModel::class.java

    private val adapter = AgentLeaderCheckedSuccesAdapter(listOf(), this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()

        getViewModel().getAllApprovalAgentCandidate(1)

        getViewModel().isStatusApprove.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (!it.error){
                    getDataBinding().swpRrefreshSucces.isRefreshing = false
                    refreshData(it.status)

                }else {
                    refreshData(emptyList())
                    getDataBinding().swpRrefreshSucces.isRefreshing = false
                }
            }
        })

        getDataBinding().swpRrefreshSucces.setOnRefreshListener {
            getViewModel().getAllApprovalAgentCandidate(1)
        }
    }


    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(activity)
        val dividerItemDecoration =
            DividerItemDecoration(getDataBinding().recyclerView.context, layoutManager.orientation)
        getDataBinding().recyclerView.layoutManager = layoutManager
        getDataBinding().recyclerView.adapter = adapter
        getDataBinding().recyclerView.addItemDecoration(dividerItemDecoration)

    }

    override fun onRecruitPressed(candidateAgent: StatusApprove, position: Int) {
        val action = AgentLeaderFragmentDirections.actionLaunchDetailAgentLeaderApproveFragment(candidateAgent.registrationId, MENU_LEADER_APPROVE)
        findNavController().navigate(action)
    }

    private fun refreshData(recruits: List<StatusApprove>) {
        getDataBinding().swpRrefreshSucces.isRefreshing = false
        adapter.refreshData(recruits)
    }

}
