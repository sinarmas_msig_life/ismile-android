package co.id.ajsmsig.ismile.ui.agent.notification

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.di.NetManager
import co.id.ajsmsig.ismile.model.api.InboxRequest
import co.id.ajsmsig.ismile.model.api.InboxResponse
import co.id.ajsmsig.ismile.util.PREF_KEY_AGENT_USER_ID
import io.reactivex.Observable
import javax.inject.Inject

class NotificationRepository @Inject constructor(
    private val preferences: SharedPreferences,
    private val netManager: NetManager,
    private val apiService: ApiService
) {

    fun getNotification(userId : String): Observable<InboxResponse> {

        if (netManager.isConnectedToInternet) {
            return apiService.getInbox(InboxRequest(userId))
        }

        return Observable.just(InboxResponse(emptyList(), true, "Mohon periksa kembali koneksi internet anda"))
    }

    fun getLoggedInUserId() = preferences.getString(PREF_KEY_AGENT_USER_ID, null)
}