package co.id.ajsmsig.ismile.ui.candidate.licency

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.id.ajsmsig.ismile.databinding.RvItemLicencyBinding
import co.id.ajsmsig.ismile.model.DetailLicense

class LicenseAdapter (private var examSubmitList: List<DetailLicense>, private val listener: LicenseAdapter.onExamSubmitPressedListener) : RecyclerView.Adapter<LicenseAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RvItemLicencyBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() =  examSubmitList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind( examSubmitList[position], listener, holder.adapterPosition)

    class ViewHolder(private val binding: RvItemLicencyBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(model: DetailLicense, listener: onExamSubmitPressedListener?, position: Int) {
            binding.model = model
            binding.fabChecklist.setOnClickListener {
                listener?.onSubmitExamPressed(model, position)
            }

            binding.executePendingBindings()
        }

    }

    interface onExamSubmitPressedListener {
        fun onSubmitExamPressed(submitSelected: DetailLicense, position : Int)
    }

    fun refreshData(examSubmitList: List<DetailLicense>) {
        this.examSubmitList = examSubmitList
        notifyDataSetChanged()
    }

}