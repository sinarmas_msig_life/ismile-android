package co.id.ajsmsig.ismile.ui.agent.home

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import co.id.ajsmsig.ismile.ui.agent.login.LoginRepository
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val repository: LoginRepository) : ViewModel() {

    val agentCode = ObservableField<String>()
    val agentName = ObservableField<String>()
    val agentTitle = ObservableField<String>()

    fun displaySavedUserSession() {
        agentCode.set(getSavedAgentCode())
        agentName.set(getSavedAgentName())
        agentTitle.set(getSavedAgentTitle())
    }

    private fun getSavedAgentCode() = repository.getSavedAgentCode()

    private fun getSavedAgentName() = repository.getSavedAgentName()

    private fun getSavedAgentTitle() = repository.getSavedAgentTitle()

    fun clearAllSavedUserData() {
        repository.clearSharedPreference()
    }

}