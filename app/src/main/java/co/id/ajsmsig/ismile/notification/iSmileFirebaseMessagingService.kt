package co.id.ajsmsig.ismile.notification

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber

class iSmileFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {

            val title = remoteMessage.notification!!.title
            val body = remoteMessage.notification!!.body

            Timber.v("[Notification] Message notification => Title $title Body $body")

            NotificationHelper.displayNotification(this, title!!, body!!)
        }

        // Check if message contains a data payload.
        if (remoteMessage.data != null && remoteMessage.data.isNotEmpty()) {
            Timber.v("[Notification] Message data payload : ${remoteMessage.data}")
        }
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Timber.v("[Notification] Refreshed token : $token")
    }
}
