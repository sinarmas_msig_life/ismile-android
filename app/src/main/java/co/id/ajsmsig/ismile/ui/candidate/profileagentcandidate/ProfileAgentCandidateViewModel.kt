package co.id.ajsmsig.ismile.ui.candidate.profileagentcandidate

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.R.string.minimal_npwp_number
import co.id.ajsmsig.ismile.SingleLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.common.ResultState
import co.id.ajsmsig.ismile.model.GetSpinnerId
import co.id.ajsmsig.ismile.model.PersoanalDataAgentCandidate
import co.id.ajsmsig.ismile.model.ProfileCandidateView
import co.id.ajsmsig.ismile.model.ProfileImagePath
import co.id.ajsmsig.ismile.model.api.AgentCandidateCityResponse
import co.id.ajsmsig.ismile.model.api.PreviewCandidateAgentResponse
import co.id.ajsmsig.ismile.util.DateUtils
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.IOException
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class ProfileAgentCandidateViewModel @Inject constructor(private val repository: ProfileAgentCandidateRepositroy) : BaseViewModel() {

    var emailAddres = ObservableField<String>(" ")
    var errorEmailAddres = ObservableInt()

    var homeAddress = ObservableField<String>()
    var errorHomeAddress = ObservableInt()

    var placeOfBirth = ObservableField<String>()
    var errorPlaceOfBirth = ObservableInt()

    var npwpNumber = ObservableField<String>()
    var errorNpwpNumber = ObservableInt()

    var housePhone = ObservableField<String>(" ")
    var errorHousePhone = ObservableInt()

    var errorSelectedCity = ObservableInt()
    private var selectedCityId = ObservableField(-1)
    var selectedCityName = ObservableField<String>()

    var religionId = ObservableField(-1)

    val candidateAgentModel = ObservableField<ProfileCandidateView>()

    private val isConeccted = ObservableBoolean (false)

    private var _isCheckedDataInLocalDb = MutableLiveData<GetSpinnerId>()
    val isCheckedDataInLocalDb: LiveData<GetSpinnerId>
        get() = _isCheckedDataInLocalDb

    private val cityAllData = ObservableField<List<AgentCandidateCityResponse>>(emptyList())
    private val _isGetCity = SingleLiveData<ResultState<List<AgentCandidateCityResponse>>>()
    val isGetCity: LiveData<ResultState<List<AgentCandidateCityResponse>>>
        get() = _isGetCity

    private val _isIdentityCardImage = MutableLiveData<String>()
    val isIdentityCardImage: LiveData<String>
        get() = _isIdentityCardImage

    private val _isSuccesSave = SingleLiveData<Boolean>()
    val isSuccesSave: SingleLiveData<Boolean>
        get() = _isSuccesSave

    private val _profileImagePath = MutableLiveData<ProfileImagePath>()
    val profileImagePath: LiveData<ProfileImagePath>
        get() = _profileImagePath

    private val _npwpImagePath = MutableLiveData<ProfileImagePath>()
    val npwpImagePath: LiveData<ProfileImagePath>
        get() = _npwpImagePath

    private val _isSavingAccountImagePath = MutableLiveData<ProfileImagePath>()
    val isSavingAccountImagePath: LiveData<ProfileImagePath>
        get() = _isSavingAccountImagePath

    private val _isUserInputValid = MutableLiveData<Int>()
    val isUserInputValid: LiveData<Int>
        get() = _isUserInputValid

    fun previewIdentityImagePath (){
        mCompositeDisposable += repository.getIdentityImagePath()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                    data-> findIdentyImageSucces(data)
            }
    }

    fun previewAgentCandidateProfile () {
        mCompositeDisposable += repository.viewProfilAgentCandidate()
            .map {
                val birthOfDate = DateUtils.getDateFormat(it.birthOfDate!!)
                val bop = DateUtils.getDateFormat(it.bopDate!!)
                val genderName = getGenderName(it.genderId!!)
                ProfileCandidateView (it.fullName, birthOfDate, it.identityNumber, it.phoneNumber, it.email, it.titleName, bop, it.maritalStatus, genderName) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                    data -> previewProfileAgentCandidateSucces(data)
            }
    }

    private fun saveUserInputInDatabase(email: String, homeAddress: String, placeOfBirth: String, selectedCity: Int, selectedCityName: String, npwpNumber: String, housePhone: String, profileImgPath: String, npwpImgPath: String, savingAccountImgPath: String, religionId: Int, millis: String) {
        mCompositeDisposable += Completable.fromCallable {
            repository.updateAgentCandidateData(email, homeAddress, placeOfBirth, selectedCity, selectedCityName, npwpNumber, housePhone, profileImgPath, npwpImgPath, savingAccountImgPath, religionId, millis) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                onSuccesDataUpdate()
            }
    }

    // Menampilkan profile candidate agent di menu lengkapi data diri
    fun getPersonalDataAgentCandidate() {
        mCompositeDisposable += repository.getPersonalData()
            .map { PersoanalDataAgentCandidate(it.email, it.homeAddress, it.placeOfBirth, it.cityId, it.cityName, it.npwpNumber, it.housePhone, it.profilePhotoImgPath, it.npwpPhotoImgPath, it.savingAccountImgPath, it.genderId, it.religionId, it.personalDataDate) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {succes -> getDataSucces(succes)},
                {errror -> Timber.e(errror.toString())}
            )
    }

    //Menampilkan data seluruh kota
    fun getAllCity() {
        viewModelScope.launch (IO) {
            try {
                val result = repository.getAllCity()
                if (result.isEmpty()){
                    setResulCity(ResultState.NoData())
                    return@launch
                }
                isConeccted.set(true)
                cityAllData.set(result)
                setResulCity(ResultState.HasData(result))
            }catch (e: Throwable){
                isConeccted.set(false)
                when (e) {
                    is IOException -> setResulCity(ResultState.NoInternetConnection())
                    is TimeoutException -> setResulCity(ResultState.TimeOut(R.string.timeout))
                    else -> setResulCity(ResultState.Error(R.string.unknown_error))
                }
            }
        }
    }

    private fun setResulCity(data: ResultState<List<AgentCandidateCityResponse>>){
        _isGetCity.sendActionOnBackground(data)
    }

    fun validateUserInput(milis: String) {

        if (!isEmailAddresValid()) {
            return
        }
        if (!isPlaceOfBirthValid()) {
            return
        }
        if (!isHomeAddresValid()) {
            return
        }
        if (!isCitySelected()) {
            return
        }
        if (!isNpwpNumberValid()) {
            return
        }
        if (!isReligionSelected()) {
            return
        }
        if (!isHousePhoneValid()) {
            return
        }
        if (!isProfilImageTaken()) {
            return
        }
        if (!isNpwpImageTaken()) {
            return
        }
        if (!isSavingAccountImageTaken()) {
            return
        }
        saveUserInputInDatabase(emailAddres.get()!!, homeAddress.get()!!, placeOfBirth.get()!!, selectedCityId.get()!!, selectedCityName.get()!!, npwpNumber.get()!!, housePhone.get()!!, _profileImagePath.value!!.imagePath, _npwpImagePath.value!!.imagePath, _isSavingAccountImagePath.value!!.imagePath, religionId.get()!!, milis)
    }

    private fun isSavingAccountImageTaken(): Boolean {
        val imagePath = _isSavingAccountImagePath.value
        if (imagePath == null) {
            setUserInputIsError(R.string.photo_book_failed)
            return false
        }
        return true
    }

    private fun isNpwpImageTaken(): Boolean {
        val imagePath = _npwpImagePath.value
        if (imagePath == null) {
            setUserInputIsError(R.string.photo_npwp_failed)
            return false
        }
        return true
    }

    private fun isProfilImageTaken(): Boolean {
        val imagePath = _profileImagePath.value
        if (imagePath == null) {
            setUserInputIsError(R.string.photo_profil_failed)
            return false
        }
        return true
    }

    private fun isHomeAddresValid(): Boolean {
        if (homeAddress.get().isNullOrEmpty()) {
            setErroHomeAddress(R.string.no_empty_field)
            return false
        } else {
            setErroHomeAddress()
        }
        return true
    }

    private fun isCitySelected(): Boolean {
        if (selectedCityName.get().isNullOrEmpty()) {
            setErrorCity(R.string.no_empty_field)
            return false
        }
        if (!isSelectedNameCitiesValid()){
            return false
        }
        if (selectedCityId.equals(-1)){
            setErrorCity(R.string.warning_input_city)
            return false
        }
        else {
            setErrorCity()
        }
        return true
    }

    private fun isSelectedNameCitiesValid() : Boolean {
        val result = cityAllData.get()

        if (result.isNullOrEmpty()){
            setErrorCity (R.string.no_internet_connection)
            return false
        }

        for ((i, value) in result.withIndex()){
            if (selectedCityName.get()!!.equals(value.KOTA_KABUPATEN)){
                selectedCityId.set(i+1)
                return true
            }
        }
        setErrorCity(R.string.warning_input_city)
        return false
    }

    private fun isReligionSelected(): Boolean {
        if (religionId.get() == -1) {
            setUserInputIsError(R.string.empty_religion)
            return false
        }
        return true
    }

    private fun isHousePhoneValid(): Boolean {
        val phoneNumber = housePhone.get()!!
        if (phoneNumber.length in 2..6) {
            setErrorHousePhone(R.string.minimal_input_phone)
            return false
        }
        return true
    }

    private fun isNpwpNumberValid(): Boolean {
        when {
            npwpNumber.get().isNullOrEmpty() -> {
                setErrorNpwpNumber(R.string.no_empty_field)
                return false
            }
            npwpNumber.get()!!.length<12 -> {
                setErrorNpwpNumber(minimal_npwp_number)
                return false
            }
            else -> {
                setErrorNpwpNumber()
            }
        }
        return true
    }

    private fun isEmailAddresValid(): Boolean {
        val isValidEmail=isValidEmail(emailAddres.get().toString())
        if (!isValidEmail){
            seterrorEmailAddres(R.string.fortmat_email_failed)
            return false
        } else {
            seterrorEmailAddres()
        }
        return true
    }

    private fun isPlaceOfBirthValid(): Boolean {
        if (placeOfBirth.get().isNullOrEmpty()) {
            setErrorPlaceOfBirth(R.string.no_empty_field)
            return false
        } else {
            setErrorPlaceOfBirth()
        }
        return true
    }

    private fun findIdentyImageSucces(data: PreviewCandidateAgentResponse) {
        _isIdentityCardImage.value=data.data.profpicImagePath
    }

    private fun previewProfileAgentCandidateSucces(data : ProfileCandidateView) {
        candidateAgentModel.set(data)
        emailAddres.set(data.email)
        Timber.e("Data berhasil di tampilkan")
    }

    private fun onSuccesDataUpdate() {
        _isSuccesSave.sendAction(true)
    }

    private fun getDataSucces(succes: PersoanalDataAgentCandidate) {
        if (!succes.personalDataDate.isNullOrEmpty()) {
            setViewPersonalDate(succes.email, succes.homeAddress, succes.placeOfBirth, succes.npwpNumber, succes.housePhone, succes.profilePhotoImgPath.toString(), succes.npwpPhotoImgPath.toString(), succes.savingAccountImgPath.toString())
            _isCheckedDataInLocalDb.value = GetSpinnerId(true, succes.genderId, succes.religionId, succes.cityName)
        } else {
            _isCheckedDataInLocalDb.value = GetSpinnerId(false, 0, 0, null)
        }
    }

    private fun setViewPersonalDate(email: String?, homeAddress: String?, placeOfBirth: String?, npwpNumber: String?, housePhone: String?, profilePhotoImgPath: String, npwpPhotoImgPath: String, savingAccountImgPath: String) {
        emailAddres.set(email)
        this.homeAddress.set(homeAddress)
        this.placeOfBirth.set(placeOfBirth)
        this.npwpNumber.set(npwpNumber)
        this.housePhone.set(housePhone)
        _profileImagePath.value = ProfileImagePath(1, profilePhotoImgPath)
        _npwpImagePath.value = ProfileImagePath(2, npwpPhotoImgPath)
        _isSavingAccountImagePath.value = ProfileImagePath(3, savingAccountImgPath)
    }

    //Mengirimkan image path ke view
    fun saveImagePath(imagePath: String, photoId: Int) {
        when (photoId) {
            1 -> _profileImagePath.value = ProfileImagePath(photoId, imagePath)
            2 -> _npwpImagePath.value = ProfileImagePath(photoId, imagePath)
            else -> _isSavingAccountImagePath.value = ProfileImagePath(photoId, imagePath)
        }
    }

    private fun getGenderName (genderId: Int): String{
        when (genderId){
            0 -> return "Perempuan"
            else -> return "Laki-Laki"
        }
    }

    fun saveSelectedCityId(citiesName: List<String>){
        for ((i, value) in citiesName.withIndex()){
            if (value.equals(selectedCityName.get().toString())){
                selectedCityId.set(i+1)
            }
        }
    }

    fun saveSelectedReligionId(id: Int) = religionId.set(id)

    private fun seterrorEmailAddres(errorMessage: Int = 0) = errorEmailAddres.set(errorMessage)

    private fun isValidEmail(email: String) = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()

    private fun setErrorPlaceOfBirth(message: Int = 0 ) = errorPlaceOfBirth.set(message)

    private fun setErrorNpwpNumber(message: Int = 0) = errorNpwpNumber.set(message)

    private fun setErrorHousePhone(message: Int = 0 ) = errorHousePhone.set(message)

    private fun setErroHomeAddress(message: Int = 0) = errorHomeAddress.set(message)

    private fun setErrorCity(message: Int = 0) = errorSelectedCity.set(message)

    private fun setUserInputIsError(message: Int?) {
        _isUserInputValid.value = message
    }
}