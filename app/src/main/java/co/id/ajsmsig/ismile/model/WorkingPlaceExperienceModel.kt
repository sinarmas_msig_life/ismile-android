package co.id.ajsmsig.ismile.model

data class WorkingPlaceExperienceModel (val imgSrc:Int,val workingName:String,val status:String, val startDate:String, val endDate:String)