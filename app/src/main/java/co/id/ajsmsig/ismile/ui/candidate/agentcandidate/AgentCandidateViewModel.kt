package co.id.ajsmsig.ismile.ui.candidate.agentcandidate

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.util.Base64
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.SingleLiveData
import co.id.ajsmsig.ismile.base.BaseResponse
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.common.ResultState
import co.id.ajsmsig.ismile.db.entity.embedded.CompletionStatus
import co.id.ajsmsig.ismile.db.entity.join.AgentCandidateAndAllQuestionnaire
import co.id.ajsmsig.ismile.model.AgentCandidateCompletionStatusModel
import co.id.ajsmsig.ismile.model.DownloadPdfModel
import co.id.ajsmsig.ismile.model.OnProgressModel
import co.id.ajsmsig.ismile.model.api.CurrentStatusResponse
import co.id.ajsmsig.ismile.model.api.StatusAgentCandidateApproveResponse
import co.id.ajsmsig.ismile.model.api.SubmitDetailedAgentCandidateRequest
import co.id.ajsmsig.ismile.model.api.SubmitDetailedAgentCandidateResponse
import co.id.ajsmsig.ismile.model.uimodel.CandidateAgentSubmitUiModel
import co.id.ajsmsig.ismile.model.uimodel.DownloadAgencyContractUiModel
import co.id.ajsmsig.ismile.util.DateUtils
import co.id.ajsmsig.ismile.util.IMAGE_COMPRESSION_PERCENTAGE
import co.id.ajsmsig.ismile.util.YES
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import timber.log.Timber
import java.io.*
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class AgentCandidateViewModel @Inject constructor(private val repository: AgentCandidateRepository) : BaseViewModel() {

    var examCity = ObservableField <String>()
    val examPlace = ObservableField <String>()
    val examProductType = ObservableField <String>()
    val examMethod= ObservableField <String>()
    val examDateLicense = ObservableField <String>()
    val examType = ObservableField <String>()

    val isLeaderApprove = ObservableBoolean (false)
    val isSubmitted= ObservableBoolean (false)

    var statuses = ObservableField<AgentCandidateCompletionStatusModel>()

    val personalDataCompleted = ObservableBoolean()
    val statementCompleted = ObservableBoolean()
    val signatureCompleted = ObservableBoolean()

    private val _isLicenseSubmitAndApproveChecked = SingleLiveData <ResultState<StatusAgentCandidateApproveResponse>>()
    val isLicenseSubmitAndApproveChecked : LiveData <ResultState<StatusAgentCandidateApproveResponse>>
        get() = _isLicenseSubmitAndApproveChecked

    val dataSubmitDate = ObservableField <String>()
    val dissupVerificationDate = ObservableField <String>()

    private val _launchStatementPage = SingleLiveData<Boolean>()
    val launchStatementPage: LiveData<Boolean>
        get() = _launchStatementPage

    private val _launchSignaturePage = SingleLiveData<Boolean>()
    val launchSignaturePage: LiveData<Boolean>
        get() = _launchSignaturePage

    private val _inputAreValid = SingleLiveData<CandidateAgentSubmitUiModel>()
    val inputAreValid: LiveData<CandidateAgentSubmitUiModel>
        get() = _inputAreValid

    private val _displaySubmitDataOnProgress = SingleLiveData<OnProgressModel>()
    val displaySubmitDataOnProgress: LiveData<OnProgressModel>
        get() = _displaySubmitDataOnProgress

    private val _isSubmitDataSuccess = SingleLiveData<ResultState<SubmitDetailedAgentCandidateResponse>>()
    val isSubmitDataSuccess: LiveData<ResultState<SubmitDetailedAgentCandidateResponse>>
        get() = _isSubmitDataSuccess

    private val _isEnabledEdit = MutableLiveData<Boolean>(false)
    val isEnabledEdit: LiveData<Boolean>
        get() = _isEnabledEdit

    private val _messageDownloadAgencyLetter = SingleLiveData <DownloadAgencyContractUiModel>()
    val messageDownloadAgencyLetter : LiveData <DownloadAgencyContractUiModel>
        get() = _messageDownloadAgencyLetter

    private val _currentStatus = SingleLiveData<ResultState<CurrentStatusResponse.Data>>()
    val currentStatus : LiveData <ResultState<CurrentStatusResponse.Data>>
        get() = _currentStatus

    private val _isAutomaticLogout = SingleLiveData<Boolean>()
    val isAutomaticLogout: LiveData<Boolean>
        get() = _isAutomaticLogout

    fun findCurrentCompletionStatus(registrationId: String) {
        mCompositeDisposable += repository.findCompletionStatus(registrationId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { data -> onRetrieveCurrentCompletionStatusSuccessful(data) },
                { throwable -> onRetrieveCurrentCompletionStatusFailed(throwable) }
            )
    }

    fun validateSubmit(registrationId: String) {
        mCompositeDisposable += repository.findCompletionStatus(registrationId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { data -> onRetrieveCompletionStatusSuccessful(data, registrationId) },
                { throwable -> onRetrieveCompletionStatusFailed(throwable) })
    }

    private fun findAllSavedData(registrationId: String) {
        mCompositeDisposable += repository.findAllSavedData(registrationId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                submitDetailedAgentCandidateData(it, registrationId)
            }
    }

    fun checkIfStatementMenuEnabled(registrationId: String) {
        mCompositeDisposable += repository.findStatementCompletionStatus(registrationId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                { _launchStatementPage.sendAction(it) },
                {}
            )
    }

    fun checkIfSignatureMenuEnabled(registrationId: String) {
        mCompositeDisposable += repository.findSignatureCompletionStatus(registrationId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                { _launchSignaturePage.sendAction(it) },
                {}
            )
    }


    fun currentStatusDate () {
        viewModelScope.launch(IO) {
            try {
                val result = repository.getCurrentStatusSubmit()
                if (result.error ){
                    setResultCurrentStatus(ResultState.Error(R.string.unknown_error))
                    return@launch
                }
                setResultCurrentStatus(ResultState.HasData(result.data))
            }catch (e: Throwable){
                when (e) {
                    is IOException -> setResultCurrentStatus(ResultState.NoInternetConnection())
                    is TimeoutException -> setResultCurrentStatus(ResultState.TimeOut(R.string.timeout))
                    else -> setResultCurrentStatus(ResultState.Error(R.string.unknown_error))
                }
            }
        }
    }

    private fun setResultCurrentStatus (data: ResultState<CurrentStatusResponse.Data>){
        _currentStatus.sendActionOnBackground(data)
    }

    fun checkIfLicenseMenuEnabled() {
        viewModelScope.launch (IO){
            try {
                val result  = repository.checkedStatusApprove()
                if (result.error) {
                    stateLinceseStatus(ResultState.Error(R.string.unknown_error))
                    return@launch
                }
                stateLinceseStatus(ResultState.HasData(result))
            }catch (e: Throwable){
                when (e) {
                    is IOException -> stateLinceseStatus(ResultState.NoInternetConnection())
                    is TimeoutException -> stateLinceseStatus(ResultState.TimeOut(R.string.timeout))
                    else -> stateLinceseStatus(ResultState.Error(R.string.unknown_error))
                }
            }
        }
    }

    private fun stateLinceseStatus(data: ResultState<StatusAgentCandidateApproveResponse>) {
        _isLicenseSubmitAndApproveChecked.sendActionOnBackground(data)
    }

    fun checkToEnabledEdit(registrationId: String) {
        mCompositeDisposable += repository.findLicenseCompletionStatus(registrationId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                { _isEnabledEdit.value = it },
                {}
            )
    }

    private fun markAsSubmitted(registrationId: String, isSubmitted: Boolean) {
        mCompositeDisposable += Maybe.fromCallable { repository.markAsSubmitted(registrationId, isSubmitted) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { },
                { }
            )
    }

    fun downloadAgencyLetter (registrationId: String){
        isLoading.set(true)
        mCompositeDisposable += repository.fetchAgencyLetter(registrationId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {data-> getAgencyLetterSucces(data, registrationId)},
                {error-> getAgencyLetterFailed() })
    }

    //Digunakan untuk fungsi logout otomatis
    fun checkForAutomaticLogout() {
        viewModelScope.launch(IO) {
            try {
                val result = repository.checkedForAutoMatedLogout()
                if (result.fullName.isNullOrEmpty()){
                    automaticLogout(true)
                    return@launch
                }
                automaticLogout(false)
            }catch (e: Throwable){
                automaticLogout(true)
            }
        }
    }

    private fun automaticLogout(isLogout: Boolean){
        _isAutomaticLogout.sendActionOnBackground(isLogout)
    }

    private fun onRetrieveCompletionStatusSuccessful(status: CompletionStatus, registrationId: String) {

        if (!status.personalDataComplete) {
            setError(true, "Semua isian 'Data Diri' harus diisi lengkap")
            return
        }

        if (!status.statementComplete) {
            setError(true, "Semua isian 'Pernyataan' harus diisi lengkap")
            return
        }

        if (!status.signatureComplete) {
            setError(true, "'Tanda tangan' harus dilengkapi")
            return
        }

        setDisplayOnProcess(false, "Mengunggah semua data. Mohon tunggu")
        findAllSavedData(registrationId)
    }

    private fun submitDetailedAgentCandidateData(data: AgentCandidateAndAllQuestionnaire, registrationId: String) {

        //Personaldata
        val address = data.agentCandidate?.personalData?.homeAddress
        val birthplace = data.agentCandidate?.personalData?.placeOfBirth
        val bukuTabunganBase64Image =
            removeLineBreakCharachter(convertToBase64(data.agentCandidate?.personalData?.savingAccountImgPath))
        val cityId = data.agentCandidate?.personalData?.cityId.toString()
        val education = SubmitDetailedAgentCandidateRequest.PersonalData.Education(
            data.agentCandidate?.personalData?.schoolName
        )
        val email = data.agentCandidate?.personalData?.email
        val homePhoneNo = data.agentCandidate?.personalData?.housePhone
        val npwp = data.agentCandidate?.personalData?.npwpNumber
        val npwpBase64Image =
            removeLineBreakCharachter(convertToBase64(data.agentCandidate?.personalData?.npwpPhotoImgPath))
        val profpicBase64Image =
            removeLineBreakCharachter(convertToBase64(data.agentCandidate?.personalData?.profilePhotoImgPath))
        val religionId = data.agentCandidate?.personalData?.religionId
        val work = SubmitDetailedAgentCandidateRequest.PersonalData.Work(
            data.agentCandidate?.personalData?.companyName,
            data.agentCandidate?.personalData?.endDateWork,
            data.agentCandidate?.personalData?.lastPosition,
            data.agentCandidate?.personalData?.startDateWork
        )
        val personalData = SubmitDetailedAgentCandidateRequest.PersonalData(
            address,
            birthplace,
            bukuTabunganBase64Image,
            cityId,
            education,
            email,
            homePhoneNo,
            npwp,
            npwpBase64Image,
            profpicBase64Image,
            religionId,
            work
        )

        //Statement
        val savedQuestionnairesData = data.questionnaire

        val questionnaires = mutableListOf<SubmitDetailedAgentCandidateRequest.Statement.Questionnaire>()

        for (i in savedQuestionnairesData) {
            val questionnaireId = i.questionnaire?.counter
            val answer = i.questionnaire?.answer
            val savedOptionalAnswer = i.questionnairesDetail
            val questionnaireType = i.questionnaire?.questionType

            val mappedOptionalAnswer = mutableListOf<SubmitDetailedAgentCandidateRequest.Statement.OptionalAnswer>()

            if (answer == YES) {
                for (data in savedOptionalAnswer) {
                    mappedOptionalAnswer.add(
                        SubmitDetailedAgentCandidateRequest.Statement.OptionalAnswer(
                            data.answerId,
                            data.label,
                            data.answer
                        )
                    )
                }
            }

            questionnaires.add(
                SubmitDetailedAgentCandidateRequest.Statement.Questionnaire(
                    questionnaireId,
                    mappedOptionalAnswer,
                    questionnaireType
                )
            )
        }

        val statement = SubmitDetailedAgentCandidateRequest.Statement(questionnaires)

        //Signature
        val signatureBase64Image = removeLineBreakCharachter(convertToBase64(data.agentCandidate?.signature?.signatureImgPath))
        val signature = SubmitDetailedAgentCandidateRequest.Signature(signatureBase64Image)

        val request = SubmitDetailedAgentCandidateRequest(
            personalData,
            data.agentCandidate?.registrationId.toString(),
            signature,
            statement
        )

        mCompositeDisposable += repository.submitDetailedAgentCandidateData(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : BaseResponse<SubmitDetailedAgentCandidateResponse>(){
                override fun onSuccess(response: SubmitDetailedAgentCandidateResponse) {
                    onSubmitDetailedAgentCandidateDataSuccessful(response, registrationId)
                }

                override fun onNoInternetConnection() {
                    setResultSubmitData(ResultState.NoInternetConnection())
                }

                override fun onTimeout() {
                    setResultSubmitData(ResultState.Error(R.string.timeout))
                }

                override fun onUnknownError(message: String) {
                    setResultSubmitData(ResultState.Error(R.string.unknown_error))
                }
            })
    }

    private fun onSubmitDetailedAgentCandidateDataSuccessful(data: SubmitDetailedAgentCandidateResponse, registrationId: String) {
        if (!data.error){
            setResultSubmitData(ResultState.HasData(data))
            markAsSubmitted(registrationId,true)
        }else{
            setResultSubmitData(ResultState.Error(R.string.unknown_error))
        }
        setDisplayOnProcess(true, null)
    }

    private fun onRetrieveCompletionStatusFailed(throwable: Throwable?) {
        setDisplayOnProcess(true, throwable?.message)
        Timber.v("Failed to get completion status ${throwable?.message}")
    }

    private fun onRetrieveCurrentCompletionStatusSuccessful(status: CompletionStatus) {

        if (status.personalDataComplete) {
            setPersonalDataCompleted(true)
        } else {
            setPersonalDataCompleted(false)
        }

        if (status.statementComplete) {
            setStatementCompleted(true)
        } else {
            setStatementCompleted(false)
        }

        if (status.signatureComplete) {
            setSignatureCompleted(true)
        } else {
            setSignatureCompleted(false)
        }

        loadCompletionStatus(status)
    }

    private fun onRetrieveCurrentCompletionStatusFailed(throwable: Throwable?) {
        Timber.v("Failed to get current completion status ${throwable?.message}")
    }

    private fun setPersonalDataCompleted(isCompleted: Boolean) = personalDataCompleted.set(isCompleted)
    private fun setStatementCompleted(isCompleted: Boolean) = statementCompleted.set(isCompleted)
    private fun setSignatureCompleted(isCompleted: Boolean) = signatureCompleted.set(isCompleted)


    fun getRegistrationId(): String {
        val registrationId = repository.getRegistrationId()

        if (registrationId != null) {
            return registrationId
        }

        return " "
    }

    fun getAgentCandidateName(): String? = repository.getAgentCandidateName()

    private fun loadCompletionStatus(status: CompletionStatus) {
        val model = AgentCandidateCompletionStatusModel(

            isCompleted(status.personalDataComplete),
            isCompleted(status.personalDataDate),
            isCompleted(status.workExperienceDate),
            isCompleted(status.educationDate),

            isCompleted(status.statementComplete),
            isCompleted(status.questionnaireDate),
            isCompleted(status.ethicCodeDate),
            isCompleted(status.employmentContractDate),
            isCompleted(status.agencyLetterDate),

            isCompleted(status.signatureComplete),

            isCompleted(status.examCompleted),
            isCompleted(status.examBsbSubmitDate), //submit bsb
            isCompleted(status.examCompletedDate) //input ujian

        )

        this.statuses.set(model)
    }

    private fun isCompleted(date: String?): String {
        if (date.isNullOrEmpty()) {
            return "Belum"
        }

        return "Sudah"
    }


    private fun isCompleted(isCompleted: Boolean): String {
        if (isCompleted) {
            return "Sudah"
        }

        return "Belum"
    }

    private fun setError(isError: Boolean, message: String) {
        _inputAreValid.sendAction(CandidateAgentSubmitUiModel(isError, message))
    }

    private fun removeLineBreakCharachter(input: String): String {
        return input.replace("\n", "")
    }

    private fun convertToBase64(imagePath: String?): String {
        if (!imagePath.isNullOrEmpty()) {
            val bitmap = BitmapFactory.decodeFile(imagePath)
            val outputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, IMAGE_COMPRESSION_PERCENTAGE, outputStream)
            val byteArray = outputStream.toByteArray()

            return Base64.encodeToString(byteArray, Base64.DEFAULT)
        }

        return ""
    }

    private fun setDisplayOnProcess(isFinished: Boolean, message: String?) {
        _displaySubmitDataOnProgress.value = OnProgressModel(isFinished, message)
    }

    private fun setResultSubmitData(result : ResultState<SubmitDetailedAgentCandidateResponse>) {
        _isSubmitDataSuccess.postValue(result)
    }

    fun settCurrentStatus(data: CurrentStatusResponse.Data) {
        isLeaderApprove.set(data.isApproved)
        isSubmitted.set(data.submitted)
        dataSubmitDate.set(data.dataSubmitDate)
        dissupVerificationDate.set(data.dissupVerificationDate)
    }

    private fun getAgencyLetterSucces(data: ResponseBody, registrationId: String){
        isLoading.set(false)
        val fileName = "SURAT_KEAGENAN_${registrationId}_${DateUtils.getCurrentTimeForFile()}.pdf"
        val response= savePdfInStorage(data, fileName)
        infoDownloadAgencyLetter (response.error, response.message!!, fileName)
    }

    private fun getAgencyLetterFailed() {
        isLoading.set(false)
        infoDownloadAgencyLetter (true, "File gagal di download", " ")
    }

    private fun savePdfInStorage (body: ResponseBody, outputFileName: String) : DownloadPdfModel{
        try {

            val directory= File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + File.separator + "/SMiLeGo" + "/PDF/" )
            if (!directory.exists()) {
                directory.mkdirs()
            }

            val fileName = "$directory/$outputFileName"

            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null

            try {
                val fileReader = ByteArray(4096)

                var fileSizeDownloaded: Long = 0

                inputStream = body.byteStream()
                outputStream = FileOutputStream(fileName)

                while (true) {
                    val read = inputStream!!.read(fileReader)

                    if (read == -1) {
                        break
                    }

                    outputStream.write(fileReader, 0, read)

                    fileSizeDownloaded += read.toLong()

                }

                outputStream.flush()
                return DownloadPdfModel(false, "")
            } catch (e: IOException) {
               return DownloadPdfModel(true, "Terjadi kesalahan dalam membuat file PDF ${e.message}")
            } finally {
                inputStream?.close()
                outputStream?.close()
            }
        }catch (e: IOException) {
            return DownloadPdfModel(true, "Terjadi kesalahan dalam membuat file PDF ${e.message}")
        }
    }

    fun clearAllSavedUserData() {
        repository.clearSharedPreference()
    }

    private fun infoDownloadAgencyLetter(error: Boolean, message: String, fileName: String){
        _messageDownloadAgencyLetter.sendAction(DownloadAgencyContractUiModel(error, message, fileName))
    }
}