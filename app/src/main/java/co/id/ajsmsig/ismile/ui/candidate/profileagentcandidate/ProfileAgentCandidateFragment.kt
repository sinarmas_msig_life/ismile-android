package co.id.ajsmsig.ismile.ui.candidate.profileagentcandidate

import android.Manifest
import android.app.Activity
import android.app.Activity.RESULT_CANCELED
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.BuildConfig
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.common.ResultState
import co.id.ajsmsig.ismile.databinding.FragmentProfileAgentCandidateBinding
import co.id.ajsmsig.ismile.model.AgentCandidateReligion
import co.id.ajsmsig.ismile.model.api.AgentCandidateCityResponse
import co.id.ajsmsig.ismile.util.DateUtils
import co.id.ajsmsig.ismile.util.IMAGE_COMPRESSION_PERCENTAGE
import co.id.ajsmsig.ismile.util.REQUEST_IMAGE_CAPTURE
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class ProfileAgentCandidateFragment : BaseFragment<FragmentProfileAgentCandidateBinding, ProfileAgentCandidateViewModel>() {

    private var mCurrentPhotoPath = ""

    private var getPhotoId = 0

    private lateinit var religionAdapter: ProfileAgentCandidateReligionSpinnerAdapter

    private lateinit var spinnerReligion: Spinner

    private lateinit var autoComplete: AutoCompleteTextView

    override fun getLayoutResourceId() = R.layout.fragment_profile_agent_candidate

    override fun getViewModelClass() = ProfileAgentCandidateViewModel::class.java
    var isSubmitted : Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        isSubmitted = ProfileAgentCandidateFragmentArgs.fromBundle(arguments!!).isSubmitted
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        autoComplete = getDataBinding().contentAgentCandidateAddDetail.atvCity

        val vm = getViewModel()
        getDataBinding().fragment = this
        getDataBinding().vm = vm

        initSpinnerReligion()

        getViewModel().previewAgentCandidateProfile()

        getViewModel().getAllCity()
        getViewModel().isGetCity.observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it){
                    is ResultState.HasData -> getCity(it.data)
                    is ResultState.NoData -> snackbar(getString(R.string.unknown_error))
                    is ResultState.NoInternetConnection -> snackbar(getString(R.string.no_internet_connection))
                    else -> snackbar(getString(R.string.unknown_error))
                }

            }
        })

        //checked data apakah sudah pernah disubit apa belum
        getViewModel().getPersonalDataAgentCandidate()
        getViewModel().isCheckedDataInLocalDb.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.error) {
                    refreshSpinnerReligion(it.religionId!!)
                    refreshAutoCompleteCity(it.cityName!!)
                }
            }
        })

        getViewModel().profileImagePath.observe(viewLifecycleOwner, Observer {
            it?.let {
                setImagePathBitmap(it.imagePath, it.id)
            }
        })

        getViewModel().npwpImagePath.observe(viewLifecycleOwner, Observer {
            it?.let {
                setImagePathBitmap(it.imagePath, it.id)
            }
        })

        getViewModel().isSavingAccountImagePath.observe(viewLifecycleOwner, Observer {
            it?.let {
                setImagePathBitmap(it.imagePath, it.id)
            }
        })

        getViewModel().isUserInputValid.observe(viewLifecycleOwner, Observer {
            it?.let {
                snackbar(getString(it))
            }
        })

        getViewModel().isSuccesSave.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it) {
                    Snackbar.make(getDataBinding().root, "Sukses Disimpan", Snackbar.LENGTH_SHORT).show()
                    val action = ProfileAgentCandidateFragmentDirections.actionLaunchWorkExperienceFragment()
                    findNavController().navigate(action)
                } else {
                    Snackbar.make(getDataBinding().root, "Gagal Disimpan", Snackbar.LENGTH_SHORT).show()
                }
            }
        })

        getViewModel().previewIdentityImagePath ()
        getViewModel().isIdentityCardImage.observe(viewLifecycleOwner, Observer {
            it?.let {
                setIdentityImage(it)
            }
        })
    }

    fun onNextButtonClicked() {
        val milis = DateUtils.getCurrentTime()
        getViewModel().validateUserInput(milis)
    }

    fun onProfileImagePressed() {
        val permissionAllowed = checkPermission()
        if (permissionAllowed) takePicture() else requestPermission()
        getPhotoId = 1
    }

    fun onNpwpImagePressed() {
        val permissionAllowed = checkPermission()
        if (permissionAllowed) takePicture() else requestPermission()
        getPhotoId = 2
    }

    fun onBsbImagePressed() {
        val permissionAllowed = checkPermission()
        if (permissionAllowed) takePicture() else requestPermission()
        getPhotoId = 3
    }

    private fun takePicture() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file = createFile()
        val uri = FileProvider.getUriForFile(activity!!, BuildConfig.APPLICATION_ID, file)

        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
    }

    private fun checkPermission(): Boolean {
        return (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermission() {
        Dexter.withActivity(activity)
            .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // Check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        takePicture()
                    }
                    // Check for permanent denial of any permission
                    if (report.isAnyPermissionPermanentlyDenied) {
                        // permission is denied permenantly, navigate user to app settings
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken) {
                    token.continuePermissionRequest()
                }

            })
            .withErrorListener {
                Toast.makeText(activity, "Error occurred! $it", Toast.LENGTH_SHORT).show()
            }
            .onSameThread()
            .check()
    }

    @Throws(IOException::class)
    private fun createFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMddHHmmss").format(Date())
        val imageFile = "JPEG_" + timeStamp + "_"
        val storageDir = activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(imageFile, ".jpg", storageDir)

        mCurrentPhotoPath = image.absolutePath //contains image uri

        return image
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != RESULT_CANCELED){
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
                compressImage(mCurrentPhotoPath)

                //Save the bitmap to viewmodel, so it will be restored when orientation changed.
                getViewModel().saveImagePath(mCurrentPhotoPath, getPhotoId)

                setImagePathBitmap(mCurrentPhotoPath, getPhotoId)
            }
        }
    }

    //Set In Image
    private fun setImagePathBitmap(imagePath: String, getPhotoId: Int) {

        if (getPhotoId == 1) {
            Glide.with(activity!!).load(imagePath).thumbnail(0.1f).into(getDataBinding().contentProfilePhoto.cvProfile)
        }
        if (getPhotoId == 2) {
            Glide.with(activity!!).load(imagePath).thumbnail(0.1f).into( getDataBinding().contentProfilePhoto.cvNpwp)
        }
        if (getPhotoId == 3) {
            Glide.with(activity!!).load(imagePath).thumbnail(0.1f).into(getDataBinding().contentProfilePhoto.cvBsbBook)
        }
    }

    private fun compressImage(imagePath: String) {
        val bitmap = BitmapFactory.decodeFile(imagePath)
        val file = File(imagePath)
        val out = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.JPEG, IMAGE_COMPRESSION_PERCENTAGE, out)

        out.flush()
        out.close()
    }


    private fun initSpinnerReligion() {
        val religion = mutableListOf<AgentCandidateReligion>()
        religion.add(AgentCandidateReligion(1, "Islam"))
        religion.add(AgentCandidateReligion(2, "Kristen Protestan"))
        religion.add(AgentCandidateReligion(3, "Kristen Katolik"))
        religion.add(AgentCandidateReligion(4, "Budha"))
        religion.add(AgentCandidateReligion(5, "Hindu"))
        religion.add(AgentCandidateReligion(6, "Lain-lain"))

        religionAdapter = ProfileAgentCandidateReligionSpinnerAdapter(activity!!, religion)
        spinnerReligion = getDataBinding().contentAgentCandidateAddDetail.spinnerReligion
        spinnerReligion.adapter = religionAdapter
        spinnerReligion.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, p3: Long) {
                val religionId = religion[position].id
                getViewModel().saveSelectedReligionId(religionId)
            }
        }
    }

    private fun setIdentityImage(profpicImagePath: String?) {
        val decodedString = Base64.decode(profpicImagePath, Base64.DEFAULT)
        Glide.with(this).load(decodedString).into(getDataBinding().contentProfilePhoto.cvIdentity)
    }

    private fun getCity( data: List<AgentCandidateCityResponse>) {

        val citiesName = mutableListOf<String>()
        for (i in data){
            citiesName.add(i.KOTA_KABUPATEN)
        }

        val adapter = ArrayAdapter(context!!, android.R.layout.simple_dropdown_item_1line, citiesName)
        autoComplete.setAdapter(adapter)
        autoComplete.onItemClickListener = AdapterView.OnItemClickListener { parent, _ , position, id ->
            getViewModel().saveSelectedCityId(citiesName)
        }

        autoComplete.onFocusChangeListener = View.OnFocusChangeListener { view, b ->
            if (b) {
                autoComplete.showDropDown()
            }
        }
    }

    private fun refreshSpinnerReligion(index: Int) {
        spinnerReligion.setSelection(index - 1, true)
    }

    private fun refreshAutoCompleteCity(index1: String) {
        autoComplete.setText(index1)
    }
}