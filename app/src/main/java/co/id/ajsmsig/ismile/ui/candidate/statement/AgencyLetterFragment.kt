package co.id.ajsmsig.ismile.ui.candidate.statement

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentAgencyLetterBinding
import com.google.android.material.snackbar.Snackbar


class AgencyLetterFragment : BaseFragment<FragmentAgencyLetterBinding, StatementViewModel>() {

    override fun getLayoutResourceId() = R.layout.fragment_agency_letter

    override fun getViewModelClass() = StatementViewModel::class.java

    private var registrationId :String= " "

    var isSubmitted : Boolean= false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        registrationId = AgencyLetterFragmentArgs.fromBundle(arguments!!).registrationId
        isSubmitted = AgencyLetterFragmentArgs.fromBundle(arguments!!).isSubmitted
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val vm = getViewModel()

        getDataBinding().vm = vm
        getDataBinding().fragment = this

        getViewModel().findAllSavedData(registrationId)

        getViewModel().inputAreValid.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.error) {
                    Snackbar.make(getDataBinding().root, it.message.toString(), Snackbar.LENGTH_SHORT).show()
                } else {
                    launchAgentCandidateHomeFragment()
                }
            }
        })
    }

    fun onSaveButtonPressed() {
        val isAgencyLetterChecked = getDataBinding().contentAgencyLetter.cbAgencyLetter.isChecked
        getViewModel().validateAgencyLetterAnswer(registrationId, isAgencyLetterChecked)
    }

    private fun launchAgentCandidateHomeFragment() {
        val action = AgencyLetterFragmentDirections.actionLaunchCandidateAgentHomeFragment(registrationId)
        findNavController().navigate(action)
    }
}
