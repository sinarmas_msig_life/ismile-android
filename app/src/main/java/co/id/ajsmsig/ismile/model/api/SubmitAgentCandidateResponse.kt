package co.id.ajsmsig.ismile.model.api

import com.google.gson.annotations.SerializedName

data class SubmitAgentCandidateResponse(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("error")
    val error: Boolean,
    @SerializedName("message")
    val message: String
) {
    data class Data(
        @SerializedName("agent_candidate_registration_id")
        val agentCandidateRegistrationId: String
    )
}