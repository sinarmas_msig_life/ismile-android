package co.id.ajsmsig.ismile.db.entity.join

import androidx.room.ColumnInfo

data class AgentCandidateAndQuestionnairesAndQuestionnairesDetails (

    @ColumnInfo(name = "registration_id")
    val registrationId : String,

    @ColumnInfo(name = "ethic_code_checked")
    val ethicCodeChecked : Boolean,

    @ColumnInfo(name = "employment_contract_checked")
    val employmentContractChecked : Boolean,

    @ColumnInfo(name = "questionnaire_id")
    val questionnaireId : Long,

    @ColumnInfo(name = "question")
    val question : String,

    @ColumnInfo(name = "actual_answer")
    val actualAnswer : Int,

    @ColumnInfo(name = "questionnaire_detail_id")
    val questionnaireDetailId : Long?,

    @ColumnInfo(name = "label")
    val label : String?,

    @ColumnInfo(name = "answer")
    val answer : String?,

    @ColumnInfo(name = "is_active")
    val isActive : Boolean?
)