package co.id.ajsmsig.ismile.db.entity.embedded

import androidx.room.ColumnInfo
import androidx.room.TypeConverters
import co.id.ajsmsig.ismile.util.Converters

data class Signature(
    @TypeConverters(Converters::class)
    @ColumnInfo(name = "signature_checked")
    var signatureChecked: Boolean?,

    @ColumnInfo(name = "signature_img_path")
    var signatureImgPath: String?
)