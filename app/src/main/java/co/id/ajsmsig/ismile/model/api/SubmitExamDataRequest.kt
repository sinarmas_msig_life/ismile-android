package co.id.ajsmsig.ismile.model.api


import com.google.gson.annotations.SerializedName

data class SubmitExamDataRequest(
    @SerializedName("bsb_image")
    val bsbImage: String,
    @SerializedName("exam_city")
    val examCity: String,
    @SerializedName("exam_date")
    val examDate: String,
    @SerializedName("exam_method_id")
    val examMethodId: Int,
    @SerializedName("exam_place")
    val examPlaceName: String,
    @SerializedName("exam_product_type_id")
    val examProductTypeId: Int,
    @SerializedName("exam_type_id")
    val examTypeId: Int,
    @SerializedName("registration_id")
    val registrationId: String
)