package co.id.ajsmsig.ismile.ui.agent.addagentcandidate

import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.model.api.SubmitAgentCandidateRequest
import okhttp3.MultipartBody
import javax.inject.Inject

class AddAgentCandidateRemoteDataSource @Inject constructor(private val apiService: ApiService) {

    fun uploadFile(body: MultipartBody) = apiService.uploadFile(body)
    fun getAgentTitle(agentCode: String) = apiService.getAgentTitle(agentCode)

    fun submitAgentCandidate(request : SubmitAgentCandidateRequest) = apiService.submitAgentCandidate(request)

}