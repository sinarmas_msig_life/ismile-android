package co.id.ajsmsig.ismile.ui.agent.recruitsuccess

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.di.NetManager
import co.id.ajsmsig.ismile.model.api.StatusRecruitRequest
import co.id.ajsmsig.ismile.model.api.StatusRecruitResponse
import co.id.ajsmsig.ismile.util.PREF_KEY_AGENT_CODE
import io.reactivex.Observable
import javax.inject.Inject

class RecruitSuccesRepository @Inject constructor(private val netManager: NetManager, private val apiService: ApiService, private val preference : SharedPreferences) {

    suspend fun getRecruitSucces(requestCode: Int): StatusRecruitResponse {
        val agentCode = preference.getString(PREF_KEY_AGENT_CODE, null)
        return apiService.fetchStatusRecruit(StatusRecruitRequest(agentCode!!, requestCode))
    }

}