package co.id.ajsmsig.ismile.ui.leader.agentleaderapprove

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.id.ajsmsig.ismile.databinding.RvItemApprovalLeaderBinding
import co.id.ajsmsig.ismile.model.StatusApprove

class AgentLeaderApproveRecyclerAdapter (private var agentWaitToApprove: List<StatusApprove>, private val listener: onLeaderAppproveAgentCandidateListener) : RecyclerView.Adapter<AgentLeaderApproveRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RvItemApprovalLeaderBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = agentWaitToApprove.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(agentWaitToApprove[position], listener, holder.adapterPosition)

    class ViewHolder(private val binding:  RvItemApprovalLeaderBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(model: StatusApprove, listener: onLeaderAppproveAgentCandidateListener?, position: Int) {
            binding.model = model

            binding.btnApprove.setOnClickListener {
                listener?.onAgentCandidateApprove (model.registrationId, position)
            }

            binding.btnCancel.setOnClickListener {
                listener?.onAgentCandudateReject(model.registrationId)
            }

            binding.root.setOnClickListener {
                listener?.onRecruitPressed(model, position)
            }
            binding.executePendingBindings()
        }
    }

    interface onLeaderAppproveAgentCandidateListener {
        fun onRecruitPressed(candidateAgent: StatusApprove, position : Int)
        fun onAgentCandidateApprove (registrationId: String, position: Int)
        fun onAgentCandudateReject(registrationId: String)
    }

    fun refreshData(agentWaitToApprove: List<StatusApprove>) {
        this.agentWaitToApprove = agentWaitToApprove
        notifyDataSetChanged()
    }

}