package co.id.ajsmsig.ismile.model.uimodel

import co.id.ajsmsig.ismile.model.PreviewCandidateAgentModel

data class PreviewCandidateUiModel (val error:Boolean,val message:String,val preview:List<PreviewCandidateAgentModel>)