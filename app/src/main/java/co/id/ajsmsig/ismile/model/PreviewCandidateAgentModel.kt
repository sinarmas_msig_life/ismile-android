package co.id.ajsmsig.ismile.model


data class PreviewCandidateAgentModel(val fullName: String?,
                                      val bod: String?,
                                      val idCardNo: String?,
                                      val phoneNo: String?,
                                      val email: String?,
                                      val titleName: String?,
                                      val bopDate: String?,
                                      val gender: String?,
                                      val maritalStatus: String?,
                                      val profpicImagePath:String?,
                                      val error:Boolean,
                                      val message:String)