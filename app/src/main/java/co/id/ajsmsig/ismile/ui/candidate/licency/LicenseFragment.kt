package co.id.ajsmsig.ismile.ui.candidate.licency

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentLicenseBinding
import co.id.ajsmsig.ismile.model.DetailLicense
import co.id.ajsmsig.ismile.model.LicenseCitiesExam
import com.google.android.material.snackbar.Snackbar

class LicenseFragment : BaseFragment<FragmentLicenseBinding, LicenseViewModel>(), LicenseAdapter.onExamSubmitPressedListener {

    override fun getLayoutResourceId() = R.layout.fragment_license

    override fun getViewModelClass() = LicenseViewModel::class.java

    private lateinit var spinnerCities: Spinner

    private var citiesName = ""

    private val adapter = LicenseAdapter(listOf(), this)

    private lateinit var spinnerCitiesAdapter: LicenseCitiesSpinnerAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val vm = getViewModel()
        getDataBinding().vm = vm
        getDataBinding().fragment = this

        initRecyclerView()

        getViewModel().getALlCities()
        getViewModel().isCities.observe(viewLifecycleOwner, Observer {
            it?.let {
                refreshSpinnerCities(it.cities)
            }
        })

        getViewModel().isExam.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (!it.error){
                    refreshData(it.detail!!)
                }
            }
        })

        getViewModel().isSubmitSucces.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (!it.error){
                    launchLicenseSubmitFragment()
                    Snackbar.make(getDataBinding().root, it.message!!, Snackbar.LENGTH_SHORT).show()
                }else{
                    Snackbar.make(getDataBinding().root, it.message!!, Snackbar.LENGTH_SHORT).show()
                }
            }
        })

        getViewModel().checkedDataDatabase()
        getViewModel().isExamSelected.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.error){
                    launchDataSubmitExam(it.citiesId)
                }
            }
        })

    }

    fun onTextTryAgainClicked() {
        getViewModel().getALlCities()
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(activity)
        val dividerItemDecoration = DividerItemDecoration(getDataBinding().rvExamSubmitInfo.context, layoutManager.orientation)

        getDataBinding().rvExamSubmitInfo.layoutManager = layoutManager
        getDataBinding().rvExamSubmitInfo.adapter = adapter
        getDataBinding().rvExamSubmitInfo.addItemDecoration(dividerItemDecoration)
    }

    private fun refreshSpinnerCities(cityName: List<String>) {

        //Get city id
        val city = mutableListOf<LicenseCitiesExam>()
        for ((lenghtOfCity, i) in cityName.withIndex()){
            city.add(LicenseCitiesExam(lenghtOfCity, i))
        }
        spinnerCitiesAdapter= LicenseCitiesSpinnerAdapter(activity!!, city)
        spinnerCities = getDataBinding().spinnerCities
        spinnerCities.adapter = spinnerCitiesAdapter
        spinnerCities.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, p3: Long) {
                citiesName = cityName[position]
                val selectedCity = city[position].id
                getViewModel().saveCityId(selectedCity)
                getViewModel().retrieveInfoLicenseExam(citiesName)
            }
        }
    }

    override fun onSubmitExamPressed(submitSelected: DetailLicense, citiesId: Int) {
        getViewModel().saveDataSubmitted(submitSelected, citiesId, citiesName)
    }

    private fun launchLicenseSubmitFragment() {
        val action= LicenseFragmentDirections.actionLaunchLicenseSubmit()
        findNavController().navigate(action)
    }

    private fun refreshData (refreshData: List <DetailLicense>){
        adapter.refreshData(refreshData)
    }

    private fun launchDataSubmitExam(indexs: Int){
//       spinnerCities.setSelection(indexs, true)
    }

}
