package co.id.ajsmsig.ismile.ui.agent.addagentcandidate

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.model.AgentTitle
import co.id.ajsmsig.ismile.model.MessageInfo
import co.id.ajsmsig.ismile.model.OnProgressModel
import co.id.ajsmsig.ismile.model.api.SubmitAgentCandidateResponse
import co.id.ajsmsig.ismile.model.uimodel.AddAgentCandidateUiModel
import co.id.ajsmsig.ismile.model.uimodel.AgentTitleUiModel
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class AddAgentCandidateViewModel @Inject constructor(private val context: Context, private val repository: AddAgentCandidateRepository) :
    BaseViewModel() {

    val fullName = ObservableField<String>()
    val dateOfBirth = ObservableField<String>()
    val identityNumber = ObservableField<String>()
    val phoneNumber = ObservableField<String>()
    val emailAddress = ObservableField<String>("")
    val bopDate = ObservableField<String>()

    private val _displaySubmitDataOnProgress = MutableLiveData<OnProgressModel>()
    val displaySubmitDataOnProgress : LiveData<OnProgressModel>
        get() = _displaySubmitDataOnProgress

    private val dob = ObservableField<String>()
    private val bop = ObservableField<String>()

    val shouldEmailAddressRequestFocus = ObservableBoolean()
    private val titleId = ObservableInt(-1) //Default value if user not selected agent title is -1
    private val maritalStatusId = ObservableInt(-1)
    val agentTitles = ObservableField<List<AgentTitle>>()

    val errorEmailFormatInvalid = ObservableInt()
    val errorFirstName = ObservableInt()
    val errorDateOfBirth = ObservableInt()
    val errorIdentityNumber = ObservableInt()
    val errorPhoneNumber = ObservableInt()
    val errorBopDate = ObservableInt()
    val errorCompanyProfileUnchecked = ObservableInt()
    val errorCompensationUnchecked = ObservableInt()
    val errorBusinessOportunityUnchecked = ObservableInt()
    var genderId = ObservableField(-1)

    private val _identityCardImagePath = MutableLiveData<String>()
    val identityCardImagePath: LiveData<String>
        get() = _identityCardImagePath

    private val _isUserInputValid = MutableLiveData<MessageInfo>()
    val isUserInputValid: LiveData<MessageInfo>
        get() = _isUserInputValid

    private val _agentTitle = MutableLiveData<AgentTitleUiModel>()
    val agentTitle: LiveData<AgentTitleUiModel>
        get() = _agentTitle

    private val _mCurrentPath = MutableLiveData <String>()
    val mCurrentPath : LiveData <String>
            get()= _mCurrentPath
    private val _isSubmitDataSuccess = MutableLiveData<AddAgentCandidateUiModel>()
    val isSubmitDataSuccess : LiveData<AddAgentCandidateUiModel>
        get() = _isSubmitDataSuccess

    val buttonSubmitEnabled = ObservableBoolean(true)

    fun getAgentTitle() {
        mCompositeDisposable += repository.getAgentTitle()
            .map {
                val titles = mutableListOf<AgentTitle>()
                for (i in it.data) { titles.add(AgentTitle(i.titleId, i.titleName)) }
                AgentTitleUiModel(it.error, it.message, titles)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableObserver<AgentTitleUiModel>() {
                override fun onComplete() {
                }

                override fun onNext(data: AgentTitleUiModel) {
                    _agentTitle.value = data
                    agentTitles.set(data.titles)
                }
                override fun onError(e: Throwable) {
                    _agentTitle.value = AgentTitleUiModel(true, e.message!!, emptyList())
                }
            })
    }

    fun validateAllSubmitParams(fileInBase64Format : String) {
        if (fileInBase64Format.isEmpty()) {
            setUserInputIsError(true, "Tidak dapat mengambil file KTP yang dipilih. Mohon untuk ambil gambar dari kamera dan coba kembali")
            return
        }
        submitAgentCandidate(fileInBase64Format)
    }
    /**
     * Submit agent candidate flow
     * Execute this method, if success then execute upload file method
     * to upload the identity card image
     */
    private fun submitAgentCandidate(fileInBase64Format : String) {
        setDisplayOnProcess(false, "Mengunggah data kandidat agent")
        val agentCode = getAgentCode()
        val removedWhiteSpaceBase64File = removeLineBreakCharachter(fileInBase64Format)

        val fullName = fullName.get()!!.toUpperCase(Locale.ROOT)

        mCompositeDisposable += repository.submitAgentCandidate(fullName, dob.get()!!, identityNumber.get()!!, phoneNumber.get()!!, emailAddress.get()!!, titleId.get(), maritalStatusId.get(),genderId.get()!!, bop.get()!!, agentCode!!, removedWhiteSpaceBase64File)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { buttonSubmitEnabled.set(false) }
            .doOnTerminate { buttonSubmitEnabled.set(true) }
            .subscribeWith(object : DisposableObserver<SubmitAgentCandidateResponse>() {
                override fun onComplete() {
                    setDisplayOnProcess(true, null)
                }

                override fun onNext(response: SubmitAgentCandidateResponse) {
                    setDisplayOnProcess(true, null)
                    if (!response.error) {
                        _isSubmitDataSuccess.value = AddAgentCandidateUiModel(false, response.message)
                    } else {
                        _isSubmitDataSuccess.value = AddAgentCandidateUiModel(true, response.message)
                    }
                }

                override fun onError(e: Throwable) {
                    setDisplayOnProcess(true, null)
                    _isSubmitDataSuccess.value = AddAgentCandidateUiModel(true, "Terjadi kesalahan koneksi. Mohon untuk submit kembali")
                }

            })
    }

    fun validateUserInput(isProfileCompanyChecked: Boolean, isCompensationChecked: Boolean, isBusinessOportunityChecked: Boolean) {
        if (!isFirstNameValid()) {
            return
        }

        if (!isDateOfBirthValid()) {
            return
        }

        if (!isIdentityNumberValid()) {
            return
        }

        if (!isPhoneNumberValid()) {
            return
        }

        if (!isEmailFormatValid()) {
            return
        }

        if (!isAgentTitleSelected()) {
            return
        }

        if (!isMaritalStatusSelected()) {
            return
        }

        if (!isBopDateValid()) {
            return
        }

        if (!isProfileCompanyChecked(isProfileCompanyChecked)) {
            return
        }

        if (!isCompensationChecked(isCompensationChecked)) {
            return
        }

        if (!isGenderSelected()) {
            return
        }

        if (!isBusinessOportunityChecked(isBusinessOportunityChecked)) {
            return
        }

        if (!isIdentityCardImageTaken()) {
            return
        }

        setUserInputIsError(false, null)

    }

    private fun isFirstNameValid(): Boolean {
        if (fullName.get().isNullOrEmpty()) {
            setFirstNameError(R.string.no_empty_field)
            return false
        } else {
            setFirstNameError()
        }
        return true
    }

    private fun isDateOfBirthValid(): Boolean {
        if (dob.get().isNullOrEmpty()) {
            setDateOfBirthError(R.string.no_empty_field)
            return false
        } else {
            setDateOfBirthError()
        }
        return true
    }

    private fun isIdentityNumberValid(): Boolean {
        when {
            identityNumber.get().isNullOrEmpty() -> {
                setIdentityNumberError(R.string.no_empty_field)
                return false
            }
            identityNumber.get()!!.length < 14 -> {
                setIdentityNumberError(R.string.minimal_ktp_number)
                return false
            }
            else -> {
                setIdentityNumberError()
            }
        }

        return true
    }

    /**
     *  Validate email format only if email field is not empty
     */
    private fun isEmailFormatValid(): Boolean {
        if (!emailAddress.get().isNullOrEmpty()) {
            val isValidFormat = isValidEmail(emailAddress.get()!!)
            return if (isValidFormat) {
                setEmailFormatInvalidError()
                setShouldEmailRequestFocus(false)
                true
            } else {
                setEmailFormatInvalidError(R.string.fortmat_email_failed)
                setShouldEmailRequestFocus(true)
                false
            }
        }
        return true
    }

    private fun isAgentTitleSelected() : Boolean {
        val selectedTitle = titleId.get()

        //Title is not selected by user
        if (selectedTitle == -1) {
            setUserInputIsError(true, "Jabatan calon agen tidak boleh kosong. Mohon pilih terlebih dahulu")
            return false
        }

        return true
    }

    private fun isMaritalStatusSelected() : Boolean {
        val selectedMaritalStatusId = maritalStatusId.get()

        if (selectedMaritalStatusId == -1) {
            setUserInputIsError(true, "Status pernikahan calon agen tidak boleh kosong. Mohon pilih terlebih dahulu")
            return false
        }

        return true
    }

    private fun isPhoneNumberValid(): Boolean {
        val phoneNo = phoneNumber.get()

        if (phoneNo.isNullOrEmpty()) {
            setPhoneNumberError(R.string.no_empty_field)
            return false
        } else if (!phoneNo.startsWith("08")) {
            setPhoneNumberError(R.string.phone_number_start)
            return false
        } else if (phoneNo.length<10){
            setPhoneNumberError(R.string.minimum_phone)
            return false
        }
        else {
            setPhoneNumberError()
        }

        return true
    }

    private fun isGenderSelected(): Boolean {
        if (genderId.get() == -1) {
            setUserInputIsError(true, "Mohon pilih jenis kelamin terlebih dahulu")
            return false
        }
        return true
    }

    private fun isBopDateValid(): Boolean {
        if (bop.get().isNullOrEmpty()) {
            setBopDateError(R.string.no_empty_field)
            return false
        } else {
            setBopDateError()
        }
        return true
    }

    /**
     * To check whether user has been select the identity card using camera
     * Will return false if user not take the picture using camera yet
     */
    private fun isIdentityCardImageTaken(): Boolean {
        val imagePath = _identityCardImagePath.value
        if (imagePath.isNullOrEmpty()) {
            setUserInputIsError(true, "Mohon lengkapi foto KTP terlebih dahulu")
            return false
        }

        return true
    }


    private fun isProfileCompanyChecked(isProfileCompanyChecked: Boolean): Boolean {
        if (!isProfileCompanyChecked) {
            setCompanyProfileError(R.string.field_approves)
            setUserInputIsError(true, context.getString(R.string.statment_company_approve))
            return false
        } else {
            setCompanyProfileError()
        }
        return true
    }

    private fun isCompensationChecked(isCompensationChecked: Boolean): Boolean {
        if (!isCompensationChecked) {
            setCompensationError(R.string.field_approves)
            setUserInputIsError(true, context.getString(R.string.statment_company_approve))
            return false
        } else {
            setCompensationError()
        }

        return true
    }

    private fun isBusinessOportunityChecked(isBusinessOportunityChecked: Boolean): Boolean {
        if (!isBusinessOportunityChecked) {
            setBussinessOportunityError(R.string.field_approves)
            setUserInputIsError(true, context.getString(R.string.statment_bisnis))
            return false
        } else {
            setBussinessOportunityError()
        }

        return true
    }

    fun saveSelectedGenderId(id: Int)= genderId.set(id)

    private fun isValidEmail(email: String) = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()

    private fun setEmailFormatInvalidError(errorMessage: Int = 0) = errorEmailFormatInvalid.set(errorMessage)
    private fun setShouldEmailRequestFocus(requestFocus: Boolean) = shouldEmailAddressRequestFocus.set(requestFocus)

    private fun setFirstNameError(errorMessage: Int = 0) = errorFirstName.set(errorMessage)

    private fun setDateOfBirthError(errorMessage: Int = 0) = errorDateOfBirth.set(errorMessage)

    private fun setIdentityNumberError(errorMessage: Int = 0) = errorIdentityNumber.set(errorMessage)

    private fun setPhoneNumberError(errorMessage: Int = 0) = errorPhoneNumber.set(errorMessage)

    private fun setBopDateError(errorMessage: Int = 0) = errorBopDate.set(errorMessage)

    private fun setCompanyProfileError(errorMessage: Int = 0) = errorCompanyProfileUnchecked.set(errorMessage)

    private fun setCompensationError(errorMessage: Int = 0) = errorCompensationUnchecked.set(errorMessage)

    private fun setBussinessOportunityError(errorMessage: Int = 0) = errorBusinessOportunityUnchecked.set(errorMessage)

    private fun setUserInputIsError(error: Boolean = false, message: String?) {
        _isUserInputValid.value = MessageInfo(error, message)
    }

    fun saveIdentityCardImagePath(imagePath: String) {
        _identityCardImagePath.value = imagePath
    }

    fun saveSelectedTitleId(selectedTitleId: Int = -1) {
        titleId.set(selectedTitleId)
    }

    fun saveSelectedMaritalStatusId(selectedMaritalStatusId : Int = -1) {
        maritalStatusId.set(selectedMaritalStatusId)
    }

    fun saveSelectedBopDate(selectedBopDate : String) {
        bop.set(selectedBopDate)
    }

    fun saveSelectedBodDate(selectedBodDate : String) {
        dob.set(selectedBodDate)
    }

    private fun setDisplayOnProcess(isFinished : Boolean, message : String?) {
        _displaySubmitDataOnProgress.value = OnProgressModel(isFinished, message)
    }

    private fun getAgentCode() = repository.getAgentCode()

    private fun removeLineBreakCharachter(input : String) : String {
        return input.replace("\n", "")
    }

    fun saveMcurrentPath (mCurrentPath: String){
        _mCurrentPath.value = mCurrentPath
    }
}