package co.id.ajsmsig.ismile.ui.candidate.agentcandidate

import android.Manifest
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.common.ResultState
import co.id.ajsmsig.ismile.databinding.FragmentAgentCandidateBinding
import co.id.ajsmsig.ismile.model.api.StatusAgentCandidateApproveResponse
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.content_agent_candidate_info.view.*
import kotlinx.android.synthetic.main.custom_lisence_info.view.*

class AgentCandidateFragment : BaseFragment<FragmentAgentCandidateBinding, AgentCandidateViewModel>() {

    override fun getLayoutResourceId() = R.layout.fragment_agent_candidate

    override fun getViewModelClass() = AgentCandidateViewModel::class.java

    private val compositeDisposable = CompositeDisposable()

    private var registrationId = ""
    private var progressDialog : ProgressDialog? = null
    private var isEnabledEdit : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_home, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_inbox -> {
                launchNotificationFragment()
            }
            R.id.action_logout -> {
                logout()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val vm = getViewModel()

        getDataBinding().vm = vm
        getDataBinding().fragment = this

        vm.checkForAutomaticLogout()
        vm.isAutomaticLogout.observe(viewLifecycleOwner, Observer {
            it.let {
                if (it){
                    launchChooseLoginRole()
                    clearAllSavedUserData()
                    deleteFcmInstance()
                }
            }
        })

        registrationId = getViewModel().getRegistrationId()

        vm.findCurrentCompletionStatus(registrationId)

        vm.currentStatusDate()
        vm.currentStatus.observe(viewLifecycleOwner, Observer {
            it.let {
                when (it) {
                    is ResultState.HasData -> vm.settCurrentStatus(it.data)
                    is ResultState.NoInternetConnection -> snackbar(getString(R.string.no_internet_connection))
                    is ResultState.TimeOut -> snackbar(getString(R.string.timeout))
                    else -> snackbar(getString(R.string.unknown_error))
                }
            }
        })

        vm.checkToEnabledEdit(registrationId)
        getViewModel().isEnabledEdit.observe(viewLifecycleOwner, Observer {
            it?.let {
                isEnabledEdit = it
            }
        })
        
        vm.inputAreValid.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.isError) {
                    snackbar(it.message)
                }
            }
        })

        vm.launchStatementPage.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it) {
                    val action = AgentCandidateFragmentDirections.actionLaunchStatementFragment (registrationId, getViewModel().getAgentCandidateName()!!)
                    action.isSubmitted = isEnabledEdit
                    findNavController().navigate(action)
                } else {
                    snackbar(getString(R.string.complete_personal_data))
                }
            }
        })

        vm.launchSignaturePage.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it) {
                    val action = AgentCandidateFragmentDirections.actionLaunchSignatureFragment()
                    action.isSubmitted = isEnabledEdit
                    findNavController().navigate(action)
                } else {
                    snackbar(getString(R.string.complete_statment))
                }
            }
        })

        vm.isSubmitDataSuccess.observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it) {
                    is ResultState.Loading -> {

                    }
                    is ResultState.HasData -> {
                        alertDialog(getString(R.string.submit_success), it.data.message)
                        getViewModel().currentStatusDate()
                    }
                    is ResultState.NoInternetConnection -> {
                        alertDialog(getString(R.string.submit_failed), getString(R.string.no_internet_connection))
                    }
                    is ResultState.Error -> {
                        alertDialog(getString(R.string.submit_failed), getString(R.string.unknown_error))
                    }
                }
            }
        })

        getViewModel().displaySubmitDataOnProgress.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.isFinished) {
                   hideProgressDialog()
                } else {
                    displayProgressDialog(it.message!!)
                }
            }
        })

        vm.isLicenseSubmitAndApproveChecked.observe( viewLifecycleOwner, Observer {
            it?.let {
                when (it) {
                    is ResultState.HasData -> chooseRoleLicense(it.data)
                    is ResultState.NoInternetConnection -> snackbar(getString(R.string.no_internet_connection))
                    is ResultState.TimeOut -> snackbar(getString(R.string.timeout))
                    else -> snackbar(getString(R.string.unknown_error))
                }
            }
        })

        getViewModel().messageDownloadAgencyLetter.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.error) {
                    snackbar("Download pdf gagal ${it.message}")
                } else {
                    snackbar("Download pdf berhasil. File disimpan di directory download")
                }
            }
        })

        getDataBinding().contentHeader.tvRegistrationId.text = registrationId
        getDataBinding().contentHeader.tvAgentUsername.text = getViewModel().getAgentCandidateName()
    }

    private fun chooseRoleLicense(result: StatusAgentCandidateApproveResponse) {
        if (result.data.isExamSubmitted && result.data.statusApprove){
            displayInfoLicense(result.data.examCity, result.data.examDate, result.data.examPlace, result.data.examProductType, result.data.examMethodName, result.data.examType )
        }
        else if (!result.data.isExamSubmitted && result.data.statusApprove){
           launchLicensePage()
        }
        else {
            snackbar(result.message)
        }
    }

    fun onButtonSubmitPressed() {
        getViewModel().validateSubmit(registrationId)
    }

    fun onPersonalDataClick() {
        val action = AgentCandidateFragmentDirections.actionProfileAgentLaunch()
        action.isSubmitted = isEnabledEdit
        findNavController().navigate(action)
    }

    /**
     * Hit api to to check input exam allowed
     */
    fun onLicensePressed() {
        getViewModel().checkIfLicenseMenuEnabled()
    }

    fun onStatementPressed() {
        getViewModel().checkIfStatementMenuEnabled(registrationId)
    }

    fun onSignaturePressed() {
        getViewModel().checkIfSignatureMenuEnabled(registrationId)
    }

    private fun logout() {
        displayAlertLogout()
    }

    fun onButtonDownloadAgencyLetterPressed() {
        getPermissionRequest (registrationId)
    }

    private fun getPermissionRequest (registerId: String){
        Dexter.withActivity(activity)
            .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener {

                override fun onPermissionsChecked(report: MultiplePermissionsReport) {

                    // check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        getViewModel().downloadAgencyLetter(registerId)
                    }

                    // check for permanent denial of any permission

                    if (report.isAnyPermissionPermanentlyDenied) {
                        // permission is denied permenantly, navigate user to app settings
                        showSettingsDialog()
                    }

                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }

            })
            .withErrorListener {
                Snackbar.make(getDataBinding().root, "Gagal mendapatkan akses ke storage. $it", Snackbar.LENGTH_LONG).show()
            }
            .onSameThread()
            .check()
    }

    private fun launchChooseLoginRole() {
        val action = AgentCandidateFragmentDirections.actionLaunchLoginRole()
        findNavController().navigate(action)
    }

    private fun clearAllSavedUserData() {
        getViewModel().clearAllSavedUserData()
    }

    private fun launchNotificationFragment() {
        val action = AgentCandidateFragmentDirections.actionLaunchInboxFragment()
        findNavController().navigate(action)
    }

    private fun launchLicensePage (){
        val action = AgentCandidateFragmentDirections.actionLaunchLicenseFragment()
        action.isSubmitted = isEnabledEdit
        findNavController().navigate(action)
    }

    private fun displayAlertLogout (){
        val title = getString(R.string.confirm_logut)
        val message= getString(R.string.logout_message)
        AlertDialog.Builder(activity)
            .setTitle(title)
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok) { dialogInterface, _ ->
                launchChooseLoginRole()
                clearAllSavedUserData()
                deleteFcmInstance()
                dialogInterface.dismiss()
            }.setNegativeButton(android.R.string.no){dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            .show()
    }

    private fun deleteFcmInstance() {
        compositeDisposable += Observable.fromCallable {
            FirebaseInstanceId.getInstance().deleteInstanceId() }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    private fun displayInfoLicense( examCity: String, examDateLicense: String, examPlace: String, examProductType: String, examMethod: String, examType: String) {

        val view = layoutInflater.inflate(R.layout.custom_lisence_info, null)
        //TODO : Benerin dan dirubah codinganya
        view.tvExamCity.setText(examCity)
        view.tvExamDateLicense.setText(examDateLicense)
        view.tvExamPlace.setText(examPlace)
        view.tvExamProductType.setText(examProductType)
        view.tvExamMethod.setText(examMethod)
        view.tvExamType.setText(examType)

        val builder = AlertDialog.Builder(activity)
        builder.setTitle(getString(R.string.exam_info))
        builder.setView(view)
               .setCancelable(false)
               .setPositiveButton(android.R.string.ok) { dialogInterface, _ -> dialogInterface.dismiss() }
               .show()
    }

    private fun displayProgressDialog(title : String?) {
        progressDialog = ProgressDialog(activity)
        progressDialog?.isIndeterminate = true
        progressDialog?.setTitle(title)
        progressDialog?.setMessage(getString(R.string.please_wait))
        progressDialog?.setCancelable(false)
        progressDialog?.show()
    }
    private fun hideProgressDialog() {
        progressDialog?.dismiss()
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private fun showSettingsDialog() {
        val builder = androidx.appcompat.app.AlertDialog.Builder(activity!!)
        builder.setTitle("Izin diperlukan")
        builder.setMessage(getString(R.string.storage_permission))
        builder.setPositiveButton(getString(R.string.open_setting)) { dialog, _ ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(getString(R.string.cancel)) { dialog, _ -> dialog.cancel() }
        builder.show()

    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", activity?.packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }
}