package co.id.ajsmsig.ismile.ui.agent.recruitfailed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.SingleLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.common.ResultState
import co.id.ajsmsig.ismile.model.StatusRecruit
import co.id.ajsmsig.ismile.model.api.StatusRecruitResponse
import co.id.ajsmsig.ismile.model.uimodel.StatusRecruitUiModel
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class RecruitFailedViewModel @Inject constructor(private val repository: RecruitFailedRepository) : BaseViewModel() {

    private val _recruits = SingleLiveData<ResultState<List<StatusRecruit>>>()
    val recruits: LiveData<ResultState<List<StatusRecruit>>>
        get() = _recruits

    fun getAllRecruitFiled(requestCode: Int) {
        setIsLoading(true)
        viewModelScope.launch (Dispatchers.IO){
            try {
                val result = repository.getRecruitFailed(requestCode)
                setIsLoading(false)
                if (result.error){
                    setResultRecruits(ResultState.Error(R.string.unknown_error))
                    return@launch
                }
                if (result.data.failed.isEmpty()){
                    setResultRecruits(ResultState.NoData())
                    return@launch
                }
                val data  = transformData(result.data.failed)
                setResultRecruits(ResultState.HasData(data))
            }catch (e: Throwable){
                setIsLoading(false)
                when (e) {
                    is IOException -> setResultRecruits(ResultState.NoInternetConnection())
                    is TimeoutException -> setResultRecruits(ResultState.TimeOut(R.string.timeout))
                    else -> setResultRecruits(ResultState.Error(R.string.unknown_error))
                }
            }
        }
    }

    private fun transformData (data: List<StatusRecruitResponse.Data.Failed>) : List<StatusRecruit> {
        val recruits = mutableListOf<StatusRecruit>()
        for (i in data) {
            recruits.add(
                StatusRecruit(
                    i.agentCandidateRegistrationId,
                    i.candidateName,
                    i.latestStatus,
                    i.profpicImagePath,
                    i.statusDate,
                    i.submitDate
                )
            )
        }
        return recruits
    }

    private fun setResultRecruits(data: ResultState<List<StatusRecruit>>){
        _recruits.sendActionOnBackground(data)
    }

//    fun getAllRecruitFiled(requestCode: Int) {
//        setIsLoading(true)
//        mCompositeDisposable += repository.getRecruitFailed(requestCode)
//            .map {
//                val recruits = mutableListOf<StatusRecruit>()
//                for (i in it.data.failed) {
//                    recruits.add(
//                        StatusRecruit(
//                            i.agentCandidateRegistrationId,
//                            i.candidateName,
//                            i.latestStatus,
//                            i.profpicImagePath,
//                            i.statusDate,
//                            i.submitDate
//                        )
//                    )
//                }
//                StatusRecruitUiModel(it.error, it.message, recruits)
//            }
//            .subscribeOn(Schedulers.io())
//            .doOnSubscribe { onRetrieveAllRecruitStart() }
//            .doOnTerminate { onRetrieveAllRecruitFinish() }
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe (
//                { data -> onRetrieveAllRecruitSuccess(data) },
//                { throwable -> onRetrieveAllRecruitError(throwable) }
//            )
//    }
//
//    private fun isRecruitsEmpty(recruits : List<StatusRecruit>) : Boolean {
//        return recruits.isEmpty()
//    }
//
//    private fun onRetrieveAllRecruitStart() {
//        setIsLoading(true)
//    }
//    private fun onRetrieveAllRecruitFinish() {
//        setIsLoading(false)
//    }
//
//    private fun onRetrieveAllRecruitSuccess(data: StatusRecruitUiModel) {
//        _recruitInfo.value = data
//
//    }
//
//    private fun onRetrieveAllRecruitError(throwable: Throwable) {
//
//    }
}