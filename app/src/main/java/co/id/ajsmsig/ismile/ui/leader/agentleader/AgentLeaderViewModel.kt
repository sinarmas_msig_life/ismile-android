package co.id.ajsmsig.ismile.ui.leader.agentleader

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import co.id.ajsmsig.ismile.SingleLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.model.StatusApprove
import co.id.ajsmsig.ismile.model.api.AgentLeaderApproveResponse
import co.id.ajsmsig.ismile.model.MessageInfo
import co.id.ajsmsig.ismile.model.uimodel.StatusApprovalUiModel
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class AgentLeaderViewModel @Inject constructor(private val repository: AgentLeaderRepository) : BaseViewModel() {

    var agentLeaderName = ObservableField<String>()
    var agentLeaderCode = ObservableField<String>()
    var agentLeaderTitle = ObservableField<String>()

    private val _isApproveSucces= SingleLiveData <MessageInfo>()
    val isApproveSucces : LiveData <MessageInfo>
        get() = _isApproveSucces

    private val _isStatusApprove = SingleLiveData<StatusApprovalUiModel>()
    val isStatusApprove: LiveData<StatusApprovalUiModel>
        get() = _isStatusApprove

    val isConnected = ObservableBoolean (false)
    val isEmptyApprove = ObservableBoolean ()

    fun getAllApprovalAgentCandidate(requestCode:Int) {
        isLoading.set(true)
        mCompositeDisposable += repository.fetchApprovalAgentCandidate(requestCode)
            .map {
                val status = mutableListOf<StatusApprove>()
                for (i in it.data){
                    status.add(StatusApprove(i.registrationId,
                        i.agentCandidateName,
                        i.submitDate,
                        i.titleName,
                        i.profpicImagePath))
                }
                StatusApprovalUiModel (it.error, it.message, status)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { data -> getApprovalDataSucces(data) },
                { error -> getApprovalDataFailed(error) }
            )
    }

    fun agentLeaderAgrementToApprove (registrationId: String, reasonReject: String, statusApporve: Boolean ){
        isLoading.set(true)
        mCompositeDisposable += repository.setApporvalAggrementAgentLeader(registrationId, reasonReject, statusApporve)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {data -> agentLeaderAgrementSucces(data)},
                {error -> agentLeaderAgrementFailed(error) })
    }

    private fun agentLeaderAgrementSucces(data: AgentLeaderApproveResponse) {
        isLoading.set(false)
        isConnected.set(false)
       _isApproveSucces.value= MessageInfo(data.error, data.message)
    }

    private fun agentLeaderAgrementFailed(error: Throwable) {
        isLoading.set(false)
        isConnected.set(true)
    }

    private fun getApprovalDataSucces(data: StatusApprovalUiModel) {
        if (!data.status.isNullOrEmpty()){
            isEmptyApprove.set(false)
        }else{
            isEmptyApprove.set(true)
        }
        _isStatusApprove.value=data
        isLoading.set(false)
    }

    private fun getApprovalDataFailed(error: Throwable?) {
        Timber.e("$error")
        isLoading.set(false)
    }

    fun displayAgenLeaderStatus (){
        agentLeaderName.set(getLeaderName())
        agentLeaderCode.set(getLeaderCode())
        agentLeaderTitle.set(getLeaderTittle())
    }

    fun getLeaderName() = repository.getSavedAgentLeaderName()
    fun getLeaderCode() = repository.getSavedAgentLeaderCode()
    fun getLeaderTittle ()= repository.getSavedAgentLeaderTitle()

    fun clearAllSavedUserData() {
        repository.clearSharedPreference()
    }

}
