package co.id.ajsmsig.ismile.ui.agent.login

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.util.*
import javax.inject.Inject

class LoginLocalDataSource @Inject constructor(private val editor: SharedPreferences.Editor, private val preferences: SharedPreferences) {

    fun setLoggedIn(isLoggedIn : Boolean) {
        editor.putBoolean(PREF_KEY_LOGIN, isLoggedIn)
        editor.apply()
    }

    fun saveAgentCode(agentCode : String) {
        editor.putString(PREF_KEY_AGENT_CODE, agentCode)
        editor.apply()
    }

    fun getSavedAgentCode() = preferences.getString(PREF_KEY_AGENT_CODE, null)


    fun saveAgentName(agentName : String) {
        editor.putString(PREF_KEY_AGENT_NAME, agentName)
        editor.apply()
    }

    fun getSavedAgentName() = preferences.getString(PREF_KEY_AGENT_NAME, null)


    fun saveAgentTitle(agentTitle : String) {
        editor.putString(PREF_KEY_AGENT_TITLE, agentTitle)
        editor.apply()
    }

    fun saveAgentUserId(userId : String) {
        editor.putString(PREF_KEY_AGENT_USER_ID, userId)
        editor.apply()
    }

    fun getSavedAgentTitle() = preferences.getString(PREF_KEY_AGENT_TITLE, null)

    fun clearSharedPreference() {
        editor.remove(PREF_KEY_AGENT_TITLE)
        editor.remove(PREF_KEY_AGENT_CODE)
        editor.remove(PREF_KEY_AGENT_USER_ID)
        editor.remove(PREF_KEY_LOGIN_ROLE_TYPE)
        editor.remove(PREF_KEY_LOGIN)
        editor.remove(PREF_KEY_AGENT_NAME)
        editor.clear()
        editor.commit()
    }

    fun saveLoginRoleType(loginRoleType : Int) {
        editor.putInt(PREF_KEY_LOGIN_ROLE_TYPE, loginRoleType)
        editor.apply()
    }
}