package co.id.ajsmsig.ismile.db.entity

import androidx.room.*
import co.id.ajsmsig.ismile.db.entity.embedded.*
import co.id.ajsmsig.ismile.util.Converters

@Entity(tableName = "agent_candidates")
data class AgentCandidate(

    @PrimaryKey
    @ColumnInfo(name = "id")
    var registrationId: String,

    @Embedded
    val personalData: PersonalData,

    @Embedded
    val statement: Statement,

    @Embedded
    val signature: Signature,

    @Embedded
    val exam : Exam,

    @Embedded
    val completionStatus: CompletionStatus?,

    @TypeConverters(Converters::class)
    @ColumnInfo(name = "is_submitted")
    var isSubmitted: Boolean?

)