package co.id.ajsmsig.ismile.model

data class AgentCandidateCompletionStatusModel (
                                                var personalDataComplete:String?,
                                                var personalDataDate:String?,
                                                var workExperienceDate:String?,
                                                var educationDate:String?,

                                                var statementComplete : String?,
                                                var questionnaireDate:String?,
                                                var ethicCodeDate:String?,
                                                var employmentContractDate:String?,
                                                var agencyLetterDate:String?,

                                                var signatureComplete : String?,

                                                var examCompleted : String,
                                                var bsbSubmitDate:String?, //submit bsb
                                                var examInputDate:String? //input ujian
)