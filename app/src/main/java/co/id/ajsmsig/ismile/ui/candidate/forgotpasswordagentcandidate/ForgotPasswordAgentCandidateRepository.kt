package co.id.ajsmsig.ismile.ui.candidate.forgotpasswordagentcandidate

import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.di.NetManager
import co.id.ajsmsig.ismile.model.api.AgentCandidateForgotPasswordRequest
import co.id.ajsmsig.ismile.model.api.AgentCandidateForgotPasswordResponse
import io.reactivex.Observable
import javax.inject.Inject

class ForgotPasswordAgentCandidateRepository @Inject constructor(private val netManager: NetManager,private val apiService: ApiService){

    fun forgotPasswordAgentCandidate(registrationId:String,phoneNumber:String): Observable<AgentCandidateForgotPasswordResponse> {
        if (netManager.isConnectedToInternet){
            return apiService.agentCandidateForgotPassword(AgentCandidateForgotPasswordRequest(registrationId,phoneNumber))
        }

        return Observable.just(AgentCandidateForgotPasswordResponse(true,"Koneksi tidak berhasil. Mohon periksa kembali koneksi internet"))
    }

}