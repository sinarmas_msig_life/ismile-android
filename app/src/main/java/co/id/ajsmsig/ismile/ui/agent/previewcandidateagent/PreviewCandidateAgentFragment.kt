package co.id.ajsmsig.ismile.ui.agent.previewcandidateagent

import android.os.Bundle
import android.util.Base64
import android.view.View
import androidx.lifecycle.Observer
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentPreviewCandidateAgentBinding
import com.bumptech.glide.Glide

class PreviewCandidateAgentFragment : BaseFragment<FragmentPreviewCandidateAgentBinding, PreviewCandidateAgentViewModel>() {
    override fun getLayoutResourceId()= R.layout.fragment_preview_candidate_agent

    override fun getViewModelClass()= PreviewCandidateAgentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val vm = getViewModel()

        getDataBinding().vm = vm

        val registrationId=PreviewCandidateAgentFragmentArgs.fromBundle(arguments!!).registrationId

        getViewModel().getCandidateAgentData(registrationId)
        getViewModel().isConnected.observe(this, Observer {
            it?.let {
                if(it.error){
                    snackbar("Terjadi kesalahan. Mohon coba kembali")
                }
                else{
                    val decodedString = Base64.decode(it.profpicImagePath, Base64.DEFAULT)
                    Glide.with(this).load(decodedString).thumbnail(0.1f).into(getDataBinding().contentPreviewCandidateAgentPhoto.ivDocumentImage)
                }
            }
        })
    }
}
