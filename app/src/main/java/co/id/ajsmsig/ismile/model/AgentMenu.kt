package co.id.ajsmsig.ismile.model

data class AgentMenu(val imageId : Int, val menuName : String, val isEnabled : Boolean)