package co.id.ajsmsig.ismile.db.entity.minimal

import androidx.room.ColumnInfo

data class PersonalDataMinimal (

    @ColumnInfo(name = "email")
    var email: String?,

    @ColumnInfo(name = "profile_photo_img_path")
    var profilePhotoImgPath: String?,

    @ColumnInfo(name = "npwp_photo_img_path")
    var npwpPhotoImgPath: String?,

    @ColumnInfo(name = "saving_account_img_path")
    var savingAccountImgPath: String?,

    @ColumnInfo (name="home_address")
    var homeAddress:String?,

    @ColumnInfo (name="place_of_birth")
    var placeOfBirth:String?,

    @ColumnInfo (name = "city_id")
    var cityId :Int?,

    @ColumnInfo(name = "city_name")
    var cityName: String?,

    @ColumnInfo (name = "npwp_number")
    var npwpNumber:String?,

    @ColumnInfo (name = "house_phone")
    var housePhone:String?,

    @ColumnInfo (name = "gender_id")
    var genderId : Int,

    @ColumnInfo (name = "religion_id")
    var religionId : Int,

    @ColumnInfo(name = "personal_data_date")
    var personalDataDate: String?
)