package co.id.ajsmsig.ismile.model.api


import com.google.gson.annotations.SerializedName

data class SubmitExamDataResponse(
    @SerializedName("error")
    val error: Boolean,
    @SerializedName("message")
    val message: String
)