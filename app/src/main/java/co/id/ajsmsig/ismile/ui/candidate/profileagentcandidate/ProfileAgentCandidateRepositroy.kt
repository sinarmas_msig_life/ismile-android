package co.id.ajsmsig.ismile.ui.candidate.profileagentcandidate

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.db.dao.AgentCandidateDao
import co.id.ajsmsig.ismile.db.entity.minimal.PersonalDataMinimal
import co.id.ajsmsig.ismile.db.entity.minimal.ProfileAgentCandidateMinimal
import co.id.ajsmsig.ismile.di.NetManager
import co.id.ajsmsig.ismile.model.api.AgentCandidateCityResponse
import co.id.ajsmsig.ismile.model.api.PreviewCandidateAgentResponse
import co.id.ajsmsig.ismile.util.PREFF_KEY_AGENT_CANDIDATE_CODE
import io.reactivex.Maybe
import io.reactivex.Observable
import javax.inject.Inject

class ProfileAgentCandidateRepositroy @Inject constructor(private var preferences: SharedPreferences, private val dao: AgentCandidateDao, private val netManager: NetManager, private val apiService: ApiService) {

    suspend fun getAllCity(): List<AgentCandidateCityResponse> {
        return apiService.getAllCity()
    }

    fun getIdentityImagePath () : Observable <PreviewCandidateAgentResponse>{
        val userId = preferences.getString(PREFF_KEY_AGENT_CANDIDATE_CODE, null)
        if (netManager.isConnectedToInternet){
            return apiService.getPreviewCandidateAgent(userId!!)
        }
        return Observable.just(PreviewCandidateAgentResponse(
                PreviewCandidateAgentResponse.Data("","","","","","","","","", "","","","",""),
                true,
                "Mohon periksa kembali koneksi internet")
        )
    }

    fun updateAgentCandidateData(
        email: String,
        homeAddress: String,
        placeOfBirth: String,
        cityId: Int,
        cityName: String,
        npwpNumber: String,
        housePhone: String,
        profilePhotoImgPath: String,
        npwpPhotoImgPath: String,
        savingAccountImgPath: String,
        religionId: Int,
        personalDataDate: String
    ) {
        val userId = preferences.getString(PREFF_KEY_AGENT_CANDIDATE_CODE, null)
        dao.updateProfileAgentCandidate(
            email,
            homeAddress,
            placeOfBirth,
            cityId,
            cityName,
            npwpNumber,
            housePhone,
            profilePhotoImgPath,
            npwpPhotoImgPath,
            savingAccountImgPath,
            religionId,
            personalDataDate,
            userId!!
        )
    }

    fun getPersonalData(): Maybe<PersonalDataMinimal> {
        val userId = preferences.getString(PREFF_KEY_AGENT_CANDIDATE_CODE, null)
        return dao.getPersonalData(userId!!)
    }

    fun viewProfilAgentCandidate(): Maybe<ProfileAgentCandidateMinimal> {
        val userId = preferences.getString(PREFF_KEY_AGENT_CANDIDATE_CODE, null)
        return dao.getProfileAgentCandidate(userId!!)
    }

}