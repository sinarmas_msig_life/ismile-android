package co.id.ajsmsig.ismile.di.module


import dagger.Module
import dagger.android.ContributesAndroidInjector
import co.id.ajsmsig.ismile.ui.main.MainActivity


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun bindMainActivity (): MainActivity
}