package co.id.ajsmsig.ismile.util

import java.text.NumberFormat
import java.text.ParseException
import java.util.*

object CurrencyUtils {

    fun convertNumberToCurrency(number: String): String? {
        val currency = number.toInt()

        val localeID = Locale("in", "ID")
        val curencyFormat = NumberFormat.getCurrencyInstance(localeID)
        try {
            return curencyFormat.format(currency)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return null
    }

}