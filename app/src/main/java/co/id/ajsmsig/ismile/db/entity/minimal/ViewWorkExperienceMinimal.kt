package co.id.ajsmsig.ismile.db.entity.minimal

import androidx.room.ColumnInfo

data class ViewWorkExperienceMinimal (
    @ColumnInfo(name = "company_name")
    var companyName: String?,

    @ColumnInfo(name = "last_position")
    var lastPosition: String?,

    @ColumnInfo(name = "start_date_work")
    var startDateWork: String?,

    @ColumnInfo(name = "end_date_work")
    var endDateWork: String?,

    @ColumnInfo(name = "school_id")
    var schoolId: Int?

)