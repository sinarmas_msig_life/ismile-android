package co.id.ajsmsig.ismile.model.uimodel

data class LoginAgentUiModel (val error:Boolean,val message:String, val leader:Boolean)