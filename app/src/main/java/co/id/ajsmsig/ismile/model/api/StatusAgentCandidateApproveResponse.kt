package co.id.ajsmsig.ismile.model.api


import com.google.gson.annotations.SerializedName

data class StatusAgentCandidateApproveResponse(
    @SerializedName("data")
    val data: Data,
    @SerializedName("error")
    val error: Boolean,
    @SerializedName("message")
    val message: String
) {
    data class Data(
        @SerializedName("exam_city")
        val examCity: String,
        @SerializedName("exam_date")
        val examDate: String,
        @SerializedName("exam_method_name")
        val examMethodName: String,
        @SerializedName("exam_place")
        val examPlace: String,
        @SerializedName("exam_product_type")
        val examProductType: String,
        @SerializedName("exam_type")
        val examType: String,
        @SerializedName("is_exam_submitted")
        val isExamSubmitted: Boolean,
        @SerializedName("status_approve")
        val statusApprove: Boolean,
        @SerializedName("status_message")
        val statusMessage: String
    )
}