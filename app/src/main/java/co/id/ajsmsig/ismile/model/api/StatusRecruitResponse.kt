package co.id.ajsmsig.ismile.model.api

import com.google.gson.annotations.SerializedName

data class StatusRecruitResponse(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("error")
    val error: Boolean,
    @SerializedName("message")
    val message: String
) {
    data class Data(
        @SerializedName("all_recruit")
        val allRecruit: List<AllRecruit>,
        @SerializedName("approval")
        val approval: List<Approval>,
        @SerializedName("failed")
        val failed: List<Failed>,
        @SerializedName("success")
        val success: List<Success>
    ) {
        data class AllRecruit(
            @SerializedName("agent_candidate_registration_id")
            val agentCandidateRegistrationId: String,
            @SerializedName("candidate_name")
            val candidateName : String?,
            @SerializedName("latest_status")
            val latestStatus: String?,
            @SerializedName("profpic_image_path")
            val profpicImagePath: String,
            @SerializedName("status_date")
            val statusDate: String?,
            @SerializedName("submit_date")
            val submitDate: String?
        )

        data class Approval(
            @SerializedName("agent_candidate_registration_id")
            val agentCandidateRegistrationId: String,
            @SerializedName("candidate_name")
            val candidateName : String?,
            @SerializedName("latest_status")
            val latestStatus: String?,
            @SerializedName("profpic_image_path")
            val profpicImagePath: String,
            @SerializedName("status_date")
            val statusDate: String?,
            @SerializedName("submit_date")
            val submitDate: String?
        )

        data class Success(
            @SerializedName("agent_candidate_registration_id")
            val agentCandidateRegistrationId: String,
            @SerializedName("candidate_name")
            val candidateName : String?,
            @SerializedName("latest_status")
            val latestStatus: String?,
            @SerializedName("profpic_image_path")
            val profpicImagePath: String,
            @SerializedName("status_date")
            val statusDate: String?,
            @SerializedName("submit_date")
            val submitDate: String?
        )

        data class Failed(
            @SerializedName("agent_candidate_registration_id")
            val agentCandidateRegistrationId: String,
            @SerializedName("candidate_name")
            val candidateName : String?,
            @SerializedName("latest_status")
            val latestStatus: String?,
            @SerializedName("profpic_image_path")
            val profpicImagePath: String,
            @SerializedName("status_date")
            val statusDate: String?,
            @SerializedName("submit_date")
            val submitDate: String?
        )
    }
}