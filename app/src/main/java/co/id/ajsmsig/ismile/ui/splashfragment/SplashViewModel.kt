package co.id.ajsmsig.ismile.ui.splashfragment

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.common.ResultState
import co.id.ajsmsig.ismile.model.AppVersionData
import co.id.ajsmsig.ismile.model.AppVersionRequest
import co.id.ajsmsig.ismile.util.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class SplashViewModel @Inject constructor(private val repository: SplashRepository) : BaseViewModel() {

    private val _appVersionResult = MutableLiveData<ResultState<AppVersionData>>()
    val appVersionResult : LiveData<ResultState<AppVersionData>>
        get() = _appVersionResult

    fun destinationScreenId() : Int {
        val isLoggedIn = repository.getUserCurrentSession().isLoggedIn
        val loginRoleType = repository.getUserCurrentSession().loginRoleType

        if (isLoggedIn && loginRoleType == LOGIN_TYPE_AGENT) {
            //Agent Home
            return LAUNCH_AGENT_AGENT_HOME
        } else if (!isLoggedIn && loginRoleType == LOGIN_TYPE_AGENT) {
            //Agent Login
            return LAUNCH_AGENT_AGENT_LOGIN
        } else if (!isLoggedIn && loginRoleType == LOGIN_TYPE_NOT_SELECTED ) {
            //Choose login
            return LAUNCH_CHOOSE_LOGIN_ROLE
        } else if (!isLoggedIn && loginRoleType == LOGIN_TYPE_AGENT_CANDIDATE ) {
            //Agent candidate login
            return LAUNCH_AGENT_AGENT_CANDIDATE_LOGIN
        } else if (isLoggedIn && loginRoleType == LOGIN_TYPE_AGENT_CANDIDATE ) {
            //Agent candidate home
            return LAUNCH_AGENT_AGENT_CANDIDATE_HOME
        } else if (!isLoggedIn && loginRoleType == LOGIN_TYPE_LEADER_AGENT){
            return LAUNCH_LEADER_AGENT_LOGIN
        } else if (isLoggedIn && loginRoleType == LOGIN_TYPE_LEADER_AGENT){
            return LAUNCH_LEADER_AGENT_HOME
        }

        return LAUNCH_SCREEN_UNDEFINED
    }

    private fun getNewAppVersion () : String{
        return "1.0.0-dev"
    }

    fun getInfoUpdateApps (flagApps: Int, flagsPlatform: Int) {
        viewModelScope.launch (IO) {
            try {
                val data = AppVersionRequest (flagApps, flagsPlatform)
                val result = repository.getInfoUpdateApps(data)
                if (!result.error){
                    setResult(ResultState.HasData(result.data))
                }
                else {
                    setResult(ResultState.Error(R.string.unknown_error))
                }

            } catch (error: Throwable){
                when(error){
                    is IOException -> setResult(ResultState.NoInternetConnection())
                    is TimeoutException -> setResult(ResultState.TimeOut(R.string.timeout))
                    else -> setResult(ResultState.Error(R.string.unknown_error))
                }
            }
        }
    }

    private fun setResult(data: ResultState<AppVersionData>) {
        _appVersionResult.postValue(data)
    }
}