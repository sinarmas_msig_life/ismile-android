package co.id.ajsmsig.ismile.model

data class ProfileImagePath(var id: Int, var imagePath: String)