package co.id.ajsmsig.ismile.ui.candidate.statement

import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.db.dao.AgentCandidateDao
import co.id.ajsmsig.ismile.db.dao.QuestionnaireDao
import co.id.ajsmsig.ismile.db.dao.QuestionnaireDetailDao
import co.id.ajsmsig.ismile.model.api.DownloadAgencyContractRequest
import javax.inject.Inject

class StatementRepository @Inject constructor(
    private val dao: AgentCandidateDao,
    private val questionnaireDao: QuestionnaireDao,
    private val questionnaireDetailDao: QuestionnaireDetailDao,
    private val apiService: ApiService
) {

    fun updateQuestionnaireCompleted(registrationId: String, millis: String? ) = dao.updateQuestionnaireCompleted(registrationId,millis)
    fun updateAnswer(id : Long, registrationId: String, answer : Int) = questionnaireDao.updateAnswer(id, registrationId, answer)
    fun findAllSavedAnswer(registrationId: String) = dao.findAllSavedAnswer(registrationId)
    fun updateDetailAnswer(id : Long, counterId: Long, answer: String?, isActive : Boolean) = questionnaireDetailDao.update(id, counterId, answer, isActive)
    fun updateEthicCodeChecked(registrationId: String, isEthicCodeChecked : Int, millis : String?) = dao.updateEthicCode(registrationId, isEthicCodeChecked, millis)
    fun updateEmploymentContract(registrationId: String, isEmploymentContractChecked : Int, millis : String?) = dao.updateEmploymentContract(registrationId, isEmploymentContractChecked, millis)
    fun updateAgencyLetter(registrationId: String, isAgencyLetterChecked: Int, isStatementCompleted : Boolean, millis: String?) = dao.updateAgencyLetter(registrationId, isAgencyLetterChecked, isStatementCompleted, millis)
    fun downloadAgencyLetter(registrationId: String, birthPlace : String, appName : String, title : String, address : String ) = apiService.downloadAgencyContract(DownloadAgencyContractRequest(registrationId, birthPlace, appName, title, address))
    fun findDataForAgencyLetter(registrationId: String) = dao.findDataForAgencyLetter(registrationId)
}