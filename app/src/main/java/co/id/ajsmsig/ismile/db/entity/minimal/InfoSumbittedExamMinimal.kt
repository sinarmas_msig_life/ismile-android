package co.id.ajsmsig.ismile.db.entity.minimal

import androidx.room.ColumnInfo

data class InfoSumbittedExamMinimal(

    @ColumnInfo(name = "exam_city_name")
    var examCityName: String?,

    @ColumnInfo(name = "exam_place_name")
    var examPlaceName: String?, //id dari tempat ujian

    @ColumnInfo(name = "exam_selected_date")
    var examSelectedDate: String?, //berisi tanggal ujian yang dipilih calon agen

    @ColumnInfo(name = "exam_method_id")
    var examMethodId: Int?, //id dari paper, online

    @ColumnInfo(name = "exam_product_type_id")
    var examProductTypeId: Int?, //id dari unit link, konvensional

    @ColumnInfo(name = "exam_type_id")
    var examTypeId: Int?,//id dari pengajuan baru, perpanjangan, pindah perusahaan)

    @ColumnInfo(name = "number_rek")
    var numberRek: String?,

    @ColumnInfo(name = "banking")
    var banking: String?,

    @ColumnInfo(name = "nominal")
    var nominal: String?
)