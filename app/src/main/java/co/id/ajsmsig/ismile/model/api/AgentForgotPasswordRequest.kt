package co.id.ajsmsig.ismile.model.api

import com.google.gson.annotations.SerializedName

data class AgentForgotPasswordRequest(
    @SerializedName("userid")
    val userid: String,
    @SerializedName("phone_no")
    val phoneNo: String,
    @SerializedName("is_leader")
    val isLeader: Boolean
)