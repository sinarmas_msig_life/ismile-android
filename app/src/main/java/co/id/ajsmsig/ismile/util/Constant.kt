package co.id.ajsmsig.ismile.util

import co.id.ajsmsig.ismile.BuildConfig

const val LOVE = "Love"
const val HELLO = "Hello"
const val DATABASE_NAME = "ismile.db"
const val BASE_URL = BuildConfig.BASE_URL
const val TMDB_API_KEY = "6f9eb82919b6afb48a95af0c8c16dfac"

const val REQUEST_IMAGE_CAPTURE = 1

const val PREF_NAME = "billyon_data"
const val PREF_KEY_LOGIN = "isLogin"
const val PREF_KEY_LOGIN_ROLE_TYPE = "login-role-type"

const val PREF_KEY_AGENT_CODE = "agent-code"
const val PREF_KEY_AGENT_NAME = "agent-name"
const val PREF_KEY_AGENT_USER_ID = "agent-userid"
const val PREF_KEY_AGENT_TITLE = "agent-title"

const val PREF_KEY_CASHIER = "cashier"

const val DIALOG_MENU_ADD_ACTIVITY = 0
const val DIALOG_MENU_ADD_CUSTOMER = 1
const val DIALOG_MENU_ADD_AGENT_CANDIDATE = 0

const val MENU_MY_RECRUITMENT = 0

const val ALL_RECRUIT_REQUEST_CODE = 0
const val RECRUIT_SUCCESS_REQUEST_CODE=2
const val RECRUIT_FAILED_REQUEST_CODE=3
const val IMAGE_COMPRESSION_PERCENTAGE= 50

const val LOGIN_TYPE_NOT_SELECTED = 0
const val LOGIN_TYPE_AGENT = 1
const val LOGIN_TYPE_AGENT_CANDIDATE = 2
const val LOGIN_TYPE_LEADER_AGENT = 3

const val LAUNCH_AGENT_AGENT_HOME = 100
const val LAUNCH_AGENT_AGENT_LOGIN = 200
const val LAUNCH_CHOOSE_LOGIN_ROLE = 300
const val LAUNCH_AGENT_AGENT_CANDIDATE_LOGIN = 400
const val LAUNCH_AGENT_AGENT_CANDIDATE_HOME = 500
const val LAUNCH_LEADER_AGENT_LOGIN = 600
const val LAUNCH_LEADER_AGENT_HOME = 700
const val LAUNCH_SCREEN_UNDEFINED = 0
const val LAUNCH_SCREEN_UPDATE_APPS = 1

const val PREFF_KEY_AGENT_CANDIDATE_CODE="agent-candidate-code"

const val LAUNCH_APP_ID=90

const val NO = 0
const val YES = 1

const val REQUEST_CODE_DRAW = 101

const val TELEGRAM_CHANNEL_ID = "1008099125:AAFB_iNXFykirrTB05I03o0VfLk1mPxpx54"
const val TELEGRAM_CHATT_ID = "-1001336191234"

const val CONNECT_TIMEOUT = "CONNECT_TIMEOUT"
const val READ_TIMEOUT = "READ_TIMEOUT"
const val WRITE_TIMEOUT = "WRITE_TIMEOUT"

const val INITIAL_PAGE_INDEX = 0

const val DEFAULT_CONNECT_TIMEOUT: Long = 30
const val DEFAULT_READ_TIMEOUT: Long = 30
const val DEFAULT_WRITE_TIMEOUT: Long = 30

const val MENU_LEADER_REQUSTED = 1
const val MENU_LEADER_APPROVE = 2
const val MENU_LEADER_REJECTED = 3

const val FLAGS_APPS_SMILEGO = 8
const val FLAGS_PLATFORM = 1