package co.id.ajsmsig.ismile.ui.agent.login

import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.model.api.AgentLoginRequest
import javax.inject.Inject

class LoginRemoteDataSource @Inject constructor(private val apiService: ApiService) {

    fun login(username : String, password : String)  = apiService.login(AgentLoginRequest(username, password))
}