package co.id.ajsmsig.ismile.model.api

data class SubmitAgentCandidateRequest(
    val full_name : String,
    val bod : String,
    val id_card_no : String,
    val phone_no : String,
    val email : String,
    val title_id : Int,
    val marital_status : Int,
    val gender_id: Int,
    val bop_date : String,
    val msag_id : String,
    val file: String
)