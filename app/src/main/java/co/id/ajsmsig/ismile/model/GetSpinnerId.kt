package co.id.ajsmsig.ismile.model

data class GetSpinnerId(val error: Boolean,val genderId: Int?, val religionId: Int?, val cityName: String?)