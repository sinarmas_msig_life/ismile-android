package co.id.ajsmsig.ismile.base

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel : ViewModel() {

    var isLoading = ObservableBoolean()
    val mCompositeDisposable = CompositeDisposable()

    fun setIsLoading(setAsLoading: Boolean) = isLoading.set(setAsLoading)
    fun getIsLoading() = isLoading

    override fun onCleared() {
        mCompositeDisposable.dispose()
        super.onCleared()
    }

}