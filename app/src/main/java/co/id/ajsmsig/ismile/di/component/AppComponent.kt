package co.id.ajsmsig.ismile.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import co.id.ajsmsig.ismile.AppController
import co.id.ajsmsig.ismile.di.module.*
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class,
    NetworkModule::class,
    RepositoryModule::class,
    AndroidInjectionModule::class,
    ActivityBuilder::class,
    FragmentModule::class,
    DatabaseModule::class,
    ViewModelModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: AppController): Builder

        fun build(): AppComponent
    }

    fun inject(app: AppController)
}
