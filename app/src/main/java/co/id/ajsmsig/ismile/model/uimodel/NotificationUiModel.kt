package co.id.ajsmsig.ismile.model.uimodel

import co.id.ajsmsig.ismile.model.NotificationModel

data class NotificationUiModel(val error: Boolean, val message: String, val data: List<NotificationModel>)