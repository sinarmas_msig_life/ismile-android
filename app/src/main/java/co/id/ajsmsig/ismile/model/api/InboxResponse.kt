package co.id.ajsmsig.ismile.model.api


import com.google.gson.annotations.SerializedName

data class InboxResponse(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("error")
    val error: Boolean,
    @SerializedName("message")
    val message: String
) {
    data class Data(
        @SerializedName("created_date")
        val createdDate: String,
        @SerializedName("flag_status")
        val flagStatus: String,
        @SerializedName("inbox_id")
        val inboxId: Int,
        @SerializedName("message")
        val message: String,
        @SerializedName("parameter")
        val parameter: Parameter?,
        @SerializedName("title")
        val title: String
    ) {
        data class Parameter(
            @SerializedName("next_action_menu_id")
            val nextActionMenuId: Int
        )
    }
}