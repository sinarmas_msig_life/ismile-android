package co.id.ajsmsig.ismile.ui.agent.addagentcandidate


import android.Manifest
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.view.View
import android.widget.AdapterView
import android.widget.DatePicker
import android.widget.Spinner
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.BuildConfig
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentAddAgentCandidateBinding
import co.id.ajsmsig.ismile.model.AgentCandidateGender
import co.id.ajsmsig.ismile.model.AgentMaritalStatus
import co.id.ajsmsig.ismile.model.AgentTitle
import co.id.ajsmsig.ismile.util.IMAGE_COMPRESSION_PERCENTAGE
import co.id.ajsmsig.ismile.util.REQUEST_IMAGE_CAPTURE
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class AddAgentCandidateFragment : BaseFragment<FragmentAddAgentCandidateBinding, AddAgentCandidateViewModel>() {

    override fun getLayoutResourceId() = R.layout.fragment_add_agent_candidate
    override fun getViewModelClass() = AddAgentCandidateViewModel::class.java
    private var mCurrentPhotoPath = ""
    private var progressDialog : ProgressDialog? = null

    private val calendar = Calendar.getInstance()
    private var seventeenYearsAgo=calendar.get(Calendar.YEAR)
    private var yearBirth = seventeenYearsAgo-17
    private var monthBirth = calendar.get(Calendar.MONTH)
    private var dayOfMonthBirth = calendar.get(Calendar.DAY_OF_MONTH)
    private var yearBop = calendar.get(Calendar.YEAR)
    private var monthBop = calendar.get(Calendar.MONTH)
    private var dayOfMonthBop = calendar.get(Calendar.DAY_OF_MONTH)

    private lateinit var adapter: AgentTitlesSpinnerAdapter
    private lateinit var maritalStatusSpinnerAdapter: AgentMaritalStatusSpinnerAdapter

    private lateinit var genderAdapter: AgentCandidateGenderSpinnerAdapter

    private lateinit var spinner: Spinner

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getActivity()!!.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)

        val vm = getViewModel()

        getDataBinding().fragment = this
        getDataBinding().vm = vm

        getViewModel().getAgentTitle()

        initSpinnerPosition()
        initSpinnerGender()
        initMarriageStatusSpinner()

        setDefaultBopDate()

        getViewModel().identityCardImagePath.observe(this, androidx.lifecycle.Observer {
            it?.let {
                setIdentityCardBitmap(it)
            }
        })

        getViewModel().mCurrentPath.observe(this, androidx.lifecycle.Observer {
            it?.let {
                mCurrentPhotoPath=it
            }
        })
        getViewModel().agentTitle.observe(this, androidx.lifecycle.Observer {
            it?.let {
                if (!it.error) {
                    refreshSpinner(it.titles)
                }
            }
        })

        getViewModel().isUserInputValid.observe(this, androidx.lifecycle.Observer {
            it?.let {
                if (it.error) {
                    snackbar(it.message!!)
                } else {
                    submit()
                }
            }
        })

        getViewModel().isSubmitDataSuccess.observe(this, androidx.lifecycle.Observer {
            it?.let {
                if (it.error) {
                    displaySubmitResultFailed(getString(R.string.submit_failed), it.message)
                } else {
                    displaySubmitResultSuccess(getString(R.string.submit_success), "Data calon agen berhasil disubmit")
                }
            }
        })

        getViewModel().displaySubmitDataOnProgress.observe(this, androidx.lifecycle.Observer {
            it?.let {
                if (it.isFinished) {
                    hideProgressDialog()
                } else {
                    displayProgressDialog(it.message!!)
                }
            }
        })

    }

    private fun initSpinnerPosition() {
        adapter = AgentTitlesSpinnerAdapter(activity!!, mutableListOf())
        val spinner = getDataBinding().contentAgentData.spinnerPosition

        spinner.adapter = adapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, p3: Long) {
                val titles = getViewModel().agentTitles.get()!!
                val selectedTitleId = titles[position].titleId
                getViewModel().saveSelectedTitleId(selectedTitleId)
            }

        }
    }

    private fun initMarriageStatusSpinner() {

        val maritalStatuses = mutableListOf<AgentMaritalStatus>()
        maritalStatuses.add(AgentMaritalStatus(1, "Menikah"))
        maritalStatuses.add(AgentMaritalStatus(2, "Lajang"))
        maritalStatuses.add(AgentMaritalStatus(3, "Cerai"))

        maritalStatusSpinnerAdapter = AgentMaritalStatusSpinnerAdapter(activity!!, maritalStatuses)

        val spinner = getDataBinding().contentAgentData.spinnerMaritalStatus

        spinner.adapter = maritalStatusSpinnerAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, p3: Long) {
                val maritaStatusId = maritalStatuses[position].id
                getViewModel().saveSelectedMaritalStatusId(maritaStatusId)
            }

        }
    }

    private fun refreshSpinner(titles: List<AgentTitle>) {
        adapter.refreshData(titles)
    }


    fun onSendButtonPressed() {
        val isProfileCompanyChecked = getDataBinding().contentAgentConfirmation.switchCompanyProfile.isChecked
        val isCompensationChecked = getDataBinding().contentAgentConfirmation.switchCompensation.isChecked
        val isBusinessOportunityChecked = getDataBinding().contentAgentConfirmation.switchBusinessOpportunity.isChecked

        getViewModel().validateUserInput(isProfileCompanyChecked, isCompensationChecked, isBusinessOportunityChecked)
    }

    fun onDateOfBirthPressed() {

        val dialog = DatePickerDialog(activity!!, datepickerListener, yearBirth, monthBirth, dayOfMonthBirth)
        val millis17YearsAgo = getMillis17YearsAgo()
        dialog.datePicker.maxDate = millis17YearsAgo

        dialog.show()
    }

    fun onBopDatePressed() {

        val dialog = DatePickerDialog(activity!!, bopDatepickerListener, yearBop, monthBop, dayOfMonthBop)
        val millis17YearsAgo = getMillis17YearsAgo()
        dialog.datePicker.minDate = millis17YearsAgo
        dialog.datePicker.maxDate = calendar.timeInMillis

        dialog.show()
    }

    private val datepickerListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
            val day = String.format("%02d", dayOfMonth)
            val month = String.format("%02d", monthOfYear + 1) //Month is 0-based
            val selectedDate = "$day-$month-$year"

            setDateOfBirthSelected(dayOfMonth,monthOfYear,year)

            getDataBinding().contentAgentData.etBod.setText(selectedDate)

            getViewModel().saveSelectedBodDate("$year-$month-$day")
        }

    }

    private val bopDatepickerListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
            val day = String.format("%02d", dayOfMonth)
            val month = String.format("%02d", monthOfYear + 1)//Month is 0-based
            val selectedDate = "$day-$month-$year"

            setBopSelected( dayOfMonthBop,monthOfYear,year)

            getDataBinding().contentAgentData.etBop.setText(selectedDate)

            getViewModel().saveSelectedBopDate("$year-$month-$day")
        }
    }

    fun onIdentityImagePressed() {
        val permissionAllowed = checkPermission()
        if (permissionAllowed) takePicture() else requestPermission()
    }

    private fun requestPermission() {
        Dexter.withActivity(activity)
            .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // Check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        takePicture()
                    }

                    // Check for permanent denial of any permission
                    if (report.isAnyPermissionPermanentlyDenied) {
                        // permission is denied permenantly, navigate user to app settings
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }

            })
            .withErrorListener {
                snackbar("Error occurred! $it")
            }
            .onSameThread()
            .check()
    }

    private fun takePicture() {

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file = createFile()

        val uri = FileProvider.getUriForFile(activity!!, BuildConfig.APPLICATION_ID, file)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
    }

    private fun checkPermission(): Boolean {
        return (ContextCompat.checkSelfPermission(
            activity!!,
            android.Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
                &&
                ContextCompat.checkSelfPermission(
                    activity!!,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED)
    }

    /**
     * After the picture has been captured,
     * the result will received on onActivityResult method. There, we can get the data, in Bitmap form.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_CANCELED){
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

                compressImage(mCurrentPhotoPath)
                //Save the bitmap to viewmodel, so it will be restored when orientation changed.
                getViewModel().saveIdentityCardImagePath(mCurrentPhotoPath)

                setIdentityCardBitmap(mCurrentPhotoPath)

            }
        }
    }

    @Throws(IOException::class)
    private fun createFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMddHHmmss").format(Date())
        val imageFile = "JPEG_" + timeStamp + "_"
        val storageDir = activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(imageFile, ".jpg", storageDir)

        mCurrentPhotoPath = image.absolutePath

        getViewModel().saveMcurrentPath(image.absolutePath)

        return image
    }

    private fun setIdentityCardBitmap(imagePath: String) {
        val bitmap = BitmapFactory.decodeFile(imagePath)
        getDataBinding().contentAttachAgentDocument.ivDocumentImage.setImageBitmap(bitmap)
    }

    /**
     * Get millis 17 years ago before now
     */
    private fun getMillis17YearsAgo(): Long {
        val calendar = Calendar.getInstance()

        val currentYear = calendar.get(Calendar.YEAR)
        val currentMonth = calendar.get(Calendar.MONTH)
        val currentDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)

        val seventeenYearsAgo = currentYear - 17

        calendar.set(Calendar.DAY_OF_MONTH, currentDayOfMonth)
        calendar.set(Calendar.MONTH, currentMonth)
        calendar.set(Calendar.YEAR, seventeenYearsAgo)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)

        return calendar.timeInMillis
    }

    private fun setDefaultBopDate() {
        val calendar = Calendar.getInstance()

        val year = calendar.get(Calendar.YEAR)
        val monthOfYear = calendar.get(Calendar.MONTH)
        val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)

        val day = String.format("%02d", dayOfMonth)
        val month = String.format("%02d", monthOfYear + 1)//Month is 0-based
        val currentDate = "$day-$month-$year"

        getDataBinding().vm?.bopDate?.set(currentDate)

        getViewModel().saveSelectedBopDate("$year-$month-$day")
    }

    private fun displayProgressDialog(title : String?) {
        progressDialog = ProgressDialog(activity)
        progressDialog?.isIndeterminate = true
        progressDialog?.setTitle(title)
        progressDialog?.setMessage(getString(R.string.please_wait))
        progressDialog?.setCancelable(false)
        progressDialog?.show()
    }

    private fun hideProgressDialog() {
            progressDialog?.dismiss()
    }

    private fun displaySubmitResultFailed(title : String, message : String) {
        alertDialog(title, message)
    }

    private fun displaySubmitResultSuccess(title : String, message : String) {
        alertDialog(title, message)
        findNavController().popBackStack()
    }

    private fun submit() {
        val fileInBase64Format = convertToBase64(mCurrentPhotoPath)
        getViewModel().validateAllSubmitParams(fileInBase64Format)
    }

    private fun setDateOfBirthSelected (dayOfMonth: Int, monthOfYear: Int, year: Int){
        yearBirth = year
        monthBirth = monthOfYear
        dayOfMonthBirth = dayOfMonth
    }

    private fun setBopSelected(dayOfMonth: Int, monthOfYear: Int, year: Int){
        yearBop = year
        monthBop = monthOfYear
        dayOfMonthBop = dayOfMonth
    }

    private fun compressImage(imagePath: String) {
        var bitmap = BitmapFactory.decodeFile(imagePath)
        val file = File(imagePath)
        val out = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.JPEG, IMAGE_COMPRESSION_PERCENTAGE, out)
        out.flush()
        out.close()
    }

    private fun convertToBase64(imagePath: String): String {
        val bitmap = BitmapFactory.decodeFile(imagePath)
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, IMAGE_COMPRESSION_PERCENTAGE, outputStream)
        val byteArray = outputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }

    private fun initSpinnerGender() {

        val gender = mutableListOf<AgentCandidateGender>()
        gender.add(AgentCandidateGender(0, "Perempuan"))
        gender.add(AgentCandidateGender(1, "Laki-Laki"))

        genderAdapter = AgentCandidateGenderSpinnerAdapter(activity!!, gender)
        spinner = getDataBinding().contentAgentData.spinnerGender

        spinner.adapter = genderAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, p3: Long) {
                val genderId = gender[position].id
                getViewModel().saveSelectedGenderId(genderId)
            }
        }
    }
}
