package co.id.ajsmsig.ismile.model

data class AgentMaritalStatus(val id : Int, val maritalStatus : String)