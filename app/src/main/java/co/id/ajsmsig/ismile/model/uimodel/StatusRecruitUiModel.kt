package co.id.ajsmsig.ismile.model.uimodel

import co.id.ajsmsig.ismile.model.StatusRecruit

data class StatusRecruitUiModel (val error : Boolean, val message : String, val recruits : List<StatusRecruit>)