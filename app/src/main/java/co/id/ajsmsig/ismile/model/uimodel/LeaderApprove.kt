package co.id.ajsmsig.ismile.model.uimodel

class LeaderApprove (
    val agentCandidateRegistrationId: String,
    val candidateName: String?,
    val latestStatus: String? ,
    val profpicImagePath: String?,
    val statusDate: String?,
    val submitDate: String?
)