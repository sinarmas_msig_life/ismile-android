package co.id.ajsmsig.ismile.ui.leader.agentleader

import android.app.AlertDialog
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentAgentLeaderBinding
import co.id.ajsmsig.ismile.model.AgentMenu
import co.id.ajsmsig.ismile.ui.agent.home.HomeFragmentDirections
import co.id.ajsmsig.ismile.ui.leader.agentleaderapprove.AgentLeaderCheckedApprovalFragment
import co.id.ajsmsig.ismile.ui.leader.agentleaderfailed.AgentLeaderCheckedFailedFragment
import co.id.ajsmsig.ismile.ui.leader.agentleadersucces.AgentLeaderCheckedSuccesFragment
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import com.google.firebase.iid.FirebaseInstanceId
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class AgentLeaderFragment : BaseFragment <FragmentAgentLeaderBinding,AgentLeaderViewModel>(), AgentLeaderMenuRecyclerAdapter.onMenuPressedListener{

    override fun getLayoutResourceId()= R.layout.fragment_agent_leader

    override fun getViewModelClass() = AgentLeaderViewModel::class.java

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val vm = getViewModel()
        getDataBinding().vm =vm
        getDataBinding().fragment=this

        setupViewpager()
        setupTabLayout()

        getViewModel().displayAgenLeaderStatus()
    }

    private fun setupViewpager() {
        val adapter = ViewpagerAdapter(childFragmentManager)
        adapter.addFragment(AgentLeaderCheckedApprovalFragment(), "Requested")
        adapter.addFragment(AgentLeaderCheckedSuccesFragment(), "Approved")
        adapter.addFragment(AgentLeaderCheckedFailedFragment(), "Rejected")
        getDataBinding().viewpager.adapter = adapter

    }

    private fun setupTabLayout() {
        getDataBinding().tabLayout.setupWithViewPager(getDataBinding().viewpager)
    }

    class ViewpagerAdapter constructor(private val fragmentManager : FragmentManager) : FragmentPagerAdapter(fragmentManager) {

        private val fragmentList = mutableListOf<Fragment>()
        private val fragmentTitle = mutableListOf<String>()

        override fun getItem(position: Int) = fragmentList[position]
        override fun getCount() = fragmentList.size
        override fun getPageTitle(position: Int) = fragmentTitle[position]

        fun addFragment(fragment : Fragment, title: String) {
            fragmentList.add(fragment)
            fragmentTitle.add(title)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_home, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        when(id) {
            R.id.action_inbox -> {
                launchNotificationFragment()
            }
            R.id.action_logout -> {
                logout()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onMenuSelected(menu: AgentMenu, position: Int) {

    }

    private fun logout() {
        displayAlertLogout()

    }
    private fun displayAlertLogout (){
        val title = getString(R.string.confirm_logut)
        AlertDialog.Builder(activity)
            .setTitle(title)
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok) { dialogInterface, _ ->
                launchChooseLoginRole()
                clearAllSavedUserData()
                deleteFcmInstance()
                dialogInterface.dismiss()
            }.setNegativeButton(android.R.string.no){dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            .show()
    }

    private fun clearAllSavedUserData() {
        getViewModel().clearAllSavedUserData()
    }

    private fun launchChooseLoginRole() {
        val action = AgentLeaderFragmentDirections.actionLaunchLoginRoleFragment()
        findNavController().navigate(action)
    }

    private fun deleteFcmInstance() {
        compositeDisposable += Observable.fromCallable {
            FirebaseInstanceId.getInstance().deleteInstanceId() }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    private fun launchNotificationFragment() {
        val action = AgentLeaderFragmentDirections.actionLaunchInboxFragment()
        findNavController().navigate(action)
    }

}
