package co.id.ajsmsig.ismile.model.api


import com.google.gson.annotations.SerializedName

data class CurrentStatusResponse(
    @SerializedName("data")
    val data: Data,
    @SerializedName("error")
    val error: Boolean,
    @SerializedName("message")
    val message: String
) {
    data class Data(
        @SerializedName("is_approved")
        val isApproved: Boolean,
        @SerializedName("is_submitted")
        val submitted: Boolean,
        @SerializedName("data_submit_date")
        val dataSubmitDate: String,
        @SerializedName("dissup_verification_date")
        val dissupVerificationDate: String
    )
}