package co.id.ajsmsig.ismile.ui.candidate.profileagentcandidate

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.fragment.app.FragmentActivity
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.databinding.CustomSpinnerAgentCandidateReligionBinding
import co.id.ajsmsig.ismile.model.AgentCandidateReligion

class ProfileAgentCandidateReligionSpinnerAdapter(val context: FragmentActivity, private var agentCandidateReligion: List<AgentCandidateReligion>) : BaseAdapter() {

    val inflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemRowHolder

        if (convertView == null) {
            val binding = CustomSpinnerAgentCandidateReligionBinding.inflate(inflater, parent, false)
            vh = ItemRowHolder(binding)
            view = binding.root
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemRowHolder
        }


        vh.bind(R.drawable.ic_action_employee, agentCandidateReligion[position].religion)

        return view

    }

    private class ItemRowHolder(private val binding: CustomSpinnerAgentCandidateReligionBinding) {

        fun bind(imageResourceId: Int, label: String) {
            binding.ivAgentCandidateReligion.setImageResource(imageResourceId)
            binding.tvAgentCandidateReligion.text = label
            binding.executePendingBindings()
        }
    }

    override fun getItem(p0: Int): Any? = null
    override fun getItemId(p0: Int): Long = 0
    override fun getCount(): Int = agentCandidateReligion.size

    fun refreshData(agentCandidateReligion: List<AgentCandidateReligion>) {
        this.agentCandidateReligion = agentCandidateReligion
        notifyDataSetChanged()
    }


}