package co.id.ajsmsig.ismile.ui.agent.forgotpasswordagent

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.di.NetManager
import co.id.ajsmsig.ismile.model.api.AgentForgotPasswordRequest
import co.id.ajsmsig.ismile.model.api.AgentForgotPasswordResponse
import io.reactivex.Observable
import javax.inject.Inject

class ForgotPasswordAgentRepository @Inject constructor(private val editor: SharedPreferences.Editor, private val preferences: SharedPreferences, private val netManager: NetManager, private val apiService: ApiService) {

    fun forgotPasswordAgent(userid:String,phone_no:String,  isLeader:Boolean):Observable<AgentForgotPasswordResponse>{
        if (netManager.isConnectedToInternet){
            return apiService.agentForgotPassword(AgentForgotPasswordRequest(userid,phone_no,isLeader))
        }
        return Observable.just(AgentForgotPasswordResponse("Koneksi tidak berhasil. Mohon periksa kembali koneksi internet",true,""))
    }

}