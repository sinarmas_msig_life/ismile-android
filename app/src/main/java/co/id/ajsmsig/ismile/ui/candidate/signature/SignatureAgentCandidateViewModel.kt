package co.id.ajsmsig.ismile.ui.candidate.signature

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.db.entity.minimal.SigantureAgentCandidateMinimal
import co.id.ajsmsig.ismile.model.MessageInfo
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SignatureAgentCandidateViewModel @Inject constructor(private val repository: SignatureAgentCandidateRepository) : BaseViewModel() {

    var isSignatureChecked = ObservableBoolean(false)

    private val _isSuccesSubmit = MutableLiveData<Boolean>()
    val isSuccesSubmit: LiveData<Boolean>
        get() = _isSuccesSubmit

    private val _isUserInputValid = MutableLiveData<MessageInfo>()
    val isUserInputValid: LiveData<MessageInfo>
        get() = _isUserInputValid

    private val _isSignaturePath = MutableLiveData <String>()
    val isSignaturePath : LiveData <String>
        get() =_isSignaturePath

    fun getInfoSignatureAgentCandidate () {
        mCompositeDisposable += repository.getSignAtureAgentCandidate()
            .subscribeOn( Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {data-> infoSignatureSuccess(data)},
                {infoSignatureFailed()})
    }

    fun submitSignature(signatureDate: String) {
        mCompositeDisposable += Completable.fromCallable { repository.updateSignatureAgentCandidate(isSignatureChecked.get(), _isSignaturePath.value!!, signatureDate) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{ sigantureSuccesSave() }
    }

    fun validateUserInput() {
        if (!isSignatureCheckedValid()) {
            return
        }
        if (!isImagePathValid()) {
            return
        }
        setInputError(false,null)
    }

    private fun isSignatureCheckedValid(): Boolean {
        if (!isSignatureChecked.get()){
            setInputError(true,"Harap menyetujui pernyatan terlebih dahulu")
            return false
        }
        return true
    }

    private fun isImagePathValid(): Boolean {
        if (_isSignaturePath.value.isNullOrEmpty()) {
            setInputError(true, "Harap tanda tangan terlebih dahulu")
            return false
        }
        return true
    }

    private fun infoSignatureSuccess(data: SigantureAgentCandidateMinimal) {
        if(!data.signatureImgPath.isNullOrEmpty()){
            _isSignaturePath.value=data.signatureImgPath
        }
    }

    private fun infoSignatureFailed() {

    }

    fun saveSigantureImagePath(imagePath: String) {
        _isSignaturePath.value = imagePath
    }

    private fun sigantureSuccesSave() {
        _isSuccesSubmit.value = true
    }


    private fun setInputError(error: Boolean, message: String?) {
        _isUserInputValid.value = MessageInfo(error, message)
    }

}