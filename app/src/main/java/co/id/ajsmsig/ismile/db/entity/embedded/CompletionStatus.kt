package co.id.ajsmsig.ismile.db.entity.embedded

import androidx.room.ColumnInfo
import androidx.room.TypeConverters
import co.id.ajsmsig.ismile.util.Converters

data class CompletionStatus(

        //Lengkapi data diri
        @ColumnInfo(name = "personal_data_date")
        var personalDataDate: String?,

        @ColumnInfo(name = "work_experience_date")
        var workExperienceDate: String?,

        @ColumnInfo(name = "education_date")
        var educationDate: String?,

        //Pernyataan
        @ColumnInfo(name = "questionnaire_date")
        var questionnaireDate: String?,

        @ColumnInfo(name = "ethic_code_date")
        var ethicCodeDate: String?,

        @ColumnInfo(name = "employment_contract_date")
        var employmentContractDate: String?,

        @ColumnInfo(name = "agency_letter_date")
        var agencyLetterDate: String?,

        //Tanda tangan
        @ColumnInfo(name = "signature_date")
        var signatureDate: String?,

        //Exam
        @ColumnInfo(name = "exam_date")
        var examCompletedDate: String?, //exam_input_date, berisi waktu kapan input ujian + submit selesai dilakukan

        @ColumnInfo(name = "exam_bsb_date")
        var examBsbSubmitDate: String?,

        @TypeConverters(Converters::class)
        @ColumnInfo(name = "exam_complete")
        var examCompleted: Boolean,

        @TypeConverters(Converters::class)
        @ColumnInfo(name = "personal_data_complete")
        var personalDataComplete: Boolean,

        @TypeConverters(Converters::class)
        @ColumnInfo(name = "signature_complete")
        var signatureComplete: Boolean,

        @TypeConverters(Converters::class)
        @ColumnInfo(name = "statement_complete")
        var statementComplete: Boolean,

        @TypeConverters(Converters::class)
        @ColumnInfo(name = "license_complete")
        var licenseComplete: Boolean


)