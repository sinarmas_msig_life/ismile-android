package co.id.ajsmsig.ismile.ui.candidate.statement

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.RadioButton
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentStatementBinding
import co.id.ajsmsig.ismile.ui.candidate.profileagentcandidate.ProfileAgentCandidateFragmentArgs
import co.id.ajsmsig.ismile.util.DateUtils
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

class StatementFragment : BaseFragment<FragmentStatementBinding, StatementViewModel>() {

    override fun getLayoutResourceId() = R.layout.fragment_statement

    override fun getViewModelClass() = StatementViewModel::class.java
    private var registrationId = ""
    private var name = ""
    var isSubmitted : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        registrationId = StatementFragmentArgs.fromBundle(arguments!!).registrationId
        name = StatementFragmentArgs.fromBundle(arguments!!).name
        isSubmitted = ProfileAgentCandidateFragmentArgs.fromBundle(arguments!!).isSubmitted

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val vm = getViewModel()

        getDataBinding().vm = vm
        getDataBinding().fragment = this

        getViewModel().findAllSavedData(registrationId)

        getViewModel().inputAreValid.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.error) {
                    Snackbar.make(getDataBinding().root, it.message.toString(), Snackbar.LENGTH_SHORT).show()
                } else {
                    launchAgencyLetterFragment()
                }
            }
        })

        getViewModel().downloadAgencyLetterPdfSuccess.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.error) {
                    Snackbar.make(getDataBinding().root, "Download pdf gagal ${it.message}", Snackbar.LENGTH_LONG).show()
                } else {
                    launchPdfViewer(it.pdfFilePath)
                }
            }
        })

        val fileName = "Kontrak keagenan $name - $registrationId"
        getDataBinding().contentEmploymentContract.tvFilename.text = fileName
    }

    fun onNextButtonPressed() {
        val isEthicCodeChecked = getDataBinding().contentEthicCode.cbEthicCode.isChecked
        val isEmploymentContractChecked = getDataBinding().contentEmploymentContract.cbEmploymentContract.isChecked

        val currentTime = DateUtils.getCurrentTime()

        val firstRadioGroup = getDataBinding().contentQuestionnaire.firstRadioGroup
        val selectedFirstGroupRadioButtonID = firstRadioGroup.checkedRadioButtonId
        val selectedFirstGroupRadioButton = firstRadioGroup.findViewById<RadioButton>(selectedFirstGroupRadioButtonID)
        val selectedFirstGroupRadioButtonIndex = firstRadioGroup.indexOfChild(selectedFirstGroupRadioButton)

        val secondRadioGroup = getDataBinding().contentQuestionnaire.secondRadioGroup
        val selectedSecondGroupRadioButtonID = secondRadioGroup.checkedRadioButtonId
        val selectedSecondGroupRadioButton =
            secondRadioGroup.findViewById<RadioButton>(selectedSecondGroupRadioButtonID)
        val selectedSecondGroupRadioButtonIndex = secondRadioGroup.indexOfChild(selectedSecondGroupRadioButton)

        val thirdRadioGroup = getDataBinding().contentQuestionnaire.thirdRadioGroup
        val selectedThirdGroupRadioButtonID = thirdRadioGroup.checkedRadioButtonId
        val selectedThirdGroupRadioButton = thirdRadioGroup.findViewById<RadioButton>(selectedThirdGroupRadioButtonID)
        val selectedthirdGroupRadioButtonIndex = thirdRadioGroup.indexOfChild(selectedThirdGroupRadioButton)

        getViewModel().validateAllAnswer(registrationId, selectedFirstGroupRadioButtonIndex, selectedSecondGroupRadioButtonIndex, selectedthirdGroupRadioButtonIndex, isEthicCodeChecked, isEmploymentContractChecked, currentTime)
    }

    private fun launchAgencyLetterFragment() {
        val action = StatementFragmentDirections.actionLaunchAgencyLetterFragment(registrationId)
        action.isSubmitted = isSubmitted
        findNavController().navigate(action)
    }

    fun onButtonDownloadAgencyLetterPressed() {
        requestStoragePermission(registrationId)
    }

    private fun requestStoragePermission(registrationId : String) {
            Dexter.withActivity(activity)
                    .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(object : MultiplePermissionsListener {

                        override fun onPermissionsChecked(report: MultiplePermissionsReport) {

                            // check if all permissions are granted
                            if (report.areAllPermissionsGranted()) {
                                getViewModel().fetchDataForAgencyLetter(registrationId)
                            }

                            // check for permanent denial of any permission

                            if (report.isAnyPermissionPermanentlyDenied) {
                                // permission is denied permenantly, navigate user to app settings
                                showSettingsDialog()
                            }

                        }

                        override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                            token.continuePermissionRequest()
                        }

                    })
                    .withErrorListener {
                        Snackbar.make(getDataBinding().root, "Gagal mendapatkan akses ke storage. $it", Snackbar.LENGTH_LONG).show()
                    }
                    .onSameThread()
                    .check()
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(getString(R.string.permission_title))
        builder.setMessage(getString(R.string.storage_permission))
        builder.setPositiveButton(R.string.open_setting) { dialog, _ ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }
        builder.show()

    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", activity?.packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    private fun launchPdfViewer(fileName: String?) {
        val action = StatementFragmentDirections.actionLaunchPdfViewer(fileName!!)
        findNavController().navigate(action)
    }

}
