package co.id.ajsmsig.ismile

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import co.id.ajsmsig.ismile.di.component.DaggerAppComponent
import co.id.ajsmsig.ismile.util.DebugTree
import co.id.ajsmsig.ismile.util.ReleaseTree
import dagger.android.HasAndroidInjector
import timber.log.Timber
import javax.inject.Inject

class AppController : Application(), HasAndroidInjector {
    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this)

        initTimber()

    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        } else {
            Timber.plant(ReleaseTree())
        }
    }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector
}
