package co.id.ajsmsig.ismile.ui.agent.recruitsuccess

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.SingleLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.common.ResultState
import co.id.ajsmsig.ismile.model.StatusRecruit
import co.id.ajsmsig.ismile.model.api.StatusRecruitResponse
import co.id.ajsmsig.ismile.model.uimodel.StatusRecruitUiModel
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class RecruitSuccessViewModel @Inject constructor(private val repository: RecruitSuccesRepository) : BaseViewModel() {

    private val _recruits = SingleLiveData<ResultState<List<StatusRecruit>>>()
    val recruits: LiveData<ResultState<List<StatusRecruit>>>
        get() = _recruits

    fun getAllRecruitSuccess(requestCode: Int) {
        setIsLoading(true)
        viewModelScope.launch (Dispatchers.IO){
            try {
                val result = repository.getRecruitSucces(requestCode)
                setIsLoading(false)
                if (result.error){
                    setResultRecruits(ResultState.Error(R.string.unknown_error))
                    return@launch
                }
                if (result.data.success.isEmpty()){
                    setResultRecruits(ResultState.NoData())
                    return@launch
                }
                val data  = transformData(result.data.success)
                setResultRecruits(ResultState.HasData(data))
            }catch (e: Throwable){
                setIsLoading(false)
                when (e) {
                    is IOException -> setResultRecruits(ResultState.NoInternetConnection())
                    is TimeoutException -> setResultRecruits(ResultState.TimeOut(R.string.timeout))
                    else -> setResultRecruits(ResultState.Error(R.string.unknown_error))
                }
            }
        }
    }

    private fun transformData (data: List<StatusRecruitResponse.Data.Success>) : List<StatusRecruit> {
        val recruits = mutableListOf<StatusRecruit>()
        for (i in data) {
            recruits.add(
                StatusRecruit(
                    i.agentCandidateRegistrationId,
                    i.candidateName,
                    i.latestStatus,
                    i.profpicImagePath,
                    i.statusDate,
                    i.submitDate
                )
            )
        }
        return recruits
    }

    private fun setResultRecruits(data: ResultState<List<StatusRecruit>>){
        _recruits.sendActionOnBackground(data)
    }
}