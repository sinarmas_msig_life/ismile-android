package co.id.ajsmsig.ismile.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import co.id.ajsmsig.ismile.AppController
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.util.PREF_NAME
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    fun provideContext(app: AppController) : Context = app

    @Provides
    @Singleton
    fun provideApplications(app : AppController) : Application = app

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit) = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun providesPreference(app : AppController) : SharedPreferences  = app.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun providesSharedPreference(sharedPreferences: SharedPreferences) : SharedPreferences.Editor = sharedPreferences.edit()

}