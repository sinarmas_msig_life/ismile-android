package co.id.ajsmsig.ismile.ui.agent.recruitsuccess


import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.common.ResultState
import co.id.ajsmsig.ismile.databinding.FragmentRecruitSuccessBinding
import co.id.ajsmsig.ismile.model.StatusRecruit
import co.id.ajsmsig.ismile.ui.agent.home.HomeFragmentDirections
import co.id.ajsmsig.ismile.util.RECRUIT_SUCCESS_REQUEST_CODE

class RecruitSuccessFragment : BaseFragment<FragmentRecruitSuccessBinding, RecruitSuccessViewModel>(), RecruitmentSuccessRecyclerAdapter.OnRecruitmentStatusPressedListener {

    override fun getLayoutResourceId() = R.layout.fragment_recruit_success

    override fun getViewModelClass() = RecruitSuccessViewModel::class.java

    private val adapter = RecruitmentSuccessRecyclerAdapter(listOf(), this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        getViewModel().getAllRecruitSuccess(RECRUIT_SUCCESS_REQUEST_CODE)
        getViewModel().recruits.observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it){
                    is ResultState.Error -> setResultRecruits(emptyList(), false, getString(R.string.unknown_error), View.VISIBLE)
                    is ResultState.TimeOut -> setResultRecruits(emptyList(), false, getString(R.string.timeout), View.VISIBLE)
                    is ResultState.NoData -> setResultRecruits(emptyList(), true, getString(R.string.no_data_agent), View.VISIBLE)
                    is ResultState.NoInternetConnection -> setResultRecruits(emptyList(), false, getString(R.string.no_internet_connection), View.VISIBLE)
                    is ResultState.HasData ->setResultRecruits(it.data, true, "", View.GONE)
                    else -> setResultRecruits(emptyList(), false, getString(R.string.unknown_error), View.VISIBLE)
                }
            }
        })

        binding.swipeRecruitSucces.setOnRefreshListener {
            getViewModel().getAllRecruitSuccess(RECRUIT_SUCCESS_REQUEST_CODE)
        }
    }

    private fun setResultRecruits(data: List<StatusRecruit>, isRefresh: Boolean, message: String, visibility: Int) {
        binding.swipeRecruitSucces.isRefreshing = isRefresh
        binding.tvMessageRecruitSuccess.text = message
        binding.tvMessageRecruitSuccess.visibility = visibility
        refreshData(data)
    }

    override fun onRecruitSuccessPressed(candidateAgent: StatusRecruit, position: Int) {
        launchPreviewAgentCandidate(candidateAgent.agentCandidateRegistrationId)
    }

    private fun initRecyclerView() {

        val layoutManager = LinearLayoutManager(activity)
        val dividerItemDecoration = DividerItemDecoration(getDataBinding().recyclerView.context, layoutManager.orientation)

        getDataBinding().recyclerView.layoutManager = layoutManager
        getDataBinding().recyclerView.addItemDecoration(dividerItemDecoration)
        getDataBinding().recyclerView.adapter = adapter
    }

    private fun refreshData(recruits : List<StatusRecruit>) {
        getDataBinding().swipeRecruitSucces.isRefreshing = false
        adapter.refreshData(recruits)
    }

    private fun launchPreviewAgentCandidate(registrationId : String) {
        val action = HomeFragmentDirections.actionHomeFragmentToPreviewCandidateAgentFragment(registrationId)
        findNavController().navigate(action)
    }
}
