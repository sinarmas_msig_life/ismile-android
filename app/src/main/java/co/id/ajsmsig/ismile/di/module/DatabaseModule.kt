package co.id.ajsmsig.ismile.di.module

import android.content.Context
import androidx.room.Room
import co.id.ajsmsig.ismile.db.iSmileDatabase
import co.id.ajsmsig.ismile.util.DATABASE_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context) = Room.databaseBuilder(context, iSmileDatabase::class.java, DATABASE_NAME).fallbackToDestructiveMigration().build()

}