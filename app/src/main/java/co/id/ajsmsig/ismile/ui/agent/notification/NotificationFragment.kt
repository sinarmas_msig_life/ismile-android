package co.id.ajsmsig.ismile.ui.agent.notification


import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentNotificationBinding
import co.id.ajsmsig.ismile.model.NotificationModel

class NotificationFragment : BaseFragment<FragmentNotificationBinding, NotificationViewModel>(),
    NotificationRecyclerAdapter.onNotificationPressedListener {

    private val adapter = NotificationRecyclerAdapter(listOf(), this)
    override fun getLayoutResourceId() = R.layout.fragment_notification
    override fun getViewModelClass(): Class<NotificationViewModel> = NotificationViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val vm = getViewModel()

        getDataBinding().vm = vm
        getDataBinding().fragment = this

        initRecyclerView()

        getViewModel().getInbox()

        getViewModel().inbox.observe(this, Observer {
            it?.let {
                if (it.error) {
                    getDataBinding().swipeNotification.isRefreshing= false
                    refreshData(emptyList())
                    snackbar(it.message)
                } else {
                    getDataBinding().swipeNotification.isRefreshing= true
                    refreshData(it.data)
                }
            }
        })

        getViewModel().shouldLogout.observe(this, Observer {
            it?.let {
                if (it) {
                    askForLogout()
                }
            }
        })

        getDataBinding().swipeNotification.setOnRefreshListener {
            getViewModel().getInbox()
        }

    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(activity)
        val dividerItemDecoration =
            DividerItemDecoration(getDataBinding().recyclerView.context, layoutManager.orientation)

        getDataBinding().recyclerView.layoutManager = layoutManager
        getDataBinding().recyclerView.adapter = adapter
        getDataBinding().recyclerView.addItemDecoration(dividerItemDecoration)

    }

    private fun refreshData(refreshData: List<NotificationModel>) {
        getDataBinding().swipeNotification.isRefreshing= false
        adapter.refreshData(refreshData)
    }

    override fun onNotificationSelected(notification: NotificationModel, position: Int) {

    }

    private fun askForLogout() {
        AlertDialog.Builder(activity)
            .setTitle("Logout diperlukan")
            .setMessage("Mohon untuk melakukan logout kemudian login kembali dikarenakan userid belum tersimpan di sesi. Logout sekarang?")
            .setCancelable(false)
            .setNegativeButton(android.R.string.no) { dialogInterface, i -> dialogInterface.dismiss() }
            .setPositiveButton(android.R.string.ok) { dialogInterface, i -> launchChooseLoginRoleFragment(dialogInterface) }
            .show()
    }

    private fun launchChooseLoginRoleFragment(dialogInterface: DialogInterface) {
        val action = NotificationFragmentDirections.actionLaunchChooseLoginRoleFragment()
        findNavController().navigate(action)
        dialogInterface.dismiss()
    }

}
