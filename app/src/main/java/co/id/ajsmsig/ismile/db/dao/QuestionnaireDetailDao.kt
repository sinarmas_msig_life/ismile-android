package co.id.ajsmsig.ismile.db.dao

import androidx.room.Dao
import androidx.room.Query
import co.id.ajsmsig.ismile.base.BaseDao
import co.id.ajsmsig.ismile.db.entity.QuestionnaireDetail

@Dao
interface QuestionnaireDetailDao : BaseDao<QuestionnaireDetail> {

    @Query("UPDATE QUESTIONNAIRES_DETAIL SET ANSWER = :answer, IS_ACTIVE = :isActive WHERE answer_id = :id AND counter_id = :counterId")
    fun update(id : Long, counterId: Long, answer: String?, isActive : Boolean)

}