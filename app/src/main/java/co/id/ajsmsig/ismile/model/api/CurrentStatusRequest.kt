package co.id.ajsmsig.ismile.model.api


import com.google.gson.annotations.SerializedName

data class CurrentStatusRequest(
    @SerializedName("registration_id")
    val registrationId: String?
)