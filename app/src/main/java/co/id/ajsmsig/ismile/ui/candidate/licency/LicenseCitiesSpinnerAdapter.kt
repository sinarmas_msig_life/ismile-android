package co.id.ajsmsig.ismile.ui.candidate.licency

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.fragment.app.FragmentActivity
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.databinding.CustomSpinnerLicenseCitiesBinding
import co.id.ajsmsig.ismile.model.LicenseCitiesExam

class LicenseCitiesSpinnerAdapter (val context: FragmentActivity, private var cities: List <LicenseCitiesExam>): BaseAdapter(){

    val inflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemRowHolder

        if (convertView == null) {
            val binding = CustomSpinnerLicenseCitiesBinding.inflate(inflater, parent, false)
            vh = ItemRowHolder(binding)
            view = binding.root
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemRowHolder
        }

        vh.bind(R.drawable.ic_action_employee, cities[position].citiyName)

        return view

    }

    private class ItemRowHolder(private val binding: CustomSpinnerLicenseCitiesBinding) {

        fun bind(imageResourceId: Int, label: String) {
            binding.ivLicenseCities.setImageResource(imageResourceId)
            binding.tvLicenseCities.text = label
            binding.executePendingBindings()
        }
    }

    override fun getItem(p0: Int): Any? = null
    override fun getItemId(p0: Int): Long = 0
    override fun getCount(): Int = cities.size

    fun refreshData(cities: List<LicenseCitiesExam>) {
        this.cities = cities
        notifyDataSetChanged()
    }
}