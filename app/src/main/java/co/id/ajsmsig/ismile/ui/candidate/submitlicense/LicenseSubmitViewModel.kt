package co.id.ajsmsig.ismile.ui.candidate.submitlicense

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.id.ajsmsig.ismile.SingleLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.model.InfoExamLicenseSubmitted
import co.id.ajsmsig.ismile.model.OnProgressModel
import co.id.ajsmsig.ismile.model.api.SubmitExamDataResponse
import co.id.ajsmsig.ismile.model.MessageInfo
import co.id.ajsmsig.ismile.ui.candidate.licency.LicenseRepository
import co.id.ajsmsig.ismile.util.CurrencyUtils
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class LicenseSubmitViewModel @Inject constructor(private val repository: LicenseRepository) : BaseViewModel() {

    val model = ObservableField <InfoExamLicenseSubmitted>()

    private val _isUserInputValid = MutableLiveData <Boolean>()
    val isUserInputValid : LiveData<Boolean>
        get() = _isUserInputValid

    private val _isMessage = SingleLiveData<MessageInfo>()
    val isMessage: LiveData<MessageInfo>
        get() = _isMessage

    private val _imagePath = MutableLiveData<String>()
    val imagePath: LiveData<String>
        get() = _imagePath

    private val _mCurrentPath = MutableLiveData <String>()
    val mCurrentPath: LiveData <String>
            get()= _mCurrentPath

    private val _displaySubmitDataOnProgress = SingleLiveData<OnProgressModel>()
    val displaySubmitDataOnProgress: LiveData<OnProgressModel>
        get() = _displaySubmitDataOnProgress

    //TODO : Display progress bar and navigate to home
    fun retrieveInfoExamSubmitted(){
        mCompositeDisposable += repository.retrieveDataLicenceFromLocal()
            .map {
                val convertNominal = CurrencyUtils.convertNumberToCurrency(it.nominal!!)
                InfoExamLicenseSubmitted (it.examCityName, it.examPlaceName, it.examSelectedDate, it.examMethodId, it.examProductTypeId, it.examTypeId, it.numberRek, it.banking, convertNominal) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {data-> getDataExamSucces(data)},
                {error-> getDataExamFailed(error)})
    }

    fun submitLicense (fileInBase64Format: String){
        val dataSubmitted= model.get()
        val removedWhiteSpaceBase64File = removeLineBreakCharachter(fileInBase64Format)
        mCompositeDisposable += repository.submitExamLicense(removedWhiteSpaceBase64File, dataSubmitted!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {data-> submittedSucces(data)},
                {error-> submittedError(error)})
    }


    fun validateUserInput() {

        if (!isNumberRekeningValid()) {
            return
        }
        if (!isBankingValid()) {
            return
        }
        if (!isNominal()) {
            return
        }
        if (!isImagePathValid()) {
            return
        }

        setDisplayOnProcess(false, "Mengunggah semua data. Mohon tunggu")
        _isUserInputValid.value = true
    }

    private fun isNumberRekeningValid(): Boolean {
        val model= model.get()
        if (model!!.numberRek.isNullOrEmpty()) {
            setErrorMessage("Harap hubungi customer service kami")
            return false
        }
        return true
    }

    private fun isBankingValid(): Boolean {
        val model= model.get()
        if (model!!.banking.isNullOrEmpty()) {
            setErrorMessage("Harap hubungi customer service kami")
            return false
        }
        return true
    }

    private fun isNominal(): Boolean {
        val model= model.get()
        if (model!!.nominal.isNullOrEmpty()) {
            setErrorMessage("Harap hubungi customer service kami")
            return false
        }
        return true
    }

    private fun isImagePathValid(): Boolean {
        if (_imagePath.value.isNullOrEmpty()) {
            setErrorMessage("Mohon upload foto terlebih dahulu")
            return false
        }
        return true
    }

    private fun getDataExamSucces(data: InfoExamLicenseSubmitted) {
        model.set(data)
    }

    private fun getDataExamFailed(error: Throwable) {
        Timber.e("$error")
    }

    private fun submittedSucces(data: SubmitExamDataResponse) {
        setDisplayOnProcess(true, null)
        _isMessage.value = MessageInfo(data.error, data.message)
    }

    private fun submittedError(error: Throwable) {
        setDisplayOnProcess(true, "Terjadi kesalahan. Mohon periksa kembali")
    }

    fun saveImagePath(mcurrentPhotoPath: String) {
        _imagePath.value = mcurrentPhotoPath
    }

    private fun setErrorMessage(message: String) {
        _isMessage.value = MessageInfo(true, message)
    }

    fun getAgentCandidateCode() : Long {
        val registrationId= repository.getSaveAgentCandidateCode()
        return registrationId!!.toLong()
    }

    private fun setDisplayOnProcess(isFinished: Boolean, message: String?) {
        _displaySubmitDataOnProgress.value = OnProgressModel(isFinished, message)
    }

    private fun removeLineBreakCharachter(input : String) : String {
        return input.replace("\n", "")
    }

    fun saveMcurrentPath(imagePath: String){
        _mCurrentPath.value = imagePath
    }
}