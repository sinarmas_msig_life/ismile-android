package co.id.ajsmsig.ismile.ui.splashfragment

import android.app.AlertDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.BuildConfig
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.common.ResultState
import co.id.ajsmsig.ismile.databinding.BottomSheetUpdateAplicationBinding
import co.id.ajsmsig.ismile.databinding.FragmentSplashBinding
import co.id.ajsmsig.ismile.model.AppVersionData
import co.id.ajsmsig.ismile.util.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.custom_alert_dialog_app_version.view.*
import timber.log.Timber

class SplashFragment : BaseFragment<FragmentSplashBinding, SplashViewModel>() {

    private lateinit var bottomSheet: BottomSheetDialog

    override fun getLayoutResourceId() = R.layout.fragment_splash
    override fun getViewModelClass(): Class<SplashViewModel> = SplashViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        appVersionChecked ()

        getActivity()!!.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)

        bottomSheet = BottomSheetDialog(activity!!)

        vm.appVersionResult.observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it) {
                    is ResultState.HasData -> validateAppToGooglePlay(it.data)
                    is ResultState.NoInternetConnection -> showAlertAppVersion(getString(R.string.title_error), getString(R.string.no_internet_connection))
                    is ResultState.TimeOut -> showAlertAppVersion(getString(R.string.title_error), getString(R.string.timeout))
                    else -> showAlertAppVersion(getString(R.string.title_error), getString(R.string.unknown_error))
                }
            }
        })
    }

    private fun validateAppToGooglePlay(data: AppVersionData) {
        val pInfo = activity!!.packageManager.getPackageInfo(activity!!.packageName, 0)
        val versionCode = pInfo.versionCode
        if (versionCode < data.version_code.toLong()){
            showBottomSheetsUpdateAplication()
        }else {
           settIngDestination()
        }
    }

    private fun settIngDestination(){
        Handler().postDelayed({
            val destinationScreenId = vm.destinationScreenId()
            when (destinationScreenId) {
                LAUNCH_AGENT_AGENT_HOME -> launchPage(SplashFragmentDirections.actionLaunchAgentHomeFragment())
                LAUNCH_AGENT_AGENT_LOGIN -> launchPage(SplashFragmentDirections.actionLaunchAgentLoginFragment(false))
                LAUNCH_CHOOSE_LOGIN_ROLE -> launchPage(SplashFragmentDirections.actionLaunchChooseLoginRole())
                LAUNCH_AGENT_AGENT_CANDIDATE_LOGIN -> launchPage(SplashFragmentDirections.actionLaunchAgentCandidateLogin())
                LAUNCH_AGENT_AGENT_CANDIDATE_HOME -> launchPage(SplashFragmentDirections.actionLaunchAgentCandidateHomeFragment(""))
                LAUNCH_SCREEN_UNDEFINED -> showErrorDialog(getString(R.string.title_error), getString(R.string.message_error_alert))
                LAUNCH_LEADER_AGENT_HOME -> launchPage(SplashFragmentDirections.actionLaunchLeaderAgentFragment())
                LAUNCH_LEADER_AGENT_LOGIN -> launchPage(
                    SplashFragmentDirections.actionLaunchAgentLoginFragment(
                        true
                    )
                )
            }
        }, 5000)
    }

    private fun launchPage(navDirections: NavDirections) {
        findNavController().navigate(navDirections)
    }

    private fun showErrorDialog(title: String, message: String) {
        AlertDialog.Builder(activity)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { dialogInterface, _ -> dialogInterface.dismiss() }
            .show()
    }

    private fun showAlertAppVersion(title: String, message: String){
        AlertDialog.Builder(activity)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(getString(R.string.try_again)) {
                    dialogInterface, _ -> getVersionApps() }
            .setNegativeButton(getString(R.string.exit)) {
                    dialogInterface, _ -> dialogInterface.dismiss() }
            .show()
    }

    private fun appVersionChecked (){
        if (BuildConfig.FLAVOR.equals("development")){
            showAlertDialogAppVersion()
            settIngDestination()
        }else {
            getVersionApps ()
        }
    }

    private fun showAlertDialogAppVersion (){
        val alertDialogScreen  = layoutInflater.inflate(R.layout.custom_alert_dialog_app_version, null)
        val alertDialogBuilder = AlertDialog.Builder(activity)
            .setTitle("PERINGATAN VERSI APLIKASI")
            .setView(alertDialogScreen)
            .setCancelable(false).create()

        alertDialogScreen.action_positive_state.setOnClickListener {
            alertDialogBuilder.dismiss()
        }

        alertDialogBuilder.show()
    }

    private fun getVersionApps () {
        try {
            val flagApps = FLAGS_APPS_SMILEGO
            val flagsPlatform = FLAGS_PLATFORM
            vm.getInfoUpdateApps(flagApps, flagsPlatform)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            Timber.e("PackageManager Catch : $e")
        }
    }
    
    fun showBottomSheetsUpdateAplication() {
        val binding : BottomSheetUpdateAplicationBinding = DataBindingUtil.inflate(
            LayoutInflater.from(
                context
            ), R.layout.bottom_sheet_update_aplication, null, false
        )
        binding.fragment = this
        binding.setLifecycleOwner(this)

        bottomSheet.setContentView(binding.root)
        bottomSheet.show()
    }

    fun onGooglePlayStoreLaunch () {
        val uri = Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID)
        var intent = Intent(Intent.ACTION_VIEW, uri)

        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)

        if (intent.resolveActivity(activity!!.packageManager) != null) {
            startActivity(intent)
        } else {
            intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID))
            if (intent.resolveActivity(activity!!.packageManager) != null) {
                startActivity(intent)
            } else {
                Toast.makeText(activity, "No play store or browser app", Toast.LENGTH_LONG).show()
            }
        }
    }
}

