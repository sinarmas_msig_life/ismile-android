package co.id.ajsmsig.ismile.model.api

import com.google.gson.annotations.SerializedName

data class AgentLoginResponse(
    @SerializedName("data")
    val data: Data,
    @SerializedName("error")
    val error: Boolean,
    @SerializedName("message")
    val message: String
) {
    data class Data(
        @SerializedName("msag_id")
        val msagId: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("title")
        val title: String,
        @SerializedName("title_id")
        val titleId: Int,
        @SerializedName("is_agent_leader")
        val isAgentLeader: Boolean
    )
}