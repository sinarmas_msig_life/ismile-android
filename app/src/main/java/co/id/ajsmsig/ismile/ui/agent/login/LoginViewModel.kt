package co.id.ajsmsig.ismile.ui.agent.login

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.SingleLiveData
import co.id.ajsmsig.ismile.base.BaseViewModel
import co.id.ajsmsig.ismile.model.api.AgentLoginResponse
import co.id.ajsmsig.ismile.model.api.SaveTokenNotificationResponse
import co.id.ajsmsig.ismile.model.uimodel.LoginAgentUiModel
import co.id.ajsmsig.ismile.util.LOGIN_TYPE_AGENT
import co.id.ajsmsig.ismile.util.LOGIN_TYPE_LEADER_AGENT
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val repository: LoginRepository) : BaseViewModel() {

    var username = ObservableField<String>()
    var errorUsername = ObservableInt()

    var password = ObservableField<String>()
    var errorPassword = ObservableInt()

    var loginTypeSelected = ObservableBoolean ()

    private val _isLoginSuccess = SingleLiveData<LoginAgentUiModel>()
    val isLoginSuccess: LiveData<LoginAgentUiModel>
        get() = _isLoginSuccess

    private fun validateLogin() {
        isLoading.set(true)
        val getEmail = username.get().toString().toUpperCase(Locale.ROOT)
        val getPassword = password.get()
        mCompositeDisposable += repository.loginAgent(getEmail, getPassword!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { setIsLoading(true) }
            .doOnTerminate { setIsLoading(false) }
            .subscribeWith(object : DisposableObserver<AgentLoginResponse>() {
                override fun onComplete() {
                    isLoading.set(false)
                }

                override fun onNext(it: AgentLoginResponse) {
                    isLoading.set(false)
                    username.set(getEmail.toUpperCase(Locale.ROOT))
                    if (!it.error) {
                        validateUserInfo(it.data.msagId, it.data.name, it.data.title, it.message, getEmail,it.data.isAgentLeader)
                    } else {
                        _isLoginSuccess.value = LoginAgentUiModel(true, it.message, false)
                    }
                }

                override fun onError(e: Throwable) {
                    isLoading.set(false)
                    _isLoginSuccess.value = LoginAgentUiModel(true, "Terjadi kesalahan. Mohon coba kembali", false)
                }
            })
    }

    fun validateUserInput() {
        if (!isUsernameValid()) {
            return
        }
        if (!isPasswordValid()) {
            return
        }
        validateLogin()
    }

    fun saveTokenNotification (token:String){
        mCompositeDisposable+= repository.saveTokenNotification(token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                saveTokenSucces(it)
            }
    }

    private fun saveTokenSucces(cek:SaveTokenNotificationResponse) {
        Timber.e(cek.message)
    }

    private fun validateUserInfo(agentCode : String, agentName : String, agentTitle : String, message : String, agentUserId : String, leader: Boolean) {
        if (agentCode.isEmpty()) {
            _isLoginSuccess.value = LoginAgentUiModel(true, "Kode agent Anda belum di set. Mohon hubungi tim Distribution Support kemudian coba kembali", false)
            return
        }

        _isLoginSuccess.sendAction(LoginAgentUiModel(false, message, leader))

        if (leader){
            repository.saveUserSession(agentCode, agentName, LOGIN_TYPE_LEADER_AGENT, agentTitle, agentUserId)
        }else{
            repository.saveUserSession(agentCode, agentName, LOGIN_TYPE_AGENT, agentTitle, agentUserId)
        }
    }

    private fun isUsernameValid(): Boolean {
        if (username.get().isNullOrEmpty()) {
            setErrorUsername(R.string.no_empty_field)
            return false
        }
        setErrorUsername()
        return true
    }

    private fun isPasswordValid(): Boolean {
        when {
            password.get().isNullOrEmpty() -> {
                setErrorPassword(R.string.no_empty_field)
                return false
            }
            password.get()!!.length<8 -> {
                setErrorPassword(R.string.minimal_password)
                return false
            }
            else -> {
                setErrorPassword()
            }
        }
        return true
    }

    fun setLoginType (typeLogin: Boolean) {
        loginTypeSelected.set(typeLogin)
    }

    private fun setErrorUsername(message: Int = 0) = errorUsername.set(message)
    private fun setErrorPassword(message: Int = 0) = errorPassword.set(message)
}