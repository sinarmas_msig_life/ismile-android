package co.id.ajsmsig.ismile.db.entity.join

import androidx.room.Embedded
import androidx.room.Relation
import co.id.ajsmsig.ismile.db.entity.Questionnaire
import co.id.ajsmsig.ismile.db.entity.QuestionnaireDetail

data class QuestionnaireAndAllQuestionnaireDetail (
    @Embedded
    var questionnaire : Questionnaire? = null,

    @Relation(
        parentColumn = "id",
        entityColumn = "questionnaire_id",
        entity = QuestionnaireDetail::class
    )
    var questionnairesDetail: List<QuestionnaireDetail> = emptyList()
)



