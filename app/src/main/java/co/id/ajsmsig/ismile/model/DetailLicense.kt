package co.id.ajsmsig.ismile.model

data class DetailLicense(
    val placesName: String,
    val examDate: String,
    val examMethodId: Int,
    val examMethodName: String,
    val examProductTypeId: Int,
    val examProductTypeName: String,
    val examTypeId: Int,
    val examTypeName: String,
    val noRek: String,
    val banking: String,
    val nominal: String
)