package co.id.ajsmsig.ismile.ui.agent.home


import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentHomeBinding
import co.id.ajsmsig.ismile.model.AgentMenu
import co.id.ajsmsig.ismile.ui.agent.recruit.RecruitFragment
import co.id.ajsmsig.ismile.ui.agent.recruitfailed.RecruitFailedFragment
import co.id.ajsmsig.ismile.ui.agent.recruitsuccess.RecruitSuccessFragment
import co.id.ajsmsig.ismile.util.DIALOG_MENU_ADD_AGENT_CANDIDATE
import co.id.ajsmsig.ismile.util.extensions.plusAssign
import com.google.firebase.iid.FirebaseInstanceId
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(), AgentMenuRecyclerAdapter.onMenuPressedListener {

    private val compositeDisposable = CompositeDisposable()

    override fun getLayoutResourceId() = R.layout.fragment_home
    override fun getViewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_home, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.action_inbox -> {
                launchNotificationFragment()
            }
            R.id.action_logout -> {
                logout()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val vm = getViewModel()

        getDataBinding().fragment = this
        getDataBinding().vm = vm

        setupViewpager()
        setupTabLayout()

        getViewModel().displaySavedUserSession()

    }


    private fun setupViewpager() {
        val adapter = ViewpagerAdapter(childFragmentManager)
        adapter.addFragment(RecruitFragment(), "Recruit")
        adapter.addFragment(RecruitSuccessFragment(), "Approved")
        adapter.addFragment(RecruitFailedFragment(), "Rejected")
        getDataBinding().viewpager.adapter = adapter
    }

    private fun setupTabLayout() {
        getDataBinding().tabLayout.setupWithViewPager(getDataBinding().viewpager)
    }

    class ViewpagerAdapter constructor(fragmentManager : FragmentManager) : FragmentPagerAdapter(fragmentManager) {

        private val fragmentList = mutableListOf<Fragment>()
        private val fragmentTitle = mutableListOf<String>()

        override fun getItem(position: Int) = fragmentList[position]
        override fun getCount() = fragmentList.size
        override fun getPageTitle(position: Int) = fragmentTitle[position]

        fun addFragment(fragment : Fragment, title: String) {
            fragmentList.add(fragment)
            fragmentTitle.add(title)
        }
    }

    override fun onMenuSelected(menu: AgentMenu, position: Int) {

    }

    fun onAddButtonPressed() {
        val addMenuOptions = resources.getStringArray(R.array.add_menu)
        AlertDialog.Builder(activity)
                .setItems(addMenuOptions) { dialogInterface, which ->
                    onAlertDialogItemPressed(which, dialogInterface)
                }
                .show()
    }

    private fun onAlertDialogItemPressed(position: Int, dialogInterface: DialogInterface) {
        when (position) {
            DIALOG_MENU_ADD_AGENT_CANDIDATE -> launchAgentCandidateFragment()
        }

        dialogInterface.dismiss()
    }

    private fun launchAgentCandidateFragment() {
        val action = HomeFragmentDirections.actionLaunchAddAgentCandidate()
        findNavController().navigate(action)
    }

    private fun launchNotificationFragment() {
       val action = HomeFragmentDirections.actionLaunchInboxFragment()
        findNavController().navigate(action)
    }

    private fun launchChooseLoginRole() {
        val action = HomeFragmentDirections.actionLaunchChooseLoginRoleFragment()
        findNavController().navigate(action)
    }

    fun onFabPressed() {
        launchAgentCandidateFragment()
    }

    private fun logout() {
        displayAlertLogout()
    }

    private fun displayAlertLogout (){
        val title = getString(R.string.confirm_logut)
        val message= getString(R.string.logout_message)
        AlertDialog.Builder(activity)
            .setTitle(title)
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok) { dialogInterface, _ ->
                launchChooseLoginRole()
                clearAllSavedUserData()
                deleteFcmInstance()
                dialogInterface.dismiss()
            }.setNegativeButton(android.R.string.no){dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            .show()
    }

    private fun deleteFcmInstance() {
        compositeDisposable += Observable.fromCallable {
            FirebaseInstanceId.getInstance().deleteInstanceId() }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    private fun clearAllSavedUserData() {
        getViewModel().clearAllSavedUserData()
    }


}
