package co.id.ajsmsig.ismile.util

import co.id.ajsmsig.ismile.model.ConvertStringToDate
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    fun setDateFormat (originalString: String) : ConvertStringToDate? {
        val dateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.US)

        try {
            val date = dateFormat.parse(originalString)
            val calendar=Calendar.getInstance()
            calendar.time = date!!
            val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
            val month = calendar.get(Calendar.MONTH)
            val year = calendar.get(Calendar.YEAR)

            return  ConvertStringToDate (year,month,dayOfMonth)
        }
        catch (e: ParseException){
            e.printStackTrace()
        }
        return null
    }

    fun getDateFormat(originalString:String): String? {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        try {
            val date = dateFormat.parse(originalString)

            val calendar=Calendar.getInstance()
            calendar.time = date!!

            val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
            val convertedDayOfWeek = String.format("%02d", dayOfMonth)
            val month = calendar.get(Calendar.MONTH) + 1
            val convertedDayOfMonth = String.format("%02d", month)
            val year = calendar.get(Calendar.YEAR)

            return  "$convertedDayOfWeek-$convertedDayOfMonth-$year"
        }
        catch (e: ParseException){
            e.printStackTrace()
        }

        return null
    }

    fun getDateFormat1(originalString:String): String?{
        val dateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        try {
            val date = dateFormat.parse(originalString)

            val calendar=Calendar.getInstance()
            calendar.time = date!!

            val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
            val convertedDayOfWeek = String.format("%02d", dayOfMonth)
            val month = calendar.get(Calendar.MONTH) + 1
            val convertedDayOfMonth = String.format("%02d", month)
            val year = calendar.get(Calendar.YEAR)

            return  "$year-$convertedDayOfMonth-$convertedDayOfWeek"
        }
        catch (e: ParseException){
            e.printStackTrace()
        }

        return null
    }

    /**
     * Convert dd/MM/yyyy HH:mm:ss date format to a more readable one
     *
     * @param specifiedDate
     * @return
     */
    fun getReadableDateFrom(specifiedDate: String): String? {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        try {
            val date = format.parse(specifiedDate)

            val calendar = Calendar.getInstance()
            calendar.time = date!!


            val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
            val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
            val mo = calendar.get(Calendar.MONTH)
            val year = calendar.get(Calendar.YEAR)
            val hour = String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY))
            val minute = String.format("%02d", calendar.get(Calendar.MINUTE))
            val second = String.format("%02d", calendar.get(Calendar.SECOND))

            val days = arrayOf("", "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu")
            val day = days[dayOfWeek]

            val months = arrayOf(
                "Januari",
                "Februari",
                "Maret",
                "April",
                "Mei",
                "Juni",
                "Juli",
                "Agustus",
                "September",
                "Oktober",
                "November",
                "Desember"
            )

            val month = months[mo]

            return "$day, $dayOfMonth $month $year $hour:$minute:$second"


        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return specifiedDate
    }

    fun getCurrentTime() : String{
        val date = Calendar.getInstance().time
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return dateFormat.format(date)
    }

    fun getCurrentTimeForFile() : String{
        val date = Calendar.getInstance().time
        val dateFormat = SimpleDateFormat("ddMMyyyyHHmmss")
        return dateFormat.format(date)
    }

}