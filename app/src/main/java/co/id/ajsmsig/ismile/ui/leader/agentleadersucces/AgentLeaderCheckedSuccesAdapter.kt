package co.id.ajsmsig.ismile.ui.leader.agentleadersucces

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.id.ajsmsig.ismile.databinding.RvItemApprovalStatusBinding
import co.id.ajsmsig.ismile.model.StatusApprove

class AgentLeaderCheckedSuccesAdapter (private var agentCandicateSucces: List<StatusApprove>, private val listener: AgentLeaderCheckedSuccesAdapter.onAgentCandidateSuccesListener) : RecyclerView.Adapter<AgentLeaderCheckedSuccesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RvItemApprovalStatusBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = agentCandicateSucces.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(agentCandicateSucces[position], listener, holder.adapterPosition)

    class ViewHolder(private val binding: RvItemApprovalStatusBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(model: StatusApprove, listener: onAgentCandidateSuccesListener?, position: Int) {
            binding.model = model
            binding.root.setOnClickListener {
                listener?.onRecruitPressed(model, position)
            }
            binding.executePendingBindings()
        }
    }

    interface onAgentCandidateSuccesListener {
        fun onRecruitPressed(candidateAgent: StatusApprove, position : Int)
    }

    fun refreshData(agentCandicateSucces: List<StatusApprove>) {
        this.agentCandicateSucces = agentCandicateSucces
        notifyDataSetChanged()
    }

}