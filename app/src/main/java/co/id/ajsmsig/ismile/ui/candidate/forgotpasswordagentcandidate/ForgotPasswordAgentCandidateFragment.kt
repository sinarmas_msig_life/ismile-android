package co.id.ajsmsig.ismile.ui.candidate.forgotpasswordagentcandidate

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentForgotPasswordAgentCandidateBinding

class ForgotPasswordAgentCandidateFragment :
    BaseFragment<FragmentForgotPasswordAgentCandidateBinding, ForgotPasswordAgentCandidateViewModel>() {

    override fun getLayoutResourceId() = R.layout.fragment_forgot_password_agent_candidate

    override fun getViewModelClass() = ForgotPasswordAgentCandidateViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val vm = getViewModel()

        getDataBinding().vm = vm
        getDataBinding().fragment = this

        getViewModel().isFindAccountSucces.observe( viewLifecycleOwner, Observer {
            if (it.findAccount) {
                snackbar(it.message)
                launchActivitySucces(it.message)
            } else {
                snackbar(it.message)
            }
        })
    }

    fun onSendPressed() {
        getViewModel().validateUserInput()
    }

    private fun launchActivitySucces(message: String) {
        val action = ForgotPasswordAgentCandidateFragmentDirections.actionLaunchForgotPasswordCandidateAgentFragment()
        action.message = message
        findNavController().navigate(action)
    }

}
