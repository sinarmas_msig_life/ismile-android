package co.id.ajsmsig.ismile.model

data class InfoExamLicenseSubmitted (
    var examCityName: String?,
    var examPlaceName: String?,
    var examSelectedDate: String?,
    var examMethodId: Int?,
    var examProductTypeId: Int?,
    var examTypeId: Int?,
    var numberRek: String?,
    var banking: String?,
    var nominal: String?
)