package co.id.ajsmsig.ismile.util.enum

enum class QuestionnaireAnswer(val value : Int) {
    NOT_SELECTED(-1),
    NO(0),
    YES(1)
}