package co.id.ajsmsig.ismile.model

data class RecruitmentStatusModel(val imageSrc : Int,  val candidateAgentId : Int, val candidateAgentName : String,  val date : String,  val latestStatus : String)