package co.id.ajsmsig.ismile.ui.splashfragment

import android.content.SharedPreferences
import co.id.ajsmsig.ismile.api.ApiService
import co.id.ajsmsig.ismile.util.*
import javax.inject.Inject

class SplashLocalDataSource @Inject constructor(private val editor: SharedPreferences.Editor, private val preferences: SharedPreferences) {

    fun setLoggedIn(isLoggedIn : Boolean) {
        editor.putBoolean(PREF_KEY_LOGIN, isLoggedIn)
        editor.apply()
    }

    fun saveAgentCode(agentCode : String) {
        editor.putString(PREF_KEY_AGENT_CODE, agentCode)
        editor.apply()
    }

    fun saveAgentName(agentName : String) {
        editor.putString(PREF_KEY_AGENT_NAME, agentName)
        editor.apply()
    }

    fun saveLoginRoleType(loginRoleType : Int) {
        editor.putInt(PREF_KEY_LOGIN_ROLE_TYPE, loginRoleType)
        editor.apply()
    }

    fun isLoggedIn() = preferences.getBoolean(PREF_KEY_LOGIN, false)
    fun getLoginRoleType() = preferences.getInt(PREF_KEY_LOGIN_ROLE_TYPE, 0)

}