package co.id.ajsmsig.ismile.ui.candidate.resetpasswordsucces

import android.os.Bundle
import android.view.View
import androidx.databinding.ObservableField
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentResetPasswordSuccesBinding


class ResetPasswordSuccesFragment : BaseFragment<FragmentResetPasswordSuccesBinding,ResetPasswordSuccesViewModel>() {

    val labelInfoSucces=ObservableField<String>()

    override fun getLayoutResourceId()= R.layout.fragment_reset_password_succes

    override fun getViewModelClass()=ResetPasswordSuccesViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getDataBinding().fragment = this

        val args=ResetPasswordSuccesFragmentArgs.fromBundle(arguments!!).message
        labelInfoSucces.set(args)

    }

    fun onLoginClick(){
        val action=ResetPasswordSuccesFragmentDirections.actionLaunchChooseLogin()
        findNavController().navigate(action)
    }


}
