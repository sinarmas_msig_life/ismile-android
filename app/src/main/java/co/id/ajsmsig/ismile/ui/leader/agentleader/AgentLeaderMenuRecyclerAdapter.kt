package co.id.ajsmsig.ismile.ui.leader.agentleader

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.id.ajsmsig.ismile.databinding.RvItemAgentMenuBinding
import co.id.ajsmsig.ismile.model.AgentMenu

class AgentLeaderMenuRecyclerAdapter (private var agentMenuList: List<AgentMenu>, private val listener: onMenuPressedListener)
    : RecyclerView.Adapter<AgentLeaderMenuRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RvItemAgentMenuBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = agentMenuList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(agentMenuList[position], listener, holder.adapterPosition)

    class ViewHolder(private val binding: RvItemAgentMenuBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(menu: AgentMenu, listener: onMenuPressedListener?, position: Int) {
            binding.menu = menu

            binding.root.setOnClickListener {
                listener?.onMenuSelected(menu, position)
            }

            binding.executePendingBindings()
        }
    }

    interface onMenuPressedListener {
        fun onMenuSelected(menu: AgentMenu, position: Int)
    }

    fun refreshData(agentMenuList: List<AgentMenu>) {
        this.agentMenuList = agentMenuList
        notifyDataSetChanged()
    }

}