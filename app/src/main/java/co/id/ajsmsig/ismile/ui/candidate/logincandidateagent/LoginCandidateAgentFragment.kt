package co.id.ajsmsig.ismile.ui.candidate.logincandidateagent

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.id.ajsmsig.ismile.R
import co.id.ajsmsig.ismile.base.BaseFragment
import co.id.ajsmsig.ismile.databinding.FragmentLoginCandidateAgentBinding
import com.google.firebase.iid.FirebaseInstanceId
import timber.log.Timber


class LoginCandidateAgentFragment : BaseFragment<FragmentLoginCandidateAgentBinding, LoginCandidateAgentViewModel>() {

    override fun getLayoutResourceId() = R.layout.fragment_login_candidate_agent

    override fun getViewModelClass() = LoginCandidateAgentViewModel::class.java

    //Defined the required values
    companion object {
        const val CHANNEL_ID = "ismileapp-sinarmasmsiglife"
        private const val CHANNEL_NAME= "i-SMiLe"
        private const val CHANNEL_DESC = "i-SMiLe Channel"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val vm = getViewModel()
        getDataBinding().fragment = this
        getDataBinding().vm = vm

        vm.loginSuccessful.observe(this, Observer {
            it?.let {
                if (!it.error) {
                    launchAgentCandidateHomeFragment(it.registrationId)
                } else {
                    snackbar(it.message)
                }
            }
        })
    }

    fun onLoginPressed() {
        getViewModel().validateUserLogin()
    }

    private fun launchAgentCandidateHomeFragment(registrationId: String) {
        initFirebasePushNotif()
        createNotificationChannel()
        val action = LoginCandidateAgentFragmentDirections.actionLaunchAgentCandidateHomeFragment(registrationId)
        findNavController().navigate(action)
    }

    fun onTextChangePasswordClick(){
        val action=LoginCandidateAgentFragmentDirections.actionlaunchForgotPassword()
        findNavController().navigate(action)
    }

    private fun initFirebasePushNotif() {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            if (!it.isSuccessful) {
                Timber.e("[Notification] Unable to generate token ${it.exception}")
                return@addOnCompleteListener
            }

            // Get new Instance ID token
            val token = it.result?.token
            Timber.v("[Notification] Your token is : $token")
            getViewModel().saveTokenNotification(token!!)
        }
    }

    private fun createNotificationChannel() {
        //creating notification channel if android version is greater than or equals to oreo
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(LoginCandidateAgentFragment.CHANNEL_ID, LoginCandidateAgentFragment.CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)
            channel.description = LoginCandidateAgentFragment.CHANNEL_DESC
            val manager = activity?.getSystemService(NotificationManager::class.java)
            manager?.createNotificationChannel(channel)
        }
    }

}
